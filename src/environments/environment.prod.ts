export const environment = {
  production: true,
  // API_DOMAIN: 'https://api.hsf-delivery.online',
  ASSETS_DOMAIN: 'https://s3-hsf-delivery.s3.eu-north-1.amazonaws.com',
  API_DOMAIN: 'https://hsf-delivery.store/',
  locales: ['en', 'fr', 'it', 'de'],
  defaultLocale: 'en',
};
