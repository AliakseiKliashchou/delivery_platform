import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public title: string = 'delivery-platform';
  public isHideFooter: boolean = true;
  public isHideHeader: boolean = true;

  constructor(
    private router: Router,
    private translateService: TranslateService
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isHideFooter =
          val.url.includes('vendor') || val.url.includes('moderation');
        this.isHideHeader =
          val.url.includes('vendor-account') ||
          val.url.includes('moderator-account');
      }
    });
  }

  ngOnInit(): void {
    this.translateService.use(environment.defaultLocale);
  }
}
