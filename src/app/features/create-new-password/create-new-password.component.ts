import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { catchError, debounceTime } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import {
  signUpResponse,
  VendorAccountService,
} from 'src/app/shared/services/vendor-account.service';
import { IVendorAccountData } from '../login-page/login-page.component';

@Component({
  selector: 'app-create-new-password',
  templateUrl: './create-new-password.component.html',
  styleUrls: ['./create-new-password.component.scss'],
})
export class CreateNewPasswordComponent
  extends UnsubscribeComponent
  implements OnInit
{
  public passwordGroup: FormGroup = new FormGroup({
    password: new FormControl('', Validators.required),
    repeatedPassword: new FormControl('', Validators.required),
  });
  public isRequirementDisplay: boolean = false;
  public hidePassword: boolean = true;
  public hideRepeatedPassword: boolean = true;
  public isPasswordValid: boolean = false;
  public isPasswordLengthValid: boolean | string = 'default';
  public isPasswordIncludeDigit: boolean | string = 'default';
  public isPasswordIncludeSpecialSymbol: boolean | string = 'default';
  public isPasswordIncludeUppercaseLatter: boolean | string = 'default';
  public isPasswordIncludeLowercaseLatter: boolean | string = 'default';
  public defaultSvgPaths: string =
    '../../../../assets/images/grey-cross-for-password.svg';
  public validSvgPaths: string =
    '../../../../assets/images/green-arrow-for-password.svg';
  public invalidSvgPaths: string =
    '../../../../assets/images/red-cross-for-password.svg';
  public isPasswordMatch!: boolean | undefined;
  public identityToken!: string;
  public isPasswordReset: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private vendorAccountService: VendorAccountService,
    private cookieService: CookieService,
    private _router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribeTo = this.activatedRoute.queryParams.subscribe(
      (params: Params) => {
        this.identityToken = params['identity_token'];
      }
    );
    this.subscribeTo = this.passwordGroup.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.isPasswordLengthValid = value.password.length >= 8;
        this.isPasswordIncludeDigit = /[0-9]/.test(value.password);
        this.isPasswordIncludeSpecialSymbol = /[\.\,\(\)\*\-\+\=\_\!\&\@\&\#]/.test(
          value.password
        );
        this.isPasswordIncludeUppercaseLatter = /[A-Z]/.test(value.password);
        this.isPasswordIncludeLowercaseLatter = /[a-z]/.test(value.password);

        if (
          this.isPasswordLengthValid === true &&
          this.isPasswordIncludeDigit === true &&
          this.isPasswordIncludeSpecialSymbol === true &&
          this.isPasswordIncludeUppercaseLatter === true &&
          this.isPasswordIncludeLowercaseLatter === true
        ) {
          this.isPasswordValid = true;
        } else {
          this.isPasswordValid = false;
        }
        if (value.repeatedPassword.length > 0) {
          this.isPasswordMatch = value.password === value.repeatedPassword;
        } else {
          this.isPasswordMatch = undefined;
        }
      });
  }

  public withoutCyr(event: any): void {
    event.target.value = event.target.value.replace(/[а-яА-ЯёЁ]/g, '');
  }

  public changeSvg(validator: boolean | string): string {
    if (validator === true) {
      return this.validSvgPaths;
    } else if (validator === false) {
      return this.invalidSvgPaths;
    } else {
      return this.defaultSvgPaths;
    }
  }

  public resetPassword(password: string): void {
    if (this.passwordGroup.valid && this.isPasswordMatch === true) {
      this.subscribeTo = this.vendorAccountService
        .resetPassword(this.identityToken, password)
        .pipe(
          catchError((error) => {
            throw error;
          })
        )
        .subscribe((tokens: signUpResponse) => {
          this.cookieService.set(
            'hsf-account-vendor-access-token',
            tokens.access!,
            undefined,
            '/'
          );
          this.cookieService.set(
            'hsf-account-vendor-refresh-token',
            tokens.refresh!,
            undefined,
            '/'
          );
          this.isPasswordReset = true;
        });
    } else {
      this.passwordGroup.markAllAsTouched();
    }
  }

  public loginVendor(): void {
    this.cookieService.get('hsf-account-vendor-access-token') &&
      (this.subscribeTo = this.vendorAccountService
        .getVendorData()
        .subscribe((res: IVendorAccountData) => {
          this._router.navigate([
            `/feed/vendor/vendor-account/${res.user.id}/products`,
          ]);
        }));
  }
}
