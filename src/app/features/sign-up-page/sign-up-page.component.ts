import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { catchError, debounceTime, throwError } from 'rxjs';
import { NgxTippyProps } from 'ngx-tippy-wrapper';

import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import {
  IEmailVerificationResponse,
  ISCNVerificationResponse,
  signUpResponse,
  VendorAccountService,
} from 'src/app/shared/services/vendor-account.service';
import { IVendorAccountData } from '../login-page/login-page.component';

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.scss'],
})
export class SignUpPageComponent
  extends UnsubscribeComponent
  implements OnInit
{
  public firstPanelOpenState = false;
  public secondPanelOpenState = false;
  public scratchCardNumberFormGroup: FormGroup = new FormGroup({
    scratchCardNumber: new FormControl('', [
      Validators.required,
      Validators.pattern(/([A-Za-z]{2})(\d{8})$/),
    ]),
  });
  public isEmailSent: boolean = false;
  public isEmailNotReceived: boolean = true;
  public email: string = 'example@example.com';
  public isVerificationPassed: boolean = false;
  public passwordGroup: FormGroup = new FormGroup({
    password: new FormControl('', Validators.required),
    repeatedPassword: new FormControl('', Validators.required),
  });
  public hidePassword: boolean = true;
  public hideRepeatedPassword: boolean = true;
  public isRequirementDisplay: boolean = false;
  public defaultSvgPaths: string =
    '../../../../assets/images/grey-cross-for-password.svg';
  public validSvgPaths: string =
    '../../../../assets/images/green-arrow-for-password.svg';
  public invalidSvgPaths: string =
    '../../../../assets/images/red-cross-for-password.svg';
  public isPasswordLengthValid: boolean | string = 'default';
  public isPasswordIncludeDigit: boolean | string = 'default';
  public isPasswordIncludeSpecialSymbol: boolean | string = 'default';
  public isPasswordIncludeUppercaseLatter: boolean | string = 'default';
  public isPasswordIncludeLowercaseLatter: boolean | string = 'default';
  public isPasswordValid: boolean = false;
  public isPasswordMatch!: boolean | undefined;
  public confirmDetailsGroup: FormGroup = new FormGroup({
    vendor_name: new FormControl('', Validators.required),
    phone: new FormControl('', [
      Validators.required,
      Validators.pattern(/([0-9]{10})$/),
      Validators.minLength(10),
    ]),
    email: new FormControl('', Validators.required),
  });
  public goToConfirmDetails: boolean = false;
  public SCNNotFound: boolean = false;
  public SCNConflict: boolean = false;
  public vendorPassword: string = '';
  private vendorData!: ISCNVerificationResponse;
  public isThreeAttempts: boolean = false;
  public isEmailResend: boolean = false;
  public counter: number = 5;
  public hiddenEmail: string = '';
  public intervalId: any;
  public timeOutId: any;
  public isClicked: boolean = false;
  public toolTipProps: NgxTippyProps = {
    arrow: true,
    placement: 'right',
    offset: [0, 30],
    maxWidth: 295,
    trigger: 'click'
  };

  constructor(
    private location: Location,
    private vendorAccountService: VendorAccountService,
    private _router: Router,
    private cookieService: CookieService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribeTo = this.scratchCardNumberFormGroup.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.SCNNotFound = false;
        this.SCNConflict = false;
        this.isClicked = false;
      });

    this.subscribeTo = this.passwordGroup.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.isPasswordLengthValid = value.password.length >= 8;
        this.isPasswordIncludeDigit = /[0-9]/.test(value.password);
        this.isPasswordIncludeSpecialSymbol = /[\.\,\(\)\*\-\+\=\_\!\&\@\&\#]/.test(
          value.password
        );
        this.isPasswordIncludeUppercaseLatter = /[A-Z]/.test(value.password);
        this.isPasswordIncludeLowercaseLatter = /[a-z]/.test(value.password);

        if (
          this.isPasswordLengthValid
          && this.isPasswordIncludeDigit
          && this.isPasswordIncludeSpecialSymbol
          && this.isPasswordIncludeUppercaseLatter
          && this.isPasswordIncludeLowercaseLatter
        ) {
          this.isPasswordValid = true;
        } else {
          this.isPasswordValid = false;
        }
        if (value.repeatedPassword.length > 0) {
          this.isPasswordMatch = value.password === value.repeatedPassword;
        } else {
          this.isPasswordMatch = undefined;
        }
      });
  }

  public goBack(): void {
    this.location.back();
  }

  public withoutCyr(event: any): void {
    event.target.value = event.target.value.replace(/[а-яА-ЯёЁ]/g, '');
  }

  public getAccess(scratchCardNumber: string | null): void {
    this.isClicked = true;
    this.scratchCardNumberFormGroup.valid
      ? this.vendorAccountService
          .sendVerificationEmail(scratchCardNumber)
          .pipe(
            catchError((error) => {
              switch (error.status) {
                case 404: {
                  this.SCNNotFound = true;
                  this.SCNConflict = false;
                  this.isClicked = false;
                  this.isThreeAttempts = false;
                  throw error;
                }
                case 409: {
                  this.SCNConflict = true;
                  this.isClicked = false;
                  this.SCNNotFound = false;
                  this.isThreeAttempts = false;
                  throw error;
                }
                case 400: {
                  this.isThreeAttempts = true;
                  this.SCNConflict = false;
                  this.isClicked = false;
                  this.SCNNotFound = false;
                  throw error;
                }
              }
              throw error;
            })
          )
          .subscribe((result: IEmailVerificationResponse) => {
            if (result.success === true) {
              this.isEmailSent = true;
              this.startTimer();
              let emailArray = result.vendor_email.split('');
              for (let i = 2; i < result.vendor_email.indexOf('@'); i++) {
                emailArray[i] = '*';
              }
              this.email = result.vendor_email;
              this.hiddenEmail = emailArray.join('');
            }
          })
      : this.scratchCardNumberFormGroup.markAllAsTouched();
  }

  public startTimer(): void {
    this.counter = 9;
    this.intervalId = setInterval(() => {
      this.counter--;
    }, 1000);

    this.timeOutId = setTimeout(() => {
      clearInterval(this.intervalId);
      this.isEmailResend = true;
    }, 9000);
  }

  public changeSvg(validator: boolean | string): string {
    if (validator === true) {
      return this.validSvgPaths;
    } else if (validator === false) {
      return this.invalidSvgPaths;
    } else {
      return this.defaultSvgPaths;
    }
  }

  public onSetVendorData(data: ISCNVerificationResponse): void {
    if (data.identity_token) {
      let { vendor_name, phone, email } = data.vendor_data!;
      this.confirmDetailsGroup.patchValue({ vendor_name, phone, email });
      this.vendorData = data;
      this.isEmailNotReceived = false;
    } else {
      this.isEmailNotReceived = true;

      return;
    }
  }

  public goToConfirmDetailsPage(password: string): void {
    if (this.passwordGroup.valid && this.isPasswordMatch === true) {
      this.vendorPassword = password;
      this.goToConfirmDetails = true;
    } else {
      this.passwordGroup.markAllAsTouched();
    }
  }

  public signUpVendor(vendor_name: string, phone: string, email: string): void {
    if (this.confirmDetailsGroup.valid) {
      if (this.vendorData) {
        let { license_number, scratch_card_number, country, city } =
          this.vendorData.vendor_data!;
        let generalVendorData = {
          license_number,
          scratch_card_number,
          country,
          city,
          email,
          phone,
          vendor_name,
          password: this.vendorPassword,
          identity_token: this.vendorData.identity_token!,
        };
        this.vendorAccountService
          .signUpVendor(generalVendorData)
          .subscribe((res: signUpResponse) => {
            if (res.access) {
              this.cookieService.set(
                'hsf-account-vendor-access-token',
                res.access,
                undefined,
                '/'
              );
              this.cookieService.set(
                'hsf-account-vendor-refresh-token',
                res.refresh!,
                undefined,
                '/'
              );
              this.subscribeTo = this.vendorAccountService
                .getVendorData()
                .subscribe((vendorData: IVendorAccountData) => {
                  this._router.navigate([
                    `/feed/vendor/vendor-account/${vendorData.user.id}/products`,
                  ]);
                });
            } else {
              throwError;
            }
          });
      }
    } else {
      this.confirmDetailsGroup.markAllAsTouched();
    }
  }

  public resendEmail(scratchCardNumber: string | null): void {
    this.counter = 9;
    this.getAccess(scratchCardNumber);
    this.isEmailResend = false;
  }

  public goBackToSCNInput(): void {
    this.isEmailSent = false;
    this.isEmailNotReceived = true;
    clearInterval(this.intervalId);
    clearTimeout(this.timeOutId);
    this.isEmailResend = false;
  }

  public get scratchCardNumber(): AbstractControl<string> {

    return this.scratchCardNumberFormGroup.get('scratchCardNumber')!;
  }

}
