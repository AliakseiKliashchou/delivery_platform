import { Location } from '@angular/common';
import { ChangeDetectorRef, Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { Select, Store } from '@ngxs/store';

import {
  OrderProducts,
  SetOrderDetails,
} from '../cart/ngxs/product.actions';
import { ProductState } from '../cart/ngxs/product.state';
import { ProductsService } from '../cart/ngxs/products.service';
import { LocationModalComponent } from '../location/location-modal/location-modal.component';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { ICartItem, IOrderDetails } from 'src/app/core/models/product.model';
import { ApiService } from 'src/app/core/services/api.service';
import { OrderConfirmationWindowComponent } from 'src/app/shared/components/order-confirmation-window/order-confirmation-window.component';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';

@Component({
  selector: 'app-order-details-page',
  templateUrl: './order-details-page.component.html',
  styleUrls: ['./order-details-page.component.scss'],
})
export class OrderDetailsPageComponent extends UnsubscribeComponent {
  public currentVendorId: string = '';
  public currentVendorName: string = '';
  public currentVendorAddress: string = '';
  private orderLocationsId: number = 1;
  public customerDataGroup: FormGroup = new FormGroup({
    customerName: new FormControl('', [Validators.required]),
    customerPhone: new FormControl(''),
    customerEmail: new FormControl('', [Validators.email]),
    customerComments: new FormControl(''),
  });
  public customerId!: number;

  private totalPrice: number = 0;
  private productsInOrder: any = {};
  public locationAddress?: string;

  constructor(
    private location: Location,
    private dialog: MatDialog,
    private productService: ProductsService,
    private store: Store,
    private _apiService: ApiService,
    private cookieService: CookieService,
    private changeDetector: ChangeDetectorRef
  ) {
    super();
  }

  @Select(ProductState.getProductCart) productCartData$!: Observable<any>;
  @Select(ProductState.getTotalPrice) totalPrice$!: Observable<number>;
  @Select(ProductState.getOrderDetails)
  orderDetails$!: Observable<IOrderDetails>;

  public ngOnInit(): void {
    this.subscribeTo = this.productCartData$.subscribe((res) => {
      this.currentVendorId = res[0].vendor_id.toString();
      res.forEach((cartItem: ICartItem) => {
        this.productsInOrder[cartItem.id] = { quantity: cartItem.quantity };
      });
    });

    this.subscribeTo = this.productService
      .getProductsByVendorsId(this.currentVendorId, '')
      .subscribe((res) => {
        this.currentVendorName = res.name;
        this.currentVendorAddress = `${res.locations[0].address_1}, ${res.locations[0].address_2}`;
        this.orderLocationsId = res.locations[0].id;
      });

    this.subscribeTo = this.totalPrice$.subscribe(
      (totalPrice: number) => (this.totalPrice = Number(totalPrice))
    );

    this.subscribeTo = this.orderDetails$.subscribe((orderDetails) => {
      if (orderDetails) {
        this.customerDataGroup
          .get('customerEmail')
          ?.setValue(orderDetails.customerEmail);
        this.customerDataGroup
          .get('customerPhone')
          ?.setValue(orderDetails.customerPhone);
        this.customerDataGroup
          .get('customerName')
          ?.setValue(orderDetails.customerName);
        this.customerDataGroup
          .get('customerComments')
          ?.setValue(orderDetails.customerComments);
        this.customerId = orderDetails.customerId!;
      }
    });
  }

  public ngAfterViewChecked(): void {
    //todo refector to GET location
    this.locationAddress = this.cookieService.get('hsf-account-address');
    this.changeDetector.detectChanges();
  }

  public goBack() {
    this.store.dispatch(
      new SetOrderDetails({
        customerId: this.customerId,
        customerPhone: this.customerDataGroup.get('customerPhone')?.value,
        customerComments: this.customerDataGroup.get('customerComments')?.value,
        customerEmail: this.customerDataGroup.get('customerEmail')?.value,
        customerName: this.customerDataGroup.get('customerName')?.value,
        customerLocation: [this.locationAddress!],
      })
    );
    this.location.back();
  }

  public openDialog() {
    this.dialog.open(OrderConfirmationWindowComponent, {
      data: { currentVendorName: this.currentVendorName },
      width: '500px',
      height: '380px',
      panelClass: 'app-order-confirmation-dialog',
    });
  }

  public openMap(): void {
    this.dialog.open(LocationModalComponent, {
      width: '800px',
      height: '600px',
      panelClass: 'app-no-padding-dialog',
    });
  }

  public sendOrder() {
    // clearing validators is required, "mask" directive adds side-effect validators
    this.customerDataGroup.controls['customerPhone'].clearValidators();
    this.store.dispatch(
      new SetOrderDetails({
        customerId: this.customerId,
        customerPhone: this.customerDataGroup.get('customerPhone')?.value,
        customerComments: this.customerDataGroup.get('customerComments')?.value,
        customerEmail: this.customerDataGroup.get('customerEmail')?.value,
        customerName: this.customerDataGroup.get('customerName')?.value,
        customerLocation: [this.locationAddress!],
      })
    );
    if (this.customerDataGroup.valid) {
      this.store.dispatch(
        new OrderProducts({
          order_status: 'pending',
          payment_method: 'by_cash',
          payment_status: 'unpaid',
          order_comment: this.customerDataGroup.get('customerComments')?.value,
        })
      ).subscribe(() => {
          this.openDialog();
        });
    } else {
      this.customerDataGroup.markAllAsTouched();
    }
  }

  public paymentMethod: { title: string }[] = [
    //temporary until have contract with backend

    {
      title: 'By cash',
    },
    {
      title: 'By card',
    },
    {
      title: 'By card online',
    },
  ];

  public paymentMethodControl = new FormControl({ title: '' });
  public disableSelect = new FormControl(true);
  public selected = 'By cash';
}
