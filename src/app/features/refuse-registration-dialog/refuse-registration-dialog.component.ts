import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-refuse-registration-dialog',
  templateUrl: './refuse-registration-dialog.component.html',
  styleUrls: ['./refuse-registration-dialog.component.scss']
})
export class RefuseRegistrationDialogComponent {}
