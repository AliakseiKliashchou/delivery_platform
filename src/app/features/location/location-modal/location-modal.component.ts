/* eslint @typescript-eslint/naming-convention: 0 */ // temporary until we choose a workaround to deal with API snake_case naming
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MapGeocoderResponse } from '@angular/google-maps';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { Store } from '@ngxs/store';

import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { GeolocationService } from '../../../shared/services/geolocation.service';
import { SetCustomerLocation } from '../../cart/ngxs/product.actions';
import { GetVendors } from '../../vendors/ngxs/vendors.actions';
import { CookieService } from 'ngx-cookie-service';
import { ICustomerLocation } from 'src/app/core/models/product.model';

export interface IMarkerData {
  position: { lat: number; lng: number };
  title: string;
  options: { animation?: google.maps.Animation | undefined, icon?: string, draggable?: boolean };
} // temporary until decide where to store interfaces

export interface DialogData {
  cart: boolean
}

@Component({
  selector: 'app-location-modal',
  templateUrl: './location-modal.component.html',
  styleUrls: ['./location-modal.component.scss']
})

export class LocationModalComponent extends UnsubscribeComponent implements OnInit {

  public location!: google.maps.LatLngLiteral;
  public zoom: number = 16;
  public markers: IMarkerData[] = [];

  public searchField: FormControl = new FormControl('');
  public geocodedAddress: string = '';
  public locationMarker?: IMarkerData;
  public locationData?: ICustomerLocation;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private geolocationService: GeolocationService,
    private cookieService: CookieService,
    private matDialog: MatDialog,
    private store: Store,
    private router: Router
  ) {
    super();
  }

  public ngOnInit(): void {
      this.onAutoLocate();
  }


  public onSelected(placeId: string) {
    this.subscribeTo = this.geolocationService.geocodePlace(placeId)
      .subscribe((response: MapGeocoderResponse) => {
        const { location } = response.results[0].geometry;

        this.geocodedAddress = response.results[0].formatted_address;
        this.location = {
          lat: location.lat(),
          lng: location.lng(),
        };
        this.parseAddressResults(response);
        this.createMarker(this.location);
      });
  }

  public onAutoLocate(): void {
    const location = this.cookieService.get('hsf-account-location');

    if (!location && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        this.showPosition(this.location);
      });
    } else {
      this.location = {
        lat: +location.split(',')[0],
        lng: +location.split(',')[1]
      };
    }
    this.showPosition(this.location);
  }

  public showPosition(coords: google.maps.LatLngLiteral): void {
    if (coords) {
      this.subscribeTo = this.geolocationService.geocodePosition(coords)
        .subscribe(( response: MapGeocoderResponse ) => {
          this.geocodedAddress = response.results[0].formatted_address;
          this.location = coords;
          this.searchField.setValue(this.geocodedAddress);
          this.parseAddressResults(response);
          this.createMarker(this.location);
        });
    }
  }

  public createMarker(position: google.maps.LatLngLiteral): void {
    this.locationMarker = {
      position: {
        lat: position.lat,
        lng: position.lng
      },
      title: this.geocodedAddress,
      options: {
        icon: '../../../assets/images/marker.svg',
        draggable: true
      }
    };
    this.markers = [this.locationMarker];
  }

  public resetLocation(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.location = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        this.showPosition(this.location);
      });
    }
  }
  //todo why dont POST on server?
  public confirmLocation(): void {
    this.cookieService.set('hsf-account-address', this.geocodedAddress);
    this.cookieService.set('hsf-account-location', `${this.location.lat},${this.location.lng}`);

    if(this.locationData) {
      this.store.dispatch(new SetCustomerLocation(this.locationData));
    }

    if (this.data?.cart) {
      this.router.navigate(["/feed/order"]);
    }
    else {
      this.store.dispatch(new GetVendors());
    }
    this.matDialog.closeAll();
  }

  public parseAddressResults({ results }: MapGeocoderResponse): void {
    const { address_components, geometry } = results[0];
    let country_name = '';
    let city_name = '';

    address_components.forEach((address) => {
      if(address.types.includes("country")) {
        country_name = address.long_name;
      } else if(address.types.includes('locality')) {
        city_name = address.long_name;
      }
    });
    this.locationData = {
      latitude: geometry.location.lat(),
      longitude: geometry.location.lng(),
      address_1: results[0].formatted_address,
      address_2: results[1]?.formatted_address || results[0].formatted_address,
      country_name,
      city_name,
    };
  }
}
