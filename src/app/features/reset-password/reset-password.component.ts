import { Component} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {catchError} from 'rxjs';

import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent extends UnsubscribeComponent {
  public emailGroup: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required)
  })

  public isEmailSent: boolean = false;
  public isEmailRegistred: boolean = true;
  public encryptedEmail: string = '';
  public isEmailResend: boolean = true;
  public counter: number = 9;
  public intervalId: any;
  public timeOutId: any;
  public isEmailReceived: boolean = true;
  public isPasswordReset: boolean = false;

  constructor(private vendorAccountService: VendorAccountService) {super()}

  public sentResetLink(email: string): void {    
    this.subscribeTo = this.vendorAccountService.sentResetLink(email)
    .pipe(
      catchError((error)=> {
        switch (error.status) {
          case 404: {
            this.isEmailRegistred = false;
            throw error;
          }
          case 400: {
            this.isEmailSent = true;
            this.isEmailResend = true;
            this.isEmailReceived = false;
            throw error;
          }
        }
        throw error;
        })
    )
    .subscribe((res: any) => {
      if (res.email) {
        this.isEmailSent = true;
        this.isEmailResend = true;
        this.counter = 9;
        this.startTimer();
        let emailArray = email.split('');
        for(let i = 2; i < email.indexOf('@'); i++) {
          emailArray[i] = '*';
        }
        this.encryptedEmail = emailArray.join('');
      } else {
        this.isEmailSent = true;
        this.isEmailResend = true;
        this.isEmailReceived = false;       
      }
    })
  }

  public startTimer() {
    this.counter = 9;
    this.intervalId = setInterval(() => {
      this.counter--;
    }, 1000)

    this.timeOutId = setTimeout(() => {
      clearInterval(this.intervalId);
      this.isEmailResend = false
    }, 9000);
  }

  public goToPreviousStep(): void{
    clearInterval(this.intervalId);
    clearTimeout(this.timeOutId);
    this.isEmailSent = false;
    this.isEmailReceived = true;
  }

  public resendEmail(email: string): void{
    this.sentResetLink(email)
    this.counter = 9; 
    this.isEmailResend = true;
  } 
}