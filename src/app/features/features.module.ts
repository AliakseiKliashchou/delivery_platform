import { ClipboardModule } from '@angular/cdk/clipboard';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MAT_ICON_DEFAULT_OPTIONS, MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';

import { TranslateModule } from '@ngx-translate/core';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsModule } from '@ngxs/store';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NgxMaskModule } from 'ngx-mask';
import { OrderModule } from 'ngx-order-pipe';
import { NgxTippyModule } from 'ngx-tippy-wrapper';
import { MultiSelectModule } from 'primeng/multiselect';

import { MapModule } from '../shared/components/map/map.module';
import { CommonState } from '../shared/ngxs/common.state';
import { PrimengModule } from '../shared/primeng.module';
import { SharedModule } from '../shared/shared.module';
import { ArchiveProductDialogComponent } from './archive-product-dialog/archive-product-dialog.component';
import { BannerTopComponent } from './banner-top/banner-top.component';
import { CartComponent } from './cart/cart.component';
import { ProductsService } from './cart/ngxs/products.service';
import { ConfirmPhoneComponent } from './confirm-phone-modal/confirm-phone.component';
import { CreateNewPasswordComponent } from './create-new-password/create-new-password.component';
import { CustomerAccountComponent } from './customer/customer-account/customer-account.component';
import { CustomerAccountHistoryPageComponent } from './customer/customer-account-history-page/customer-account-history-page.component';
import { CustomerAccountProfilePageComponent } from './customer/customer-account-profile-page/customer-account-profile-page.component';
import { FeatureRoutingModule } from './features-routing.module';
import { OftenOrderedProductComponent } from './home-page/components/often-ordered-product/often-ordered-product.component';
import { OftenOrderedProductsComponent } from './home-page/components/often-ordered-products/often-ordered-products.component';
import { HomePageComponent } from './home-page/home-page.component';
import { OftenOrderedProductsService } from './home-page/often-ordered-products.service';
import { LocationModalComponent } from './location/location-modal/location-modal.component';
import { LoginAttemptsComponent } from './login-attempts/login-attempts.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ModeratorAccountComponent } from './moderator/moderator-account/moderator-account.component';
import { ModeratorAccountAccountComponent } from './moderator/moderator-account-account/moderator-account-account.component';
import { ModeratorAccountFeedbackComponent } from './moderator/moderator-account-feedback/moderator-account-feedback.component';
import { ModeratorAccountHotlineComponent } from './moderator/moderator-account-hotline/moderator-account-hotline.component';
import { ModeratorAccountSettingsComponent } from './moderator/moderator-account-settings/moderator-account-settings.component';
import { ModeratorAccountSidebarComponent } from './moderator/moderator-account-sidebar/moderator-account-sidebar.component';
import { ModeratorAccountSupportComponent } from './moderator/moderator-account-support/moderator-account-support.component';
import { ModeratorAccountUserComponent } from './moderator/moderator-account-user/moderator-account-user.component';
import { ModeratorAccountVendorsComponent } from './moderator/moderator-account-vendors/moderator-account-vendors.component';
import { ModeratorSignInComponent } from './moderator/moderator-sign-in/moderator-sign-in.component';
import { ModeratorState } from './moderator/ngxs/moderator.state';
import { OnModerationState } from './moderator/ngxs/on-moderation.state';
import { ProductOnModerationComponent } from './moderator/product-on-moderation/product-on-moderation.component';
import { OrderDetailsPageComponent } from './order-details-page/order-details-page.component';
import { GroupByPipe } from './pipes/group-by.pipe';
import { RefuseRegistrationDialogComponent } from './refuse-registration-dialog/refuse-registration-dialog.component';
import { RemoveProductDialogComponent } from './remove-product-dialog/remove-product-dialog.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SearchFilterComponent } from './search-result/search-filter/search-filter.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { SignUpPageComponent } from './sign-up-page/sign-up-page.component';
import { AddNewProductPageComponent } from './vendor-account/add-new-product-page/add-new-product-page.component';
import { CreateReportModalComponent } from './vendor-account/create-report-modal/create-report-modal.component';
import { InventoryModalComponent } from './vendor-account/inventory-modal/inventory-modal.component';
import { ReportDateModalComponent } from './vendor-account/report-date-modal/report-date-modal.component';
import { ReportDateRangeModalComponent } from './vendor-account/report-date-range-modal/report-date-range-modal.component';
import { ShiftPopUpComponent } from './vendor-account/shift-pop-up/shift-pop-up.component';
import { StatusHistoryComponent } from './vendor-account/status-history/status-history.component';
import { VendorAccountPageComponent } from './vendor-account/vendor-account-page/vendor-account-page.component';
import { VendorAccountPageOrdersComponent } from './vendor-account/vendor-account-page-orders/vendor-account-page-orders.component';
import { VendorAccountPageProductsComponent } from './vendor-account/vendor-account-page-products/vendor-account-page-products.component';
import { VendorAccountPageSidebarComponent } from './vendor-account/vendor-account-page-sidebar/vendor-account-page-sidebar.component';
import { VendorEditProductPageComponent } from './vendor-account/vendor-edit-product-page/vendor-edit-product-page.component';
import { VendorPageComponent } from './vendor-page/vendor-page.component';
import { VendorsComponent } from './vendors/vendors.component';
import { VendorsFilterComponent } from './vendors/vendors-filter/vendors-filter.component';
import { VendorsGridComponent } from './vendors/vendors-grid/vendors-grid.component';
import { VerifyPhoneComponent } from './verify-phone-modal/verify-phone.component';
import { WrongCodeComponent } from './wrong-code/wrong-code.component';
import { ProductState } from 'src/app/features/cart/ngxs/product.state';

@NgModule({
  declarations: [
    VendorsComponent,
    VendorsFilterComponent,
    VendorsGridComponent,
    BannerTopComponent,
    VendorPageComponent,
    LocationModalComponent,
    CartComponent,
    SearchResultComponent,
    OrderDetailsPageComponent,
    OftenOrderedProductsComponent,
    HomePageComponent,
    SearchFilterComponent,
    SignUpPageComponent,
    LoginPageComponent,
    ConfirmPhoneComponent,
    VerifyPhoneComponent,
    WrongCodeComponent,
    VendorAccountPageComponent,
    VendorAccountPageProductsComponent,
    VendorAccountPageSidebarComponent,
    VendorAccountPageOrdersComponent,
    ResetPasswordComponent,
    CreateNewPasswordComponent,
    RefuseRegistrationDialogComponent,
    AddNewProductPageComponent,
    RemoveProductDialogComponent,
    ArchiveProductDialogComponent,
    ModeratorAccountComponent,
    LoginAttemptsComponent,
    VendorEditProductPageComponent,
    ModeratorAccountSidebarComponent,
    StatusHistoryComponent,
    GroupByPipe,
    ModeratorSignInComponent,
    ModeratorAccountHotlineComponent,
    ModeratorAccountFeedbackComponent,
    ModeratorAccountVendorsComponent,
    ModeratorAccountUserComponent,
    ModeratorAccountAccountComponent,
    ModeratorAccountSettingsComponent,
    ModeratorAccountSupportComponent,
    ProductOnModerationComponent,
    OftenOrderedProductsComponent,
    OftenOrderedProductComponent,
    InventoryModalComponent,
    ShiftPopUpComponent,
    CustomerAccountProfilePageComponent,
    CustomerAccountHistoryPageComponent,
    CustomerAccountComponent,
    CreateReportModalComponent,
    ReportDateModalComponent,
    ReportDateRangeModalComponent
  ],
  imports: [
    PrimengModule,
    CommonModule,
    SharedModule,
    MatMenuModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    TranslateModule.forChild({ extend: true }),
    MapModule,
    MatRadioModule,
    FeatureRoutingModule,
    MatCheckboxModule,
    NgxMaskModule.forChild(),
    MatExpansionModule,
    MatIconModule,
    AngularSvgIconModule.forRoot(),
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatTooltipModule,
    MatDialogModule,
    MatChipsModule,
    MatAutocompleteModule,
    FormsModule,
    NgxsModule.forRoot([ProductState, ModeratorState, OnModerationState, CommonState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    OrderModule,
    NgxTippyModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    ClipboardModule,
    MultiSelectModule
  ],
  providers: [
     MatPaginatorIntl,
    ProductsService,
    OftenOrderedProductsService,
    {
      provide: MAT_ICON_DEFAULT_OPTIONS,
      useValue: '../../assets/images/moderation/arrow-down.svg'
    }
  ],
  exports: [
    VendorsComponent,
    BannerTopComponent,
    VendorsFilterComponent,
    VendorPageComponent,
    SearchResultComponent,
    OrderDetailsPageComponent,
    TranslateModule
  ]
})
export class FeaturesModule {
}
