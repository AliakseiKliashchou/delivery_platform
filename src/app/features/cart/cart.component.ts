import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/overlay';
import { Router } from '@angular/router';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

import { IProduct } from 'src/app/core/models/product.model';
import { ApiService } from 'src/app/core/services/api.service';
import { DeleteOrderComponent } from 'src/app/shared/components/delete-order/delete-order.component';
import { ProductState } from './ngxs/product.state';
import { ConfirmPhoneComponent } from '../confirm-phone-modal/confirm-phone.component';
import { LocationModalComponent } from '../location/location-modal/location-modal.component';
import { getModalConfig } from '../../shared/utils/getModalConfig';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  @Select(ProductState.getProductCart) productCart$!: Observable<IProduct[]>;

  @Select(ProductState.getTotalPrice) totalPrice$!: Observable<number>;

  public openDialog(
    component: ComponentType<
      DeleteOrderComponent | ConfirmPhoneComponent
    > = DeleteOrderComponent
  ): void {
    this.dialog.closeAll();
    this.dialog.open(
      component,
      getModalConfig(500, 200, 'app-delete-dialog', { isCart: true })
    );
  }

  constructor(
    private dialog: MatDialog,
    private _apiService: ApiService,
    private cookieService: CookieService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._apiService.get(`/api/shopping_cart`);
  }

  public goToOrderDetailsPage(): void {
    if (!this.cookieService.get('hsf-account-customer-access-token')) {
      this.dialog.open(
        ConfirmPhoneComponent,
        getModalConfig(500, 292, 'app-delete-dialog', { isCart: true })
      );
    } else {
      if (!this.cookieService.get('hsf-account-address')) {
        this.dialog.open(
          LocationModalComponent,
          getModalConfig(800, 600, 'app-no-padding-dialog', { cart: true })
        );
      } else {
        this.router.navigate(['/feed/order']);
        this.dialog.closeAll();
      }
    }
  }

}
