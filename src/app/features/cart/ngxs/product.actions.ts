import { ICustomerLocation, IOrderData, IOrderDetails, IProduct } from 'src/app/core/models/product.model';
import { IDietaryTag, IVendorsFilter } from 'src/app/core/models/vendor.model';
import {
  CustomerProfileData
} from '../../customer/customer-account-profile-page/customer-account-profile-page.component';

export class SetOrderDetails {
  static readonly type = '[Product] SetOrderDetails';

  constructor(public orderDetails: Omit<IOrderDetails, 'customerLocationId'>) {}
}

export class UpdateCustomerProfileData {
  static readonly type = '[Product] UpdateCustomerProfileData';

  constructor(public customerData: CustomerProfileData) {}
}

export class GetProductsByVendorsId {
  static readonly type = '[Product] Get';

    constructor(public id: string | undefined, public dietaryTags: IDietaryTag | string) {}
}

export class GetProductsByProductName {
  static readonly type = '[Product] GetResult';

  constructor(public filterData?: IVendorsFilter | any) {}
}

export class AddProductToCart {
  static readonly type = '[Product] Add';

  constructor(public product: IProduct) {}
}

export class RemoveProductFromCart {
  static readonly type = '[Product] Remove';

  constructor(public product: IProduct) {}
}

export class RemoveAllProductsFromCart {
  static readonly type = '[Product] RemoveAll';

  constructor() {}
}

export class IncreaseProductQuantity {
  static readonly type = '[Product] IncreaseProductQuantity';

  constructor(public product: IProduct) {}
}

export class DecreaseProductQuantity {
  static readonly type = '[Product] DecreaseProductQuantity';

  constructor(public product: IProduct) {}
}
export class GetProductHistory {
  static readonly type = '[Product] GetProductHistory';

  constructor(public id: number) {}
}

export class SetCustomerLocation {
  static readonly type = '[Product] SetCustomerLocation';

  constructor(public location: ICustomerLocation) {}
}

export class OrderProducts {
  static readonly type = '[Product] OrderProducts';

  constructor(public orderData: IOrderData) {}
}

export class GetFoodGroups {
  static readonly type = '[FoodGroups] GetFoodGroups';

  constructor() {}
}

export class SetActiveFoodGroups {
  static readonly type = '[FoodGroups] SetActiveFoodGroups';

  constructor(public activeFoodGroups: string[] | undefined) {}
}

export class UpdateFoodGroups {
  static readonly type = '[FoodGroups] UpdateFoodGroups';

  constructor(public id: number) {}
}

export class GetDietaryTags {
  static readonly type = '[DietaryTags] GetDietaryTags';

  constructor() {}
}

export class SetActiveDietaryTags {
  static readonly type = '[DietaryTags] SetActiveDietaryTags';

  constructor(public activeDietaryTags: string[] | undefined) {}
}

export class UpdateDietaryTags {
  static readonly type = '[DietaryTags] UpdateDietaryTags';

  constructor(public id: number) {}
}

export class GetAllowedTags {
  static readonly type = '[AllowedTags] GetAllowedTags';

  constructor() {}
}

export class CustomerLogout {
  static readonly type = '[Customer] Logout';

  constructor() {}
}
