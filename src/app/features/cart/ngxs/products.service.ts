import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ICartItem, IProduct, IVendorData, } from 'src/app/core/models/product.model';
import { IDietaryTag } from 'src/app/core/models/vendor.model';
import { ApiService } from 'src/app/core/services/api.service';
import { IStatusHistoryData } from '../../../core/models/history.model';

export const defaultHttpOptions = {
  headers: new HttpHeaders({
    Accept: '*/*',
  }),
  withCredentials: true,
};

export interface IAddProductToCart {
  shopping_cart: IShoppingCart;
}

export interface IShoppingCart {
  cart_total_price: number;
  cart_total_quantity: number;
  cart_total_save: number;
  cart_total_weight: number;
  customer_id: number | null;
  products: ICartItem[];
}

export interface IDeleteCartResponce {
  success: boolean;
  error: string[];
}

@Injectable({
  providedIn: 'root',
})

export class ProductsService {
  constructor(private _apiService: ApiService) {
  }

  public getProductsByVendorsId(
    id: string | undefined,
    dietaryTags: IDietaryTag | string
  ): Observable<IVendorData> {
    return this._apiService.get(
      `api/accounts/vendors/${id}/get_products/?dietary_tags=${dietaryTags}`
    );
  }

  public addProductToCart(product: IProduct): Observable<IAddProductToCart> {
    return this._apiService.post(
      `api/shopping_cart/add_product_to_cart/`,
      {
        product_id: product.id,
        product_quantity: product.quantity! + 1,
      },
      defaultHttpOptions
    );
  }

  public deleteProductFromCart(
    product: IProduct
  ): Observable<IAddProductToCart> {
    return this._apiService.put(
      `api/shopping_cart/delete_product_from_cart/${product.id}/`,
      {},
      defaultHttpOptions
    );
  }

  public updateProductQuantity(
    product: IProduct,
    quantity: number
  ): Observable<IAddProductToCart> {
    return this._apiService.put(
      `api/shopping_cart/update_product_quantity/`,
      {
        product_quantity: quantity,
        product_id: product.id,
      },
      defaultHttpOptions
    );
  }

  public deleteShoppingCart(): Observable<IDeleteCartResponce> {
    return this._apiService.delete(
      `api/shopping_cart/`,
      defaultHttpOptions);
  }

  public getHistory(id: number): Observable<IStatusHistoryData[]> {
    return this._apiService.get(`api/products/history/${id}`);
  }
}
