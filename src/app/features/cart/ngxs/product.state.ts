import { Injectable } from '@angular/core';

import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Observable, of, tap } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { IStatusHistoryData } from '../../../core/models/history.model';
import { AllowedTags } from '../../../shared/interfaces/allowed-tags.interface';
import { IFoodGroupStatus } from '../../../shared/interfaces/food-group-status.interface';
import { IFoodGroups } from '../../../shared/interfaces/product-lists.interface';
import { VendorProductsService } from '../../../shared/services/vendor-products.service';
import {
  AddProductToCart,
  CustomerLogout,
  DecreaseProductQuantity,
  GetAllowedTags,
  GetDietaryTags,
  GetFoodGroups,
  GetProductHistory,
  GetProductsByProductName,
  GetProductsByVendorsId,
  IncreaseProductQuantity,
  OrderProducts,
  RemoveAllProductsFromCart,
  RemoveProductFromCart,
  SetActiveDietaryTags,
  SetActiveFoodGroups,
  SetCustomerLocation,
  SetOrderDetails,
  UpdateCustomerProfileData,
  UpdateDietaryTags,
  UpdateFoodGroups,
} from './product.actions';
import { IAddProductToCart, IDeleteCartResponce, ProductsService, } from './products.service';
import {
  ICartItem,
  IOrderBody,
  IOrderDetails,
  IProduct,
  IProducts,
  IVendorData,
} from 'src/app/core/models/product.model';
import { IVendorItem, IVendorsList } from 'src/app/core/models/vendor.model';
import { ApiService } from 'src/app/core/services/api.service';
import { ILocationResponse } from 'src/app/shared/interfaces/location-response.interface';
import { UserLocationService } from 'src/app/shared/services/user-location.service';
import { VendorsService } from 'src/app/shared/services/vendors.service';
import { removeCookie } from 'src/app/shared/utils/removeCookie';

export interface ProductStateModel {
  vendorData: IVendorData;
  products: IProducts;
  productCart: ICartItem[];
  totalPrice: number;
  vendorsArr: IVendorItem[];
  orderDetails: IOrderDetails;
  selectedProduct?: IStatusHistoryData[];
  food_groups: IFoodGroupStatus[];
  dietary_tags: IFoodGroupStatus[];
  dietaryTagsIds: number[];
  allowedTags: AllowedTags;
}

@State<ProductStateModel>({
  name: 'products',
  defaults: {
    vendorData: {
      average_rate: 0,
      flag_status: null,
      id: 0,
      license_number: '',
      locations: [],
      name: '',
      orders_count: 0,
      products: {},
      star_status: 0,
      schedule: {},
    },
    products: {},
    productCart: [],
    totalPrice: 0,
    vendorsArr: [],
    orderDetails: {
      customerId: undefined,
      customerName: '',
      customerPhone: '',
      customerEmail: '',
      customerComments: '',
      customerLocation: [],
      customerLocationId: undefined,
    },
    selectedProduct: [],
    food_groups: [],
    dietary_tags: [],
    dietaryTagsIds: [],
    allowedTags: {},
  },
})
@Injectable()
export class ProductState {

  constructor(
    private readonly _productsService: ProductsService,
    private readonly _vendorsService: VendorsService,
    private readonly _userLocationService: UserLocationService,
    private readonly _apiService: ApiService,
    private readonly _vendorProductsService: VendorProductsService,
  ) {
  }

  @Selector()
  static getOrderDetails(state: ProductStateModel): IOrderDetails {
    return state.orderDetails;
  }

  @Selector()
  static getProductCart(state: ProductStateModel): ICartItem[] {
    return state.productCart;
  }

  @Selector()
  static getTotalPrice(state: ProductStateModel): number {
    return state.totalPrice;
  }

  @Selector()
  static getVendorDataByVendorsId(state: ProductStateModel): IVendorData {
    return state.vendorData;
  }

  @Selector()
  static getVendorDataByProductName(state: ProductStateModel): IVendorItem[] {
    return state.vendorsArr;
  }

  @Selector()
  static getProductsByVendorsId(state: ProductStateModel): IProducts {
    return state.products;
  }

  @Selector()
  static getProductHistory(state: ProductStateModel): IStatusHistoryData[] {
    return state.selectedProduct || [];
  }

  @Selector()
  static getFoodGroups(state: ProductStateModel): IFoodGroupStatus[] {
    return state.food_groups || [];
  }

  @Selector()
  static getDietaryTags(state: ProductStateModel): IFoodGroupStatus[] {
    return state.dietary_tags || [];
  }

  @Selector()
  static getAllowedTags(state: ProductStateModel): AllowedTags {
    return state.allowedTags || {};
  }

  @Action(GetFoodGroups)
  getFoodGroups(
    { patchState }: StateContext<ProductStateModel>
  ) {
    return this._vendorProductsService.getFoodGroupList().pipe(
      tap(({ food_groups = [] }: IFoodGroups) => {
        const foodGroups = [...food_groups.map(({ status, isDisabled, ...rest }) => (
          { status: false, isDisabled: false, ...rest }
        ))];

        patchState({
          food_groups: foodGroups,
        });
      }),
      catchError((err) => {
        throw(err);
      })
    );
  }

  @Action(UpdateFoodGroups)
  updateFoodGroups(
    { getState, patchState }: StateContext<ProductStateModel>,
    { id }: UpdateFoodGroups
  ): void {

    const { food_groups: foodGroups, dietary_tags: dietaryTags, allowedTags } = getState();
    const foodGroupsCopy = [...foodGroups];
    const dietaryTagsCopy = dietaryTags.map(({ status, isDisabled, ...rest }) => (
      { status: false, isDisabled: false, ...rest }
    ));
    const foodGroupToUpdate = foodGroupsCopy.find(({ id: currentId }) => currentId === id);

    if (foodGroupToUpdate) {
      foodGroupToUpdate.status = !foodGroupToUpdate.status;
    }

    const updatedDietaryTagsCopy = this.updateDietaryTagsCopy(dietaryTagsCopy, allowedTags, foodGroupsCopy);

    patchState({
      food_groups: foodGroupsCopy,
      dietary_tags: updatedDietaryTagsCopy,
    });
  }

  @Action(SetActiveFoodGroups)
  setActiveFoodGroups(
    { patchState, getState }: StateContext<ProductStateModel>,
    { activeFoodGroups }: SetActiveFoodGroups
  ): void {
    if (!activeFoodGroups) {
      return;
    }

    const { food_groups: foodGroups, dietary_tags: dietaryTags, allowedTags } = getState();
    const foodGroupsCopy = foodGroups.map(group => {
      if (activeFoodGroups.includes(group.name)) {
        return {
          ...group,
          status: true
        };
      }

      return group;
    });

    const dietaryTagsCopy = this.updateDietaryTagsCopy(dietaryTags, allowedTags, foodGroupsCopy);

    patchState({
      food_groups: foodGroupsCopy,
      dietary_tags: dietaryTagsCopy,
    });
  }

  private updateDietaryTagsCopy(dietaryTags: IFoodGroupStatus[], allowedTags: AllowedTags, foodGroupsCopy: IFoodGroupStatus[]): IFoodGroupStatus[] {
    let dietaryTagsCopy: IFoodGroupStatus[] = JSON.parse(JSON.stringify(dietaryTags));

    Object.entries(allowedTags).map(([tagId, allowedList]) => {
      Object.entries(foodGroupsCopy.filter(f => f.status)).map(([, foodValue]) => {
        if (!allowedList.includes(foodValue.name)) {
          dietaryTagsCopy = dietaryTagsCopy.map((item) => {
            if (item.id !== +tagId) return item;

            return { ...item, status: false, isDisabled: true };
          });
        }
      });
    });

    return dietaryTagsCopy;
  }

  @Action(GetDietaryTags)
  getDietaryTags(
    { patchState, dispatch }: StateContext<ProductStateModel>
  ) {
    return this._vendorProductsService.getDietaryTypeList().pipe(
      tap(({ dietary_tags = [] }) => {
        const dietaryTags = dietary_tags.map(({ status, isDisabled, ...rest }) => (
          { status: false, isDisabled: false, ...rest }
        ));
        const dietaryTagsIds = dietary_tags.map(({ id }) => id);

        patchState({
          dietary_tags: dietaryTags,
          dietaryTagsIds: dietaryTagsIds,
        });
        //set initial state
        dispatch(new GetAllowedTags());
      }),
      catchError((err) => {
        throw(err);
      })
    );
  }

  @Action(UpdateDietaryTags)
  updateDietaryTags(
    { getState, patchState }: StateContext<ProductStateModel>,
    { id }: UpdateDietaryTags
  ): void {
    const state = getState();
    const { dietary_tags: dietaryTags } = state;
    let dietaryTagsCopy = JSON.parse(JSON.stringify(dietaryTags));
    let dietaryTagsToUpdate = [...dietaryTagsCopy].find(({ id: currentId }) => currentId === id);

    if (dietaryTagsToUpdate) {
      dietaryTagsToUpdate.status = !dietaryTagsToUpdate.status;
    }

    patchState({
      dietary_tags: dietaryTagsCopy
    });
  }

  @Action(SetActiveDietaryTags)
  setActiveDietaryTags(
    { patchState, getState }: StateContext<ProductStateModel>,
    { activeDietaryTags }: SetActiveDietaryTags
  ) : void {
    if (!activeDietaryTags) {
      return;
    }

    const state = getState();
    const { dietary_tags: dietaryTags } = state;
    const dietaryTagsCopy: IFoodGroupStatus[] = dietaryTags.map(tag => {
      if (activeDietaryTags.includes(tag.name)) {
        return {
          ...tag,
          status: true
        };
      }

      return tag;
    });

    patchState({
      dietary_tags: dietaryTagsCopy
    });
  }

  @Action(GetAllowedTags)
  getAllowedTags(
    { getState, patchState }: StateContext<ProductStateModel>
  ) {
    const state = getState();

    return this._vendorProductsService.getAllowedFoodGroupsForTags(state.dietaryTagsIds).pipe(
      tap((allowedFoodGroups) => {
        const allowedTags = state.dietaryTagsIds.reduce((acc, tag, index) => (
          { ...acc, [tag]: [...allowedFoodGroups[index].allowed_food_groups] }
        ), {});

        patchState({
          allowedTags: allowedTags,
        });
      }),

      catchError((err) => {
        throw(err);
      })
    );
  }

  @Action(GetProductHistory)
  getProductHistory(
    { patchState }: StateContext<ProductStateModel>,
    { id }: GetProductHistory
  ) {
    return this._productsService.getHistory(id).pipe(
      tap((returnData: IStatusHistoryData[]) => {

        patchState({
          selectedProduct: returnData,
        });
      })
    );
  }

  @Action(SetCustomerLocation)
  setCustomerLocation(
    { getState, patchState }: StateContext<ProductStateModel>,
    { location }: SetCustomerLocation,
  ): Observable<ILocationResponse | undefined> {
    const state = getState();

    return this._userLocationService.setLocation(state?.orderDetails?.customerId, location)
      .pipe(
        tap((res: ILocationResponse | undefined) => {
          if (res) {
            patchState({
              orderDetails: {
                ...state.orderDetails,
                customerLocationId: res.location_id,
              }
            });
          }
        })
      );
  }

  @Action(SetOrderDetails)
  setOrderDetails(
    { getState, patchState }: StateContext<ProductStateModel>,
    { orderDetails }: SetOrderDetails
  ): void {
    const state = getState();

    patchState({
      orderDetails: {
        ...orderDetails,
        customerLocationId: state.orderDetails.customerLocationId,
      }
    });
  }

  @Action(UpdateCustomerProfileData)
  updateCustomerProfileData(
    { patchState, getState }: StateContext<ProductStateModel>,
    { customerData }: UpdateCustomerProfileData
  ): void {

    const { orderDetails } = getState();

    patchState({
      orderDetails: {
        ...orderDetails,
        customerName: customerData.customer_name,
        customerEmail: customerData.email,
      }
    });
  }

  @Action(CustomerLogout)
  customerLogout(
    { patchState, getState }: StateContext<ProductStateModel>,
  ): void {
    const state = getState();
    const { orderDetails: orderDetails } = state;
    let orderDetailsCopy = JSON.parse(JSON.stringify(orderDetails));
    orderDetailsCopy.customerId = undefined;

    removeCookie('hsf-account-customer-access-token');
    removeCookie('hsf-account-customer-refresh-token');

    patchState({
      ...state,
      orderDetails: orderDetailsCopy,
    });
  }

  @Action(GetProductsByProductName)
  getProductsByProductName(
    { getState, patchState }: StateContext<ProductStateModel>,
    { filterData }: GetProductsByProductName
  ): Observable<IVendorsList | null> {
    return this._vendorsService.getVendors(filterData).pipe(
      tap((result: IVendorsList) => {
        const vendorsArr = result.vendors;
        const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));
        let productCart: ICartItem[] = [];
        const state = getState();
        if (state) {
          productCart = [...state.productCart];
        }

        if (productCart.length) {
          vendorsArrCopy.forEach((vendor: IVendorItem) => {
            vendor.products?.forEach((product: IProduct) => {
              productCart.forEach((cartItem: ICartItem) => {
                if (product.id === cartItem.id) {
                  product.quantity = cartItem.quantity;
                }
              });
            });
          });
        }

        patchState({
          vendorsArr: vendorsArrCopy,
        });
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }

  @Action(GetProductsByVendorsId)
  getProductsByVendorsId(
    { getState, patchState }: StateContext<ProductStateModel>,
    { id, dietaryTags }: GetProductsByVendorsId
  ): Observable<IVendorData | null> {
    return this._productsService.getProductsByVendorsId(id, dietaryTags).pipe(
      tap((result: IVendorData) => {
        const state = getState();
        const vendorData = result;
        const productCart = [...state.productCart];
        const products = result.products;
        const productsCopy = JSON.parse(JSON.stringify(products));

        if (!!productCart.length) {
          for (let key in productsCopy) {
            productsCopy[key].forEach((product: IProduct) => {
              productCart.forEach((cartItem: ICartItem) => {
                if (product.id === cartItem.id) {
                  product.quantity = cartItem.quantity;
                }
              });
            });
          }
        }

        patchState({
          vendorData,
          products: productsCopy,
        });
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }

  @Action(AddProductToCart)
  addProductToCart(
    { getState, patchState }: StateContext<ProductStateModel>,
    { product }: AddProductToCart
  ): Observable<IAddProductToCart | null> {
    return this._productsService.addProductToCart(product).pipe(
      tap((shoppingCartResult: IAddProductToCart) => {
        const state = getState();
        const productCart = shoppingCartResult.shopping_cart.products;
        const totalPrice = shoppingCartResult.shopping_cart.cart_total_price;
        const products = { ...state.products };
        const productsCopy = JSON.parse(JSON.stringify(products));
        const vendorsArr = [...state.vendorsArr];
        const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));

        if (productCart.length) {
          for (let key in productsCopy) {
            productsCopy[key].forEach((productData: IProduct) => {
              productCart.forEach((cartItem: ICartItem) => {
                if (productData.id === cartItem.id) {
                  productData.quantity = cartItem.quantity;
                }
              });
            });
          }

          vendorsArrCopy.forEach((vendor: IVendorItem) => {
            vendor.products?.forEach((productData: IProduct) => {
              productCart.forEach((cartItem: ICartItem) => {
                if (productData.id === cartItem.id) {
                  productData.quantity = cartItem.quantity;
                }
              });
            });
          });
        }

        patchState({
          ...state,
          productCart,
          products: productsCopy,
          totalPrice,
          vendorsArr: vendorsArrCopy,
        });
      }),
      catchError((error) => {
        return of(null);
      })
    );
  }

  @Action(RemoveAllProductsFromCart)
  removeAllProductsFromCart({
                              getState,
                              patchState,
                            }: StateContext<ProductStateModel>): Observable<IDeleteCartResponce> {
    return this._productsService.deleteShoppingCart().pipe(
      tap((res: IDeleteCartResponce) => {
        const state = getState();
        let products = { ...state.products };
        let productCart = [...state.productCart];
        let totalPrice = state.totalPrice;
        let vendorsArr = [...state.vendorsArr];
        const productsCopy = JSON.parse(JSON.stringify(products));
        const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));
        productCart.length = 0;
        totalPrice = 0;

        for (let key in productsCopy) {
          productsCopy[key].forEach((product: IProduct) => {
            product.quantity = 0;
          });
        }

        vendorsArrCopy.forEach((vendor: IVendorItem) => {
          vendor.products?.forEach((product: IProduct) => {
            product.quantity = 0;
          });
        });

        localStorage.clear();

        patchState({
          ...state,
          productCart,
          totalPrice,
          products: productsCopy,
          vendorsArr: vendorsArrCopy,
        });
      }),
      catchError((err) => {
        throw err;
      })
    );
  }

  @Action(RemoveProductFromCart)
  removeProductFromCart(
    { getState, patchState }: StateContext<ProductStateModel>,
    { product }: RemoveProductFromCart
  ): Observable<IAddProductToCart> {
    return this._productsService.deleteProductFromCart(product).pipe(
      tap((shoppingCartResult: IAddProductToCart) => {
        const state = getState();
        let products = { ...state.products };
        const productCart = shoppingCartResult
          ? shoppingCartResult.shopping_cart.products
          : [];
        const totalPrice = shoppingCartResult
          ? shoppingCartResult.shopping_cart.cart_total_price
          : 0;
        const productsCopy = JSON.parse(JSON.stringify(products));
        const vendorsArr = [...state.vendorsArr];
        const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));

        if (!productCart.length) {
          localStorage.clear();
        }

        for (let key in productsCopy) {
          productsCopy[key].forEach((item: IProduct) => {
            if (item.id === product.id) {
              item.quantity = 0;
            } else {
              item = item;
            }
          });
        }

        vendorsArrCopy.forEach((vendor: IVendorItem) => {
          vendor.products?.forEach((item: IProduct) => {
            if (item.id === product.id) {
              item.quantity = 0;
            } else {
              item = item;
            }
          });
        });

        patchState({
          ...state,
          productCart,
          products: productsCopy,
          totalPrice,
          vendorsArr: vendorsArrCopy,
        });
      }),
      catchError((err) => {
        throw(err);
      })
    );
  }

  @Action(IncreaseProductQuantity)
  increaseProductQuantity(
    { getState, patchState }: StateContext<ProductStateModel>,
    { product }: IncreaseProductQuantity
  ): Observable<IAddProductToCart> {
    return this._productsService
      .updateProductQuantity(product, product.quantity! + 1)
      .pipe(
        tap((shoppingCartResult: IAddProductToCart) => {
          const state = getState();
          let products = { ...state.products };
          const productCart = shoppingCartResult.shopping_cart.products;
          const totalPrice = shoppingCartResult.shopping_cart.cart_total_price;
          const productsCopy = JSON.parse(JSON.stringify(products));
          const vendorsArr = [...state.vendorsArr];
          const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));

          for (let key in productsCopy) {
            productsCopy[key].forEach((item: IProduct) => {
              if (item.id === product.id) {
                item.quantity = item.quantity! + 1;
              } else {
                item = item;
              }
            });
          }

          if (productCart.length) {
            vendorsArrCopy.forEach((vendor: IVendorItem) => {
              vendor.products?.forEach((productData: IProduct) => {
                productCart.forEach((cartItem: ICartItem) => {
                  if (productData.id === cartItem.id) {
                    productData.quantity = cartItem.quantity;
                  }
                });
              });
            });
          }

          patchState({
            ...state,
            productCart,
            products: productsCopy,
            totalPrice,
            vendorsArr: vendorsArrCopy,
          });
        }),
        catchError((err) => {
          throw err;
        })
      );
  }

  @Action(DecreaseProductQuantity)
  decreaseProductQuantity(
    { getState, patchState }: StateContext<ProductStateModel>,
    { product }: DecreaseProductQuantity
  ): Observable<IAddProductToCart> {
    return this._productsService
      .updateProductQuantity(product, product.quantity! - 1)
      .pipe(
        tap((shoppingCartResult: IAddProductToCart) => {
          const state = getState();
          let products = { ...state.products };
          const productCart = shoppingCartResult.shopping_cart.products;
          const totalPrice = shoppingCartResult.shopping_cart.cart_total_price;
          const productsCopy = JSON.parse(JSON.stringify(products));
          const vendorsArr = [...state.vendorsArr];
          const vendorsArrCopy = JSON.parse(JSON.stringify(vendorsArr));

          for (let key in productsCopy) {
            productsCopy[key].forEach((item: IProduct) => {
              if (item.id === product.id) {
                item.quantity = item.quantity! - 1;
              } else {
                item = item;
              }
            });
          }

          if (productCart.length) {
            vendorsArrCopy.forEach((vendor: IVendorItem) => {
              vendor.products?.forEach((productData: IProduct) => {
                productCart.forEach((cartItem: ICartItem) => {
                  if (productData.id === cartItem.id) {
                    productData.quantity = cartItem.quantity;
                  }
                });
              });
            });
          }

          patchState({
            ...state,
            productCart,
            products: productsCopy,
            totalPrice,
            vendorsArr: vendorsArrCopy,
          });
        }),
        catchError((err) => {
          throw err;
        })
      );
  }

  @Action(OrderProducts)
  orderProducts(
    { dispatch, getState }: StateContext<ProductStateModel>,
    { orderData }: OrderProducts
  ): Observable<any> {
    const state = getState();

    const orderBody: IOrderBody = {
      ...orderData,
      customer_id: state.orderDetails.customerId!,
      order_location_id: state.orderDetails.customerLocationId!,
      products_quantity: {},
      total_price: Number(state.totalPrice),
      vendor_id: Number(state.vendorData.id),
    };

    state.productCart.forEach((item: ICartItem) => {
      orderBody.products_quantity[item.id] = { quantity: item.quantity };
    });

    return this._apiService
      .post(`api/order/`, orderBody)
      .pipe(
        tap(() => {
          dispatch(new RemoveAllProductsFromCart());
        }),
      );
  }
}
