import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import {
  CustomerAccountService,
  CustomerDataVerificationResponse
} from '../../shared/services/customer-account.service';
import { getModalConfig } from '../../shared/utils/getModalConfig';
import { LocationModalComponent } from '../location/location-modal/location-modal.component';
import { CookieService } from 'ngx-cookie-service';

export interface DialogData {
  phone: string;
  isCart?: boolean;
}

@Component({
  selector: 'app-verify-phone',
  templateUrl: './verify-phone.component.html',
  styleUrls: ['./verify-phone.component.scss'],
})
export class VerifyPhoneComponent implements OnInit {

  public phone!: string;
  public counter!: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private cookieServ: CookieService,
    private readonly customerAccountService: CustomerAccountService,
    private readonly _router: Router,
    private dialog: MatDialog,
  ) {
  }

  public ngOnInit(): void {
    this.phone = `+${this.data.phone.slice(0, 4)} *** ** ${this.data.phone.slice(-3)}`;
  }

  public setCustomerData(response: CustomerDataVerificationResponse) {
    this.customerAccountService.setAccessToken(response.access);
    this.customerAccountService.setRefreshToken(response.refresh);
    this.customerAccountService.setCustomerData();

    if (this.customerAccountService.getIsAccountAddress()) {
      this.data.isCart === false
        ? this.customerAccountService.navigateToFeed()
        : this.customerAccountService.navigateToOrder();
    } else {
      !this.data.isCart
        ? this.dialog.closeAll()
        : this.dialog.open(
          LocationModalComponent,
          getModalConfig(800, 600, 'app-no-padding-dialog', {cart: true})
        );
    }
  }
}
