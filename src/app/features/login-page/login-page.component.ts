import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { catchError, throwError } from 'rxjs';

import { IProductData } from "../../shared/interfaces/product-from-list.interface";
import { VendorLoginDialogComponent } from 'src/app/shared/components/vendor-login-dialog/vendor-login-dialog.component';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';
import {
  signUpResponse,
  VendorAccountService,
} from 'src/app/shared/services/vendor-account.service';

export interface IVendorAccountData {
  meta: IMetaData;
  user: IUserData;
}

export interface IUserData {
  id: number;
  products: IProductData[];
}

export interface IMetaData {
  page: number;
  per_page: number;
  total_pages: number;
  total_records: number;
}

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent extends UnsubscribeComponent {
  public loginAndPassword: FormGroup = new FormGroup({
    login: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required),
    remember: new FormControl(false),
  });
  public hide: boolean = true;
  public isCredentialsWrong: boolean = false;

  constructor(
    private dialog: MatDialog,
    private location: Location,
    private vendorAccountService: VendorAccountService,
    private _router: Router,
    private cookieService: CookieService,
    private localstorage: LocalstorageService
  ) {
    super();
  }

  public goBack(): void {
    this.location.back();
  }

  public openDialog() {
    this.dialog.open(VendorLoginDialogComponent, {
      width: '450px',
      panelClass: 'app-vendor-login-dialog',
    });
  }

  public loginVendor(email: string, password: string): void {
    if (this.loginAndPassword.valid) {
      let loginData = {
        email,
        password,
        user_type: 'vendor',
      };

      this.subscribeTo = this.vendorAccountService
        .loginVendor(loginData)
        .pipe(
          catchError((error) => {
            this.isCredentialsWrong = true;
            throw error;
          })
        )
        .subscribe((res: signUpResponse) => {
          if (res.access) {
            this.cookieService.set(
              'hsf-account-vendor-access-token',
              res.access,
              undefined,
              '/'
            );
            this.loginAndPassword.controls['remember'].value &&
              this.cookieService.set(
                'hsf-account-vendor-refresh-token',
                res.refresh!,
                undefined,
                '/'
              );
            this.subscribeTo = this.vendorAccountService
              .getVendorData()
              .subscribe((resp: IVendorAccountData) => {
                this._router.navigate([
                  `/feed/vendor/vendor-account/${resp.user.id}/products`,
                ]);
                this.localstorage.setValue(
                  'vendor_id', resp.user.id.toString()
                );
              });
          } else {
            throwError;
          }
        });
    } else {
      this.loginAndPassword.markAllAsTouched();
    }
  }
}
