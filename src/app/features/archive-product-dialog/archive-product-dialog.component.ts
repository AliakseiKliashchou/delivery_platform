import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AgroexToastService } from "ngx-agroex-toast";

import { TOAST_CONFIG } from "../../shared/constants/toast-messages";
import { ProductStatusEnum } from "../../shared/enums/product-status.enum";
import { VendorProductsService } from "../../shared/services/vendor-products.service";

@Component({
  selector: 'app-archive-product-dialog',
  templateUrl: './archive-product-dialog.component.html',
  styleUrls: ['./archive-product-dialog.component.scss'],
})
export class ArchiveProductDialogComponent {
  constructor(
    private vendorProductsService: VendorProductsService,
    private toastService: AgroexToastService,
    @Inject(MAT_DIALOG_DATA) public data: { productId: number }
  ) {};

  public archiveProduct() {
    this.vendorProductsService.updateProductStatus(
      this.data.productId,
      { status: ProductStatusEnum.Archived }
    )
      .subscribe({
        next: () => {
          this.toastService.addToast(TOAST_CONFIG.forProduct.success.archived);
        },
        error: () => {
          this.toastService.addToast(TOAST_CONFIG.byDefault.error);
        }
      });
  }
}
