import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveProductDialogComponent } from './archive-product-dialog.component';

describe('ArchiveProductDialogComponent', () => {
  let component: ArchiveProductDialogComponent;
  let fixture: ComponentFixture<ArchiveProductDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchiveProductDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ArchiveProductDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
