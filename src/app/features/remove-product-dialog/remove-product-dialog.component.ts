import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { AgroexToastService } from "ngx-agroex-toast";

import { VendorProductsService } from "../../shared/services/vendor-products.service";
import { TOAST_CONFIG } from "../../shared/constants/toast-messages";

@Component({
  selector: 'app-remove-product-dialog',
  templateUrl: './remove-product-dialog.component.html',
  styleUrls: ['./remove-product-dialog.component.scss'],
})
export class RemoveProductDialogComponent {
  constructor(
    private vendorProductsService: VendorProductsService,
    private toastService: AgroexToastService,
    @Inject(MAT_DIALOG_DATA) public data: { productId: number }
  ) {};

  public removeProduct() {
    this.vendorProductsService.removeProduct(this.data.productId)
      .subscribe({
        next: () => {
          this.toastService.addToast(TOAST_CONFIG.forProduct.success.removed);
          },
        error: () => {
          this.toastService.addToast(TOAST_CONFIG.byDefault.error);
        }
      });
  }
}
