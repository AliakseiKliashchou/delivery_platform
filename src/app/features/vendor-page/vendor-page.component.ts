import { AfterViewInit, Component, HostListener, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { Select, Store } from "@ngxs/store";
import { Observable } from "rxjs";

import { IProducts, IVendorData } from "../../core/models/product.model";
import { UnsubscribeComponent } from "../../shared/directives/unsubscribe.component";
import { GetProductsByVendorsId } from "../cart/ngxs/product.actions";
import { ProductState } from "../cart/ngxs/product.state";
import { IVendor, ScheduleList } from "src/app/core/models/vendor.model";

@Component({
  selector: "app-vendor-page",
  templateUrl: "./vendor-page.component.html",
  styleUrls: ["./vendor-page.component.scss"],
})
export class VendorPageComponent extends UnsubscribeComponent implements OnInit, AfterViewInit {
  public vendorData: IVendor = {
    id: 0,
    name: "",
    license_number: "",
    locations: [],
    average_rate: 0,
    flag_status: null,
    star_status: 0,
    products: [],
    orders_count: 0,
    schedule: {},
  };

  public vendorsSchedule: ScheduleList = {};
  public vendorSchedule$!: Observable<ScheduleList>;
  public isStoreOpen: boolean = true;
  public isClosesIn30Minutes: boolean = false;
  public reviews: number = 0;
  public dietaryOptions: { title: string; img: string }[] = [
    //temporary until have contract with backend
    { title: "Vegan", img: "Vegan" },
    { title: "Lactose Free", img: "Lactose Free" },
    { title: "Gluten Free", img: "Gluten Free" },
  ];

  public dietaryControl = new FormControl("");
  public filterForm!: FormGroup;

  public list: Array<keyof IProducts> = [];
  public activeItem: string = this.list[0];

  private id: string | undefined = document.location.href.split("/").pop();

  public onSelectItem(item: string): void {
    const itemYOffset = document.getElementById(item)!.getBoundingClientRect()?.top + window.pageYOffset - 150;

    window.scrollTo({ top: itemYOffset, behavior: "smooth" });
    this.activeItem = item;
  }

  private enrollmentSection!: HTMLElement | null;

  constructor(
    private readonly store: Store,
  ) {

    super();
  }

  public ngAfterViewInit(): void {
    window.scrollTo(0, 0);
  }

  @Select(ProductState.getProductsByVendorsId) products$!: Observable<IProducts>;
  @Select(ProductState.getVendorDataByVendorsId) vendorData$!: Observable<IVendorData>;

  ngOnInit(): void {
    this.store.dispatch(new GetProductsByVendorsId(this.id, ""));
    this.initFilters();
    this.subscribeTo = this.filterForm.valueChanges.subscribe(({ dietaryTags }) => {
      if (dietaryTags.indexOf("All") === 0) {
        dietaryTags.shift();
      }
      this.store.dispatch(new GetProductsByVendorsId(this.id, dietaryTags));
    });

    this.subscribeTo = this.products$.subscribe((res: IProducts) => {
      this.list = Object.keys(res) as Array<keyof IProducts>;
      this.activeItem = this.list[0];
    });

    this.subscribeTo = this.vendorData$.subscribe((res => {
      this.vendorsSchedule = res.schedule as ScheduleList;
    }));
  }

  @HostListener("window:scroll", ["$event"])
  reactOnDocumentScroll() {
    this.list.forEach((el) => {
      this.enrollmentSection = document.getElementById(el.replace(/\s/g, ""));
      if (this.enrollmentSection) {
        if (
          window.screenY >= this.enrollmentSection.getBoundingClientRect().top - 170 &&
          window.screenY < this.enrollmentSection.getBoundingClientRect().bottom - 170
        ) {
          this.activeItem = el;
        } else if (window.screenY > this.enrollmentSection.getBoundingClientRect().bottom - 170) {
          this.activeItem = el;
        }
      }
    });
  }

  public initFilters(): void {
    this.filterForm = new FormGroup({
      dietaryTags: new FormControl(""),
    });
  }
}
