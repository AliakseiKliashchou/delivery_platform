import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer-account',
  templateUrl: './customer-account.component.html',
  styleUrls: ['./customer-account.component.scss']
})
export class CustomerAccountComponent {
  //todo add logic to calculate progress in profile-page component and input here
  public userInfoProgress: number = 66;

  //todo wait until backend method related to promocode on reviews
  public promoCode: string = 'promocode';
  public leavedReviews: number = 0;
  public orderFromLastStoreName: string = 'Domino’s';
  public reviewsCount: number = 40;

  //todo bug: switch between personal -> order -> personal not renders personal UI correctly
}
