import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAccountHistoryPageComponent } from './customer-account-history-page.component';

describe('CustomerAccountHistoryPageComponent', () => {
  let component: CustomerAccountHistoryPageComponent;
  let fixture: ComponentFixture<CustomerAccountHistoryPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerAccountHistoryPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerAccountHistoryPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
