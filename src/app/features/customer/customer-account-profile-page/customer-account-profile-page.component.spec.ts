import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAccountProfilePageComponent } from './customer-account-profile-page.component';

describe('CustomerAccountProfilePageComponent', () => {
  let component: CustomerAccountProfilePageComponent;
  let fixture: ComponentFixture<CustomerAccountProfilePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerAccountProfilePageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerAccountProfilePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
