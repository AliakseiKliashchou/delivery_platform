import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { IOrderDetails } from '../../../core/models/product.model';
import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { CustomerAccountService } from '../../../shared/services/customer-account.service';
import { getModalConfig } from '../../../shared/utils/getModalConfig';
import { ProductState } from '../../cart/ngxs/product.state';
import { LocationModalComponent } from '../../location/location-modal/location-modal.component';

export interface CustomerProfileData {
  customer_name: string;
  email: string;
}

export interface Address {
  string: string;
}

@Component({
  selector: 'app-customer-account-profile-page',
  templateUrl: './customer-account-profile-page.component.html',
  styleUrls: ['./customer-account-profile-page.component.scss']
})
export class CustomerAccountProfilePageComponent extends UnsubscribeComponent implements OnInit  {

  public customerDataGroup!: FormGroup;

  public customerId!: number;
  public customerPhone: string = '';
  public customerComments: string = '';
  public customerEmail: string = '';
  public customerName: string = '';
  public customerLocation: string[] = [];
  public customerLocationId: number = 0;

  public agreeToReceiveInfo: boolean = true;
  public isEdiMenuVisible: boolean = false;

  @Select (ProductState.getOrderDetails) orderDetails$!: Observable<IOrderDetails>;

  constructor(
    private readonly customerAccountService: CustomerAccountService,
    private readonly dialog: MatDialog,
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribeTo = this.setCustomerInitialData();
    this.initNewUserForm();
  }

  //todo add type
  private setCustomerInitialData() {
    return this.orderDetails$.subscribe((customerData) => {
      this.customerId = customerData.customerId || 0;
      this.customerPhone = customerData.customerPhone || '';
      this.customerComments = customerData.customerComments || '';
      this.customerEmail = customerData.customerEmail || '';
      this.customerName = customerData.customerName || '';
      this.customerLocation = customerData.customerLocation || [];
      //todo add "agreeToReceive"
    });
  }

  private initNewUserForm(): void {
    this.customerDataGroup = new FormGroup({
      customerName: new FormControl('', ),
      customerEmail: new FormControl('', [Validators.email]),
      customerLocation: new FormControl(),
      // todo add agreeToReceive: new FormControl(),
    });
  }

  public updateUserData(): void {
    if (this.customerDataGroup.dirty) {
      const userData = this.getFormGroupData();
      this.customerAccountService.updateUserData(this.customerId, userData);
      this.customerDataGroup.reset();
    }
  }

  public getFormGroupData(): CustomerProfileData {
    return {
      customer_name: this.customerDataGroup.get('customerName')?.value || this.customerName,
      email: this.customerDataGroup.get('customerEmail')?.value || this.customerEmail
      // todo add agreeToReceive
    };
  }

  deleteAccount() {
    //todo
    // make popup "are you sure?"
    // wait until backend method appears /api/accounts/customer/delete
  }

  public openMapDialog(): void {
    this.dialog.open(
      LocationModalComponent,
      getModalConfig(800, 600, 'app-no-padding-dialog')
    );
  }

  public changeCustomerNumber() {
    // wait until backend method
  }

  public updatePhoto(): void {
    // Logic to open system window for choosing a file and updating the photo
  }

  public removePhoto(): void {
    // Logic to show removal confirmation and delete the photo from the Database and UI
  }

  public showPhotoEditMenu(): void {
    this.isEdiMenuVisible = !this.isEdiMenuVisible;
  }

  public discardChanges(): void {
    this.customerDataGroup.reset();
  }
}
