import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { VisibilityService } from 'src/app/shared/services/visibility-service';
import { CookieService } from 'ngx-cookie-service';
import { LocationModalComponent } from '../location/location-modal/location-modal.component';

@Component({
  selector: 'app-banner-top',
  templateUrl: './banner-top.component.html',
  styleUrls: ['./banner-top.component.scss'],
})

export class BannerTopComponent implements OnInit {
  public searchForm!: FormGroup;
  public isBannerVisible: boolean = true;

  constructor(
    private dialog: MatDialog,
    private visibilityService: VisibilityService,
    private cookieService: CookieService
  ) {}

  public ngOnInit(): void {
    this.initFilters();

    //need to add a cookies check for new roles, such as courier, moderator, etc after they appears
    !this.cookieService.get('hsf-account-address') && this.openDialog();
    (
      !!this.cookieService.get('hsf-account-customer-access-token')
      || !!this.cookieService.get('hsf-account-vendor-access-token')
      || !!this.cookieService.get('hsf-account-vendor_moderator-access-token')
      || !this.isBannerVisible
    )
      ? this.isBannerVisible = false
      : this.isBannerVisible = true;
  }

  public openDialog(): void {
    this.dialog.open(LocationModalComponent, {
      width: '800px',
      height: '600px',
      panelClass: 'app-no-padding-dialog',
    });
  }

  public onVisible(): void {
    this.visibilityService.updateVisibility('visible');
  }

  public onHidden(): void {
    this.visibilityService.updateVisibility('hidden');
  }

  public initFilters(): void {
    this.searchForm = new FormGroup({
        'search': new FormControl('')
      }
    );
  }

  public onCloseBanner(): void {
    this.isBannerVisible = false;
  }
}
