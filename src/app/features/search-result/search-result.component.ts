import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

import { IVendorItem } from 'src/app/core/models/vendor.model';
import { UnsubscribeComponent } from '../../shared/directives/unsubscribe.component';
import { VendorsService } from '../../shared/services/vendors.service';
import { GetProductsByProductName } from '../cart/ngxs/product.actions';
import { ProductState } from '../cart/ngxs/product.state';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss'],
})

export class SearchResultComponent extends UnsubscribeComponent implements OnInit {
  public productName: string = '';
  public vendorsArr: IVendorItem [] = [];
  public productNumber: number = 0;
  public productsExpanded: boolean = false;
  public itemsToShow: number = 6;
  public allVendorsShown: boolean = false;

  public distance: string = (Math.random() * 5).toFixed(1);

  constructor(private _vendorsService: VendorsService,
              private store: Store,
              private route: ActivatedRoute) {
    super();
  }

  @Select(ProductState.getVendorDataByProductName) vendorsArr$!: Observable<IVendorItem[]>;

  public ngOnInit(): void {
    this.initializeProduct();
    this.store.dispatch(new GetProductsByProductName({ product_name: this.productName }));

    this.subscribeTo = this.vendorsArr$
      .subscribe((res: IVendorItem[]) => {
        this.productNumber = 0;
        res?.forEach((element: IVendorItem) => {
          if (element.products?.length) {
            this.productNumber = this.productNumber + element.products?.length;
          }
        });
      });
  }

  public initializeProduct(): void {
    this.subscribeTo = this.route.url.subscribe(url => {
      const lastSegment = url[url.length - 1].path;
      if (lastSegment === 'search') {
        this.productName = '';
      } else {
        this.productName = lastSegment.split('%20').join(' ')
      }
    })
  }

  public onFilterChanged(filter: any) {
    if (filter.dietary_tags?.includes('All')) {
      filter.dietary_tags = [];
    }

    this.productName = filter.filter_name;
    this.store.dispatch(new GetProductsByProductName({
      product_name: filter.filter_name,
      dietary_tags: filter.dietary_tags,
      order: filter.order
    }));
    this.productName = filter.filter_name;
    this.subscribeTo = this.vendorsArr$
      .subscribe((res: IVendorItem[]) => {
        this.productNumber = 0;
        res?.forEach((element: IVendorItem) => {
          if (element.products?.length) {
            this.productNumber = this.productNumber + element.products?.length;
          }
        });
      });

    this.itemsToShow = 6;
    this.allVendorsShown = false;
  }

  public loadAllVendors(): void {
    this.itemsToShow = this.vendorsArr.length;
    this.allVendorsShown = true;
  }
}
