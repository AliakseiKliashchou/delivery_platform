import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { debounceTime } from 'rxjs';

import { IMultiselectOption } from '../../../shared/components/dropdown-multiselect/dropdown-multiselect.component';
import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss']
})
export class SearchFilterComponent extends UnsubscribeComponent implements OnInit {

  @Input() searchQuery = '';
  @Output() filterChanged = new EventEmitter<any>();
  public filterForm!: FormGroup;

  public dietaryOptions: IMultiselectOption[] = [
    { title: 'Vegan',
    img: 'Vegan'
    },
    { title: 'Lactose Free',
      img: 'Lactose Free'
    },
    { title: 'Gluten Free',
      img: 'Gluten Free'
    }];

  public sortOptions: { title: string, value: string }[] = [ //temporary until have contract with backend

    {
      title: 'The nearest',
      value: 'nearest'
    },
    {
      title: 'The most popular',
      value: 'most_popular'
    },
    {
      title: 'The most reviewed',
      value: 'most_reviewed'
    }
  ];


  public ngOnInit(): void {
    this._initFilters();
    this.subscribeTo = this.filterForm.valueChanges.pipe(debounceTime(500))
      .subscribe((value) => {
        this.filterChanged.emit(value);
      });
  }

  private _initFilters(): void {
    this.filterForm = new FormGroup({
        'filter_name': new FormControl(this.searchQuery),
        'dietary_tags': new FormControl(''),
        'order': new FormControl('most_popular')
      }
    );
  }
}
