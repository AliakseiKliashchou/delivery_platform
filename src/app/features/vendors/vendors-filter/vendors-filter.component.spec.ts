import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsFilterComponent } from './vendors-filter.component';

describe('VendorsFilterComponent', () => {
  let component: VendorsFilterComponent;
  let fixture: ComponentFixture<VendorsFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorsFilterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
