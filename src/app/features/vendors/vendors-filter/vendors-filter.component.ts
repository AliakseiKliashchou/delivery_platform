import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { debounceTime } from 'rxjs';

import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { IMultiselectOption } from '../../../shared/components/dropdown-multiselect/dropdown-multiselect.component';

@Component({
  selector: 'app-vendors-filter',
  templateUrl: './vendors-filter.component.html',
  styleUrls: ['./vendors-filter.component.scss']
})
export class VendorsFilterComponent extends UnsubscribeComponent implements OnInit {

  @Output() filterChanged = new EventEmitter<any>();
  public filterForm!: FormGroup;
  public foodGroups: string[] = ['Meat', 'Fruit', 'Fast Food', 'Fish', 'Vegetables', 'Seafood', 'Smoothies', 'Confection'];

  public dietaryOptions: IMultiselectOption[] = [
    {
      title: 'Vegan',
      img: 'Vegan'
    },
    {
      title: 'Lactose Free',
      img: 'Lactose Free'
    },
    {
      title: 'Gluten Free',
      img: 'Gluten Free'
    }];


  public sortOptions: { title: string, value: string }[] = [ //temporary until have contract with backend

    {
      title: 'The nearest',
      value: 'nearest'
    },
    {
      title: 'The most popular',
      value: 'most_popular'
    },
    // {
    //   title: 'The most reviewed',
    // }
    // this will be updated when backend provides list of options
    ];

  public ngOnInit(): void {
    this.initFilters();
    this.subscribeTo = this.filterForm.valueChanges.pipe(debounceTime(500))
      .subscribe((value) => {
      this.filterChanged.emit(value);
    });
  }

  public initFilters(): void {
    this.filterForm = new FormGroup({
        'food_group': new FormControl(''),
        'dietary_tags': new FormControl(''),
        'order': new FormControl('')
      }
    );
  }
}
