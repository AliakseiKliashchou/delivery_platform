import { Injectable } from '@angular/core';

import { Action, Selector, State, StateContext, StateToken } from '@ngxs/store';
import { tap } from 'rxjs';

import { IOrdersList, IVendorsFilter } from '../../../core/models/vendor.model';
import { VendorsService } from '../../../shared/services/vendors.service';
import { GetVendorOrders, GetVendors, VendorsStateModel } from './vendors.actions';

export const VENDORS_STATE_TOKEN = new StateToken<VendorsStateModel>('vendors');

@State<VendorsStateModel>({
  name: VENDORS_STATE_TOKEN,
  defaults: {
    vendorsList: [],
    vendorsLoaded: false,
    vendorOrders: {
      meta: {
        page: 0,
        total_pages: 0,
        total_records: 0,
        per_page: 0
      },
      orders: []
    }
  },
})
@Injectable()
export class VendorsState {

  constructor(private vendorsService: VendorsService) {}

  @Selector()
  static getVendorOrders(state: VendorsStateModel): IOrdersList {
    return state.vendorOrders;
  }

  @Action(GetVendors)
  getVendors(
    { patchState }: StateContext<VendorsStateModel>,
    { food_group, dietary_tags, order }: IVendorsFilter
  ) {
    return this.vendorsService
      .getVendors({ food_group, dietary_tags, order })
      .pipe(
        tap((vendorsResult) => {
          patchState({
            vendorsList: vendorsResult.vendors,
            vendorsLoaded: true,
          });
        })
      );
  }

  @Action(GetVendorOrders)
  getVendorOrders(
    { patchState }: StateContext<VendorsStateModel>,
    { filter, vendorId }: GetVendorOrders
  ) {
    return this.vendorsService
      .getVendorOrders(filter, vendorId)
      .pipe(
        tap((ordersResult) => {
          patchState({
            vendorOrders: ordersResult
          });
        })
      );
  }
}
