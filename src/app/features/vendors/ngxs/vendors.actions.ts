import { IOrdersFilter, IOrdersList, IVendorItem } from '../../../core/models/vendor.model';

export interface VendorsStateModel {
  vendorsList: IVendorItem[];
  vendorsLoaded: boolean;
  vendorOrders: IOrdersList;
}

export class GetVendors {
  static readonly type = '[Vendors] GetVendors';

  constructor
  (
    public food_group?: string,
    public dietary_tags?: string[],
    public order?: string,
  ) {}
}

export class GetVendorOrders {
  static readonly type = '[Orders] GetVendorOrders';

  constructor
  (
    public filter: IOrdersFilter,
    public vendorId: number,
  ) {}
}
