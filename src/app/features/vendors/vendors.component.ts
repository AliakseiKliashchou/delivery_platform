import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { VENDORS_STATE_TOKEN } from './ngxs/vendors.state';
import { GetVendors, VendorsStateModel } from './ngxs/vendors.actions';
import { IVendorItem, IVendorsFilter } from '../../core/models/vendor.model';
import { UnsubscribeComponent } from '../../shared/directives/unsubscribe.component';

@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent extends UnsubscribeComponent implements OnInit {

  @Select(VENDORS_STATE_TOKEN) vendors$!: Observable<VendorsStateModel>;
  public vendors: IVendorItem[] = [];
  public vendorsLoaded: boolean = false;

  constructor(private _store: Store) {
    super();
  }

  public ngOnInit() : void {
    this._store.dispatch(new GetVendors());

    this.subscribeTo = this.vendors$.subscribe((res: VendorsStateModel) => {
      if(res) {
        this.vendors = res.vendorsList;
        this.vendorsLoaded = res.vendorsLoaded;
      }
    });
  }

  public onFilterChanged(filter: IVendorsFilter) {

    if (filter.dietary_tags?.includes('All')) {
      filter.dietary_tags = [];
    }
    this._store.dispatch(new GetVendors( filter.food_group, filter.dietary_tags, filter.order ));
  }
}
