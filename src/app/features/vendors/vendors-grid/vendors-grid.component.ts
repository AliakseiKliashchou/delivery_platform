import { Component, Input } from '@angular/core';

import { IVendorItem } from '../../../core/models/vendor.model';

@Component({
  selector: 'app-vendors-grid',
  templateUrl: './vendors-grid.component.html',
  styleUrls: ['./vendors-grid.component.scss']
})
export class VendorsGridComponent {
  @Input() vendors: IVendorItem[] = [];
}
