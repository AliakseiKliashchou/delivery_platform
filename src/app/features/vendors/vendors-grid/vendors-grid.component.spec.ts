import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorsGridComponent } from './vendors-grid.component';

describe('VendorsGridComponent', () => {
  let component: VendorsGridComponent;
  let fixture: ComponentFixture<VendorsGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorsGridComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
