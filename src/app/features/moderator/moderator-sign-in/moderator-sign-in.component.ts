import { AfterViewInit, Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { Store } from '@ngxs/store';
import { CookieService } from 'ngx-cookie-service';
import { tap } from 'rxjs/operators';
import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { LoginAttemptsComponent } from '../../login-attempts/login-attempts.component';
import { Login } from '../ngxs/moderator.action';

import { HfsToken, UserData } from '../ngxs/moderator.model';
import { ModeratorService } from '../ngxs/moderator.service';

@Component({
  selector: 'app-moderator-sign-in',
  templateUrl: './moderator-sign-in.component.html',
  styleUrls: ['./moderator-sign-in.component.scss']
})
export class ModeratorSignInComponent extends UnsubscribeComponent implements AfterViewInit {
  public loginData!: UserData;
  public loginAndPassword: FormGroup = new FormGroup({
    login: new FormControl('', [Validators.required]),
    password: new FormControl('', Validators.required),
    remember: new FormControl(false),
  });
  public hide: boolean = true;
  public isCredentialsWrong: boolean = false;
  public invalidedAttemptCounter: number = 0;

  constructor(
    private moderatorService: ModeratorService,
    private cookieService: CookieService,
    private fb: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    private readonly store: Store) {
    super();
  }

  ngAfterViewInit(): void {
    window.scrollTo(0, 0);
  }

  public loginModerator(email: string, password: string): void {
    this.isCredentialsWrong = false;
    this.invalidedAttemptCounter++;
    if (this.invalidedAttemptCounter === 3) {
      this.dialog.open(LoginAttemptsComponent, {
        width: '450px',
        height: '350px',
        panelClass: 'app-no-padding-dialog',
      });
    }
    if (this.loginAndPassword.valid) {
      this.loginData = {
        email,
        password,
        user_type: 'vendor_moderator'
      };
      this.store.dispatch(new Login(this.loginData)).pipe(
        tap((responce: HfsToken) => {
          if (this.loginAndPassword.value.remember) {
            this.cookieService.set('hsf-account-vendor_moderator-refresh-token', responce.authModerator.hsf_account_vendor_moderator_refresh_token!, undefined, '/');
          }
        })
      ).subscribe();
      this.subscribeTo = this.store.dispatch(new Login(this.loginData)).subscribe(
        () => {
          this.router.navigate([`/feed/moderation/moderator-account/hotline`]);
        }
      );
    }
  }
}
