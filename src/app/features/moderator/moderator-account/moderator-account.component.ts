import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { ModeratorService } from "./../ngxs/moderator.service";
import { UnsubscribeComponent } from "src/app/shared/directives/unsubscribe.component";
import { removeCookie } from "src/app/shared/utils/removeCookie";

@Component({
  selector: "app-moderator-account",
  templateUrl: "./moderator-account.component.html",
  styleUrls: ["./moderator-account.component.scss"],
})
export class ModeratorAccountComponent extends UnsubscribeComponent {
  public removeCookie(event: any): void {
    removeCookie('hsf-account-vendor_moderator-access-token');
    removeCookie('hsf-account-vendor_moderator-refresh-token');
    this.router.navigate([`/feed/moderation/sign-in`]);
  }

  constructor(private moderatorService: ModeratorService, private router: Router) {
    super();
  }
}
