import { Injectable } from '@angular/core';

import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { CookieService } from 'ngx-cookie-service';
import { tap } from 'rxjs/operators';

import { Login, Logout } from './moderator.action';
import { ModeratorService } from './moderator.service';

const DEFAULT_LOGIN_DATA = {
  hsf_account_vendor_moderator_access_token: '',
  hsf_account_vendor_moderator_refresh_token: '',
  email: '',
  password: '',
  user_type: '',
};

export interface AuthStateModel {
  hsf_account_vendor_moderator_access_token: string | undefined;
  hsf_account_vendor_moderator_refresh_token: string | undefined;
  email: string;
  password: string;
  user_type: string;
}

@State<AuthStateModel>({
  name: 'authModerator',
  defaults: {
    hsf_account_vendor_moderator_access_token: '',
    hsf_account_vendor_moderator_refresh_token: '',
    email: '',
    password: '',
    user_type: ''
  }
})
@Injectable()
export class ModeratorState {
  public loginData: AuthStateModel = { ...DEFAULT_LOGIN_DATA };

  @Selector()
  static isAuthenticated(state: AuthStateModel): boolean {
    return !!state.hsf_account_vendor_moderator_access_token;
  }

  @Selector()
  static token(state: AuthStateModel): string | undefined {
    return state.hsf_account_vendor_moderator_access_token;
  }

  @Selector()
  static role(state: AuthStateModel): string | undefined {
    return state.user_type;

  }

  constructor(
    private readonly store: Store,
    private readonly moderatorService: ModeratorService,
    private cookieService: CookieService) {
  }

  @Action(Login)
  login(ctx: StateContext<AuthStateModel>, action: Login) {
    return this.moderatorService.login(action.payload).pipe(
      tap((result) => {
          ctx.patchState({
            hsf_account_vendor_moderator_access_token: result.access,
            hsf_account_vendor_moderator_refresh_token: result.refresh,
            email: action.payload.email,
            password: action.payload.password,
            user_type: 'vendor_moderator'
          });
        }
      )
    );

  }


  @Action(Logout)
  logout({ patchState, getState }: StateContext<AuthStateModel>) {
    this.cookieService.delete('hsf-account-vendor_moderator-access-token');
    this.cookieService.delete('hsf-account-vendor_moderator-refresh-token');
    patchState({
      ...this.loginData
    });
  }
}
