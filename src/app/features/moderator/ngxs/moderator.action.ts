import { FormControlMap } from './moderator.model';

export class Login {
  static readonly type = '[Auth] Login Moderator';

  constructor(public payload: { email: string, password: string, user_type: string }) {
  }
}

export class Logout {
  static readonly type = '[Auth] Logout Moderator';
}

export class GetProductsOnModeration {
  static readonly type = '[Auth] Get Product On Moderation';

// eslint-disable-next-line max-len
  constructor(public name: string, public start_date: Date | string | undefined, public end_date: Date | string | undefined, public grouped: boolean, public page?: number, public per_page?: number, public max_per_page?: number, public id?: number) {
  }
}

export class GetOneProductOnModeration {
  static readonly type = '[Moderation] Get One Product On Moderation';

  constructor(public id: number) {
  }
}

export class GetReviewOnModeration {
  static readonly type = '[Moderation] Get Reviews On Moderation';

  constructor(public id: number, public idReview?: number) {

  }
}

export class PostReviewOnModeration {
  static readonly type = '[Moderation] Post Reviews On Moderation';

  constructor(public id: number | undefined, public history_record: FormControlMap, public field_flags: FormControlMap) {
  }
}

export class DeleteReviewOnModeration {
  static readonly type = '[Moderation] Delete Reviews On Moderation';

  constructor(public id: number) {
  }
}

export class UpdateReviewOnModeration {
  static readonly type = '[Moderation] Update Product On Moderation';

  constructor(public id: number, public history_record: FormControlMap, public field_flags: FormControlMap) {
  }
}

export class UpdateStatusOnModeration {
  static readonly type = '[Moderation] Update Status On Moderation';

  constructor(public id: number, public status?: string, public comment?: string) {
  }
}

export class GetQuantityAllProd {
  static readonly type = '[Moderation] Get Quantity All Prod';
}

export class ClearOnModerateReviewAction {
  static readonly type = '[Moderation] Clear OnModerate Review';
}
