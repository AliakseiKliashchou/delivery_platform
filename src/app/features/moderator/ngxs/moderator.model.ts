export interface UserData {
  email: string;
  password: string;
  user_type: string;
}

export interface SignUpResponseModerator {
  access: string;
  refresh?: string;
}

export interface HfsToken {
  authModerator: {
    hsf_account_vendor_moderator_access_token: string;
    hsf_account_vendor_moderator_refresh_token?: string;
  };

}

export interface IProdOnModeration {
  'success': true,
  'products': [
    {
      id: number | undefined;
    }
  ],
  products_grouped: [
    {}
  ],
  meta?: PaginationMetadata,
  oneProductOnModeration: IProductOnModeration | undefined;
}

export interface IProductOnModeration {
  id: number;
  name: string;
  weight: number;
  price: number;
  description: string;
  cooking_time: string;
  number_purchases: number;
  product_discount_percent: number;
  count: number;
  avg_rating: number;
  avg_page_rank: number;
  avg_good_rating: number;
  approval_status: string[];
  vendor_id: number;
  quantity: number;
  ingredients: string[];
  food_groups: string[];
  discounts: number[];
  dietary_tags: string[];
  comments: number[];
  image: string;
  updated_at: string;
  publish_date: string;
  vendor_name: string;
  assignee: string;
  moderatorColor?: string;
  meta?: PaginationMetadata;
  vendor: string;
  start_date: string;
  end_date: string;
  grouped?: string;
  metadata?: {};
  foodGroup: string;
  dietaryTag: string;
  discount: number;
  final_price: number;
  count_products: number;
  oneProductOnModeration: IProductOnModeration | undefined;
}

export interface PaginationMetadata {
  page: number;
  per_page: number;
  max_per_page?: number;
  total_records?: number;
}

export interface IMetaHotline {
  page: number;
  total_pages?: number;
  total_records?: number;
}

export interface IProductOneOnModeration {
  id: number;
  name?: string;
  weight: number;
  price: number;
  description: string;
  cooking_time?: string;
  number_purchases?: number;
  product_discount_percent?: number;
  count?: number;
  avg_rating?: number;
  avg_page_rank?: number;
  avg_good_rating?: number;
  approval_status?: string[];
  vendor_id?: number;
  quantity?: number;
  ingredients?: any[];
  food_groups?: string[];
  discounts?: any[];
  dietary_tags?: any[];
  comments?: number[];
  image?: null | string;
  updated_at?: string;
  publish_date?: null | string;
  moderator_id?: number;
  vendor_name?: string;
  assignee?: string;
  moderatorColor?: string[];
  count_products?: number;
  has_reviews?: boolean;
}

export interface IProductHistoryRecord {
  id: number;
  old_status: string;
  new_status: string;
  created_at: string;
  changed_by: string;
}

export interface IProductFieldFlags {
  errGeneral: string,
  errPhoto: string,
  errName: string,
  errWeight: string,
  errPrice: string,
  addManual: string,
}

export interface IProductReview {
  id: number;
  text?: null | string;
  field_flags: IProductFieldFlags;
  history_record: IProductHistoryRecord;
  created_at: string;
  product_id: number;
  moderator_id: number;
}

export interface FormControlMap {
  [key: string]: string;
}
