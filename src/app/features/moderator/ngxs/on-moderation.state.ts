import { Injectable } from '@angular/core';

import { Action, Selector, State, StateContext } from '@ngxs/store';
import { catchError, tap } from 'rxjs/operators';

import { IHotlineData } from '../moderator-account-hotline/moderator-account-hotline.component';
import { ClearOnModerateReviewAction, DeleteReviewOnModeration, GetOneProductOnModeration, GetProductsOnModeration, GetQuantityAllProd, GetReviewOnModeration, PostReviewOnModeration, UpdateReviewOnModeration, UpdateStatusOnModeration } from './moderator.action';
import { IProductOneOnModeration, IProductOnModeration, IProductReview, PaginationMetadata } from './moderator.model';
import { ModeratorService } from './moderator.service';


export interface IOnModerationState {
  success: boolean;
  products: IProductOnModeration[];
  products_grouped?: {
    [vendorName: string]: {}[];
  };
  meta: PaginationMetadata;
  oneProduct?: IProductOneOnModeration;
  review?: IProductReview[];
  idReview?: number;
}

@State<IOnModerationState>({
  name: 'onModeration',
  defaults: {
    success: false,
    products: [],
    products_grouped: {},
    meta: {
      page: 1,
      per_page: 5,
    },
    oneProduct: {
      id: 0,
      name: '',
      weight: 0,
      price: 0,
      description: '',
      cooking_time: '',
      number_purchases: 0,
      product_discount_percent: 0,
      count: 0,
      avg_rating: 0,
      avg_page_rank: 0,
      avg_good_rating: 0,
      approval_status: [],
      vendor_id: 0,
      quantity: 0,
      ingredients: [],
      food_groups: [],
      discounts: [],
      dietary_tags: [],
      comments: [],
    },
    review: [],
    idReview: 0
  }
})

@Injectable()
export class OnModerationState {
  @Selector()
  static success(state: IOnModerationState): boolean {
    return state.success;
  }

  @Selector()
  static products(state: IOnModerationState): IProductOnModeration[] | undefined {
    return state.products;
  }

  @Selector()
  static meta(state: IOnModerationState): PaginationMetadata {
    return state.meta;
  }

  @Selector([OnModerationState.products])
  static getHotlineDataByVendorFront(products: IHotlineData[]) {
    if (Array.isArray(products)) {
      return products.map((group: IHotlineData) => {
        return {
          vendor_name: 'vendors_name',
          ...group,
        };
      });
    } else {
      return products;
    }
  }

  @Selector()
  static getAssignees(state: IOnModerationState): IProductOnModeration[] | undefined {
    return Object.entries(state.products)
      .flatMap(([products]: any[]) => {
          return products.map(({ assignee }: any) => ({ assignee }));
        }
      );
  }

  @Selector()
  static products_grouped(state: IOnModerationState): { [p: string]: {}[] } | undefined {
    return state.products_grouped;
  }

  @Selector()
  static getStateSnapshot(state: IOnModerationState): IOnModerationState {
    return state;
  }

  @Selector()
  static getOneProductOnModeration(state: IOnModerationState): IProductOneOnModeration {
    return state.oneProduct as IProductOneOnModeration;
  }

  @Selector()
  static getReviewOnModeration(state: IOnModerationState): IProductReview[] | undefined {
    return state.review;
  }

  @Selector()
  static getIdReviewOnModeration(state: IOnModerationState): number {
    return <number>state.idReview;
  }
  @Selector()
  static deleteReviewOnModeration(state: IOnModerationState): IProductReview[] | undefined {
    return state.review;
  }

  @Selector()
  static updateReviewOnModeration(state: IOnModerationState): IProductReview[] | undefined {
    return state.review;
  }

  @Selector()
static getQuantityAllProd(state: IOnModerationState): number | undefined {
    return state.meta.total_records;
}

  constructor(private readonly moderatorService: ModeratorService) {
  }

  @Action(GetQuantityAllProd) getQuantityAllProd(ctx: StateContext<IOnModerationState>) {
    return this.moderatorService.getQuantityAllProd().pipe(
      tap((res) => {
        ctx.patchState({
          meta: res.meta,
        });
      })
    );
  }

  @Action(GetProductsOnModeration) getProductOnModeration(ctx: StateContext<IOnModerationState>, {
    name, start_date, end_date, grouped, page, per_page, max_per_page
  }: GetProductsOnModeration) {
    return this.moderatorService.getProductsOnModeration(name, start_date, end_date, grouped, page, per_page, max_per_page).pipe(
      tap((res) => {
        ctx.patchState({
          success: true,
          products: res.products as unknown as IProductOnModeration[] | undefined,
          products_grouped: res.products_grouped as unknown as {},
          meta: res.meta,
        });
      })
    );
  }

  @Action(GetOneProductOnModeration)
  getOneProductOnModeration(ctx: StateContext<IOnModerationState>, { id }: GetOneProductOnModeration) {
    return this.moderatorService.getOneProductOnModeration(id).pipe(
      tap((res: IProductOneOnModeration): void => {
        ctx.patchState({
          success: true,
          oneProduct: res,
        });
      })
    );
  }

  @Action(GetReviewOnModeration) getReviewOnModeration(ctx: StateContext<IOnModerationState>, {
    id, idReview
  }: GetReviewOnModeration) {
    return this.moderatorService.getReviewOnModeration(id).pipe(
      tap((res: IProductReview): void => {
        ctx.patchState({
          idReview: res.id,
          review: res.field_flags as unknown as IProductReview[]
        });
      }), catchError((err) => {
        ctx.patchState({
          review: []
        });

        return err;
      })
    );
  }

  @Action(PostReviewOnModeration) postReviewOnModeration(ctx: StateContext<IOnModerationState>, {
    id, history_record, field_flags
  }: PostReviewOnModeration) {
    return this.moderatorService.postReviewOnModeration(id, history_record, field_flags).pipe(
      tap((res: IProductReview): void => {
        ctx.patchState({
          review: res as unknown as IProductReview[]
        });
      })
    );
  }

  @Action(DeleteReviewOnModeration) deleteReviewOnModeration(ctx: StateContext<IOnModerationState>, {
    id
  }: DeleteReviewOnModeration) {
    return this.moderatorService.deleteReviewOnModeration(id);
  }

  @Action(UpdateReviewOnModeration) updateReviewOnModeration(ctx: StateContext<IOnModerationState>, {
    id, history_record, field_flags
  }: UpdateReviewOnModeration) {
    return this.moderatorService.updateReviewOnModeration(id, history_record, field_flags).pipe(
      tap((res: IProductReview): void => {
        ctx.patchState({
          success: true,
          review: res as unknown as IProductReview[]
        });
      })
    );
  }

  @Action(UpdateStatusOnModeration) updateStatusOnModeration(ctx: StateContext<IOnModerationState>, {
    id, status, comment
  }: UpdateStatusOnModeration) {
    return this.moderatorService.updateStatusOnModeration(id, status, comment).pipe(
      tap((res: IProductReview): void => {
        ctx.patchState({
          success: true,
          review: res as unknown as IProductReview[],
        });
      })
    );
  }
  @Action(ClearOnModerateReviewAction)
  clearOnModerateReview(ctx: StateContext<IOnModerationState>) {
    const state = ctx.getState();

    state.review = [];
    state.idReview = 0;
    ctx.patchState(state);
  }
}
