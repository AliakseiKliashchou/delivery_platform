import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ENDPOINTS } from '../../../shared/constants/endpoints';
import { IProdOnModeration, IProductOneOnModeration, IProductReview, SignUpResponseModerator, UserData } from './moderator.model';
import { ApiService } from 'src/app/core/services/api.service';

export const defaultHttpOptions = {
  headers: new HttpHeaders({
    // eslint-disable-next-line @typescript-eslint/naming-convention
    Accept: '*/*',
  }),
  withCredentials: true,
};

@Injectable({
  providedIn: 'root',
})
export class ModeratorService {
  public AUTH_TOKEN = 'hsf-account-vendor_moderator-access-token';
  public REFRESH_TOKEN = 'hsf-account-vendor_moderator-refresh-token';

  constructor(
    private apiService: ApiService,
    private cookieService: CookieService,
  ) {
  }

  public login(userData: UserData): Observable<SignUpResponseModerator> {
    return this.apiService
      .post(`${ENDPOINTS.login}`, { ...userData, user_type: 'vendor_moderator' }).pipe(
        tap((response: any) => {
          if (response.access) {
            this.cookieService.set(this.AUTH_TOKEN, response.access, undefined, '/');
          }
        })
      );
  }

  public logout(): any {
    this.cookieService.delete(this.AUTH_TOKEN);
    this.cookieService.delete(this.REFRESH_TOKEN);
  }

  public getProductsOnModeration(
    name: string,
    start_date: Date | string | undefined,
    end_date: Date | string | undefined,
    grouped: boolean,
    page: number | undefined,
    per_page: number | undefined,
    max_per_page?: number | undefined
  ): Observable<IProdOnModeration> {
    return this.apiService.get(
      // eslint-disable-next-line max-len
      `${ENDPOINTS.productsOnModeration}?name=${name}&start_date=${start_date}&end_date=${end_date}&grouped=${grouped}&page=${page}&per_page=${per_page}&max_per_page=${max_per_page}`);
  }

  public getOneProductOnModeration(id: number): Observable<IProductOneOnModeration> {
    return this.apiService.get(
      `${ENDPOINTS.productsOnModeration}/${id}`);
  }

  public getReviewOnModeration(id: number): Observable<IProductReview> {
    return this.apiService.get(
      `${ENDPOINTS.review}${id}`);
  }

  public postReviewOnModeration(id: number | undefined, history_record: any, field_flags: any): Observable<IProductReview> {

    return this.apiService.post(
      `${ENDPOINTS.review}${id}`, { history_record, field_flags });
  }

  public deleteReviewOnModeration(id: number): Observable<IProductReview> {
    return this.apiService.delete(
      `${ENDPOINTS.review}${id}`, {});
  }

  public updateReviewOnModeration(id: number, history_record: any, field_flags: any): Observable<IProductReview> {
    return this.apiService.put(
      `${ENDPOINTS.review}${id}`, { history_record, field_flags });
  }

  public updateStatusOnModeration(id: number, status?: string, comment?: string): Observable<IProductReview> {
    return this.apiService.put(
      `${ENDPOINTS.updateStatus}${id}`, { status, comment });
  }

  public getQuantityAllProd(): Observable<any> {
    return this.apiService.get(`${ENDPOINTS.productsOnModeration}`);
  }
}
