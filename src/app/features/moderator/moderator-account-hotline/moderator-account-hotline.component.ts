import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

import { Select, Store } from '@ngxs/store';
import format from 'date-fns/format';
import { clone } from 'lodash';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';

import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { IdSharedService } from '../../../shared/services/id-shared.service';
import { ModeratorMessageService } from '../../../shared/services/moderator-message.service';
import { GetProductsOnModeration } from '../ngxs/moderator.action';
import { IMetaHotline, IProductOnModeration } from '../ngxs/moderator.model';
import { OnModerationState } from '../ngxs/on-moderation.state';

export interface IHotlineData {
  name: string;
  vendor: string;
  assignee: string;
  start_date: string;
  end_date: string;
  grouped?: string;
  moderatorColor?: string;
  metadata?: {};
}

@Component({
  selector: 'app-moderator-account-hotline',
  templateUrl: './moderator-account-hotline.component.html',
  styleUrls: ['./moderator-account-hotline.component.scss']
})
export class ModeratorAccountHotlineComponent extends UnsubscribeComponent implements OnInit, AfterViewInit {
  @Select(OnModerationState.products) hotlineData$!: Observable<IProductOnModeration[]>;
  @Select(OnModerationState.meta) metadata$!: Observable<IMetaHotline>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild('inputField') inputField!: ElementRef;
  @ViewChild('selectModeratorInput') selectModeratorInput!: string | null;
  @ViewChild('select') matSelect!: MatSelect;
  public message: string = '';
  public displayedColumns: string[] = [
    'name',
    'vendor',
    'assignee',
    'last updated'
  ];
  public hotlineData!: IHotlineData[];
  public panelOpenState: boolean = false;
  public hotlineDataByVendorFront!: IHotlineData[];
  public dataSource: MatTableDataSource<IHotlineData> = new MatTableDataSource<IHotlineData>([]);
  public range = new FormGroup({
    start: new FormControl<Date | string>(''),
    end: new FormControl<Date | string>('')
  });
  public name: string = '';
  public start_date: any = '';
  public end_date: any = '';
  public isChecked: boolean = false;
  public page: number = 1;
  public per_page: number = 5;
  public total_pages!: number;
  public total_records!: number;
  public assignee: { [key: string]: string } = {};
  public searchResultProducts: string[] = [];
  public searchInputGroup!: FormGroup;
  public selectModerator: FormControl<string | null> = new FormControl('');
  public moderatorsList: { color: string; name: string }[] = [
    { color: '#FFFFFF', name: 'Unassigned' }
  ];
  public inputSearch!: string;
  public selectedValues: { color: string; name: string }[] = [];
  public dateStart: undefined | null = null;
  public dateEnd: undefined | null = null;
  public resetFilter: boolean = false;

  // eslint-disable-next-line max-len
  constructor(private readonly store: Store, private readonly router: Router, private readonly idSharedService: IdSharedService, private readonly mesService: ModeratorMessageService, private messageService: MessageService) {
    super();
  }

  public getDataFront(data: IHotlineData[]): IHotlineData[] {
    if (Array.isArray(data)) {
      return data.map((group: IHotlineData) => {
        return {
          vendor_name: 'vendors_name',
          ...group
        };
      });
    } else {
      return data;
    }
  }

  public getHotlineData(): void {
    this.store.dispatch(new GetProductsOnModeration(this.name, this.start_date, this.end_date, this.isChecked, this.page, this.per_page));
    this.metadata$.subscribe((meta: IMetaHotline): void => {
      this.page = clone(meta.page);

      if (meta.total_pages != null) {
        this.total_pages = clone(meta.total_pages);
      }

      if (meta.total_records != null) {
        this.total_records = clone(meta.total_records);
      }
    });

    this.hotlineData$.subscribe(
      (data: IProductOnModeration[]): void => {
        this.hotlineData = clone(data);
        this.hotlineDataByVendorFront = this.getDataFront(this.hotlineData);
        this.dataSource = new MatTableDataSource<IHotlineData>(this.hotlineDataByVendorFront);

        if (this.hotlineData[Symbol.iterator]) {
          for (let item of this.hotlineData) {
            const color: string = item.moderatorColor !== undefined ? item.moderatorColor : '';
            const existModerator = this.moderatorsList.find((moderator) => moderator.name === item.assignee);

            if (!existModerator) {
              this.moderatorsList.push({
                'color': color,
                'name': item.assignee
              });
            }
          }
        }
      });
  }

  ngOnInit(): void {
    this.getHotlineData();
    this.dataPicker();
    this.searchInputGroup = new FormGroup({
      searchInput: new FormControl('')
    });
    this.subscribeTo = this.searchInputGroup
      .get('searchInput')!
      .valueChanges.pipe()
      .subscribe((value: string) => {
        this.onSearch(value);
        this.getHotlineData();
      });
    this.subscribeTo = this.metadata$.subscribe((meta: IMetaHotline) => {
    });
    this.subscribeTo = this.hotlineData$.subscribe((data: IProductOnModeration[]) => {
      this.hotlineData = data;
    });
    this.dataSource = new MatTableDataSource<IHotlineData>(this.hotlineData);
  }

  ngAfterViewInit(): void {
    window.scrollTo(0, 0);
    this.mesService.message$.subscribe((message: string) => {
      this.message = message;
    });

    if (this.message) {
      this.messageService.add({ severity: 'success', detail: this.message });
    }

    if (this.paginator && this.paginator._intl) {
      this.paginator._intl.itemsPerPageLabel = 'Rows per page:';
    }

    if (this.start_date && this.end_date) {
      this.resetFilter = !this.resetFilter;
    }

    this.resetFilters();
  }

  override ngOnDestroy(): void {
    this.moderatorsList = [];
    this.selectedValues = [];
  }

  public dataPicker() {
    this.range.valueChanges.subscribe(values => {
      this.start_date = '';
      this.end_date = '';

      (values.start != null || values.end != null)
        ? this.resetFilter = true
        : this.resetFilter = false;

      if (values.start != null && values.end != null) {
        this.start_date = format(new Date(values.start), 'yyyy-MM-dd');
        this.end_date = format(new Date(values.end), 'yyyy-MM-dd');
        this.getHotlineData();
        this.start_date = '';
        this.end_date = '';
      }
    });
  }

  public onPageChange(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    this.per_page = event.pageSize;
    this.getHotlineData();
  }

  public onSearch(searchValue: string): void {
    this.resetFilter = !!searchValue;
    this._clearResults();
    this.name = searchValue;
    this.searchResultProducts = this.hotlineData.map(
      (item) => item.name
    );
    this.inputSearch = searchValue;
  }

  private _clearResults(): void {
    this.searchResultProducts = [];
  }

  public clearSearchInput(): void {
    const value: string = '';

    this.searchInputGroup.get('searchInput')?.patchValue(value);
    this._clearResults();
  }

  public closeSearchResult(): void {
    this._clearResults();
  }

  public setSearchValue(value: string): void {
    if (this.searchInputGroup.get('searchInput')?.value === value) {
      this._clearResults();
    }

    this.searchInputGroup.get('searchInput')?.patchValue(value);
  }

  public onToggleChange(): void {
    this.isChecked = !this.isChecked;

    this.isChecked ? this.resetFilter = true : this.resetFilter = false;

    this.getHotlineData();
  }

  public resetSelect(index: number): void {
    this.selectedValues = this.selectedValues.filter((value, i) => i !== index);
  }

  public navigateToProduct(id: number): void {
    this.idSharedService.setIdProduct(id);
    this.router.navigate([
      `feed/moderation/moderator-account/hotline/${id}/on-moderation/`
    ]);
  }

  public refresh(): void {
    this.store.dispatch(new GetProductsOnModeration(this.name, this.start_date, this.end_date, this.isChecked, this.page, this.per_page));
  }

  public resetFilters(): void {
    this.start_date = '';
    this.end_date = '';
    this.clearSearchInput();
    this.dateStart = null;
    this.dateEnd = null;
    this.isChecked = false;
    this.getHotlineData();
  }
}
