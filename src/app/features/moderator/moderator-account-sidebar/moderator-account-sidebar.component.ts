import { AfterViewInit, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { Select, Store } from '@ngxs/store';
import { clone } from 'lodash';
import { Observable } from 'rxjs';

import { ClearOnModerateReviewAction, GetQuantityAllProd, Logout } from '../ngxs/moderator.action';
import { IMetaHotline } from '../ngxs/moderator.model';
import { ModeratorService } from '../ngxs/moderator.service';
import { IOnModerationState, OnModerationState } from '../ngxs/on-moderation.state';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';

export interface Section {
  title: string;
  quantity: number | undefined;
}

export interface Page {
  title: string;
}

@Component({
  selector: 'app-moderator-account-sidebar',
  templateUrl: './moderator-account-sidebar.component.html',
  styleUrls: ['./moderator-account-sidebar.component.scss']
})
export class ModeratorAccountSidebarComponent extends UnsubscribeComponent implements OnInit, AfterViewInit {
  @Output() onLogout = new EventEmitter<boolean>();
  @Select(OnModerationState.meta) metadata$!: Observable<IMetaHotline>;
  @Select(OnModerationState.getQuantityAllProd) quantityAllProd$!: Observable<number | undefined>;
  public sections: Section[] = [
    {
      title: 'Hotline',
      quantity: 0
    },
    {
      title: 'Feedback',
      quantity: 0
    },
    {
      title: 'Vendors',
      quantity: 0
    },
    {
      title: 'User',
      quantity: 0
    }
  ];
  public activeItem: string = this.sections[0].title;
  public pages: Page[] = [
    {
      title: 'Account'
    },
    {
      title: 'Settings'
    },
    {
      title: 'Support'
    }
  ];

  constructor(private readonly moderatorService: ModeratorService, private readonly router: Router, private readonly store: Store) {
    super();
  }

  ngOnInit(): void {
    this.store.dispatch(new GetQuantityAllProd());
    this.subscribeTo = this.moderatorService.getQuantityAllProd().subscribe((meta: any) => {
      this.sections[0].quantity = clone(meta.meta.total_records);
    });
  }

  public removeCookie(): void {
    this.onLogout.emit();
  }

  ngAfterViewInit(): void {
    window.scrollTo(0, 0);
    this.store.dispatch(new GetQuantityAllProd());
    this.subscribeTo = this.moderatorService.getQuantityAllProd().subscribe((meta: any) => {
      this.sections[0].quantity = clone(meta.meta.total_records);
    });
  }

  public onSelectItem(item: string): void {
    this.store.dispatch(new ClearOnModerateReviewAction());
    if (item === 'Logout') {
      this.store.dispatch(new Logout());
      this.removeCookie();
      this.activeItem = item;
    }

    this.activeItem = item;
  }
}
