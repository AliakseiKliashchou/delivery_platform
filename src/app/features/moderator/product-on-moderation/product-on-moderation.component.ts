import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { Select, Store } from '@ngxs/store';
import { clone } from 'lodash';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';

import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { IdSharedService } from '../../../shared/services/id-shared.service';
import { LocalstorageService } from '../../../shared/services/localstorage.service';
import { ModeratorMessageService } from '../../../shared/services/moderator-message.service';
import { ClearOnModerateReviewAction, DeleteReviewOnModeration, GetOneProductOnModeration, GetProductsOnModeration, GetQuantityAllProd, GetReviewOnModeration, UpdateStatusOnModeration } from '../ngxs/moderator.action';
import { FormControlMap, IMetaHotline, IProductOneOnModeration } from '../ngxs/moderator.model';
import { ModeratorService } from '../ngxs/moderator.service';
import { OnModerationState } from '../ngxs/on-moderation.state';

interface Review {
  [key: string]: string;
}

interface SpellCheckError {
  id: string;
  offset: number;
  length: number;
  description: {
    [key: string]: string;
  };
  bad: string;
  better: string[];
  type: string;
}

interface SpellCheckResponse {
  result: boolean;
  errors: SpellCheckError[];
}

interface SpellCheckResult {
  status: boolean;
  response: SpellCheckResponse;
}

interface Product {
  manual: string;
  general: string;
  photo: string;
  name: string;
  weight: number | string;
  price: number | string;
}

interface ErrorDescriptions {
  errGeneral: string;
  errPhoto: string;
  errName: string;
  errWeight: string;
  errPrice: string;
  addManual: string;
}

@Component({
  selector: 'app-product-on-moderation',
  templateUrl: './product-on-moderation.component.html',
  styleUrls: ['./product-on-moderation.component.scss']
})
export class ProductOnModerationComponent extends UnsubscribeComponent implements OnInit, AfterViewInit {
  @ViewChild('textarea', { static: false }) textarea!: ElementRef;
  @Select(OnModerationState.getOneProductOnModeration) oneProduct$!: Observable<IProductOneOnModeration>;
  @Select(OnModerationState.getReviewOnModeration) reviews$!: Observable<Review[]>;
  @Select(OnModerationState.deleteReviewOnModeration) deleteReview$!: Observable<Review[]>;
  @Select(OnModerationState.meta) metadata$!: Observable<IMetaHotline>;
  public form!: FormGroup;
  public review: Review[] = [];
  public currentTab: string = 'information';
  public oneProduct!: IProductOneOnModeration;
  public idProduct!: number | undefined;
  public pathGetId!: string [];
  public price!: { price: number | undefined };
  public ingredients!: string | undefined;
  public foodGroups!: string | undefined;
  public dietary_tags!: string | undefined;
  public defaultImg: string = '../../../../assets/images/moderation/product-on-moderation/photoComing2.jpg';
  public display: boolean = false;
  public displayRemove: boolean = false;
  public displayPublish: boolean = false;
  public displaySend: boolean = false;
  public isDisabled: boolean = true;
  protected readonly Object = Object;
  public price_with_discount: number | undefined = 0;
  public discount: number | undefined = 0;
  public isChecked: boolean = false;
  public item!: string;
  public formControlMap: FormControlMap = {
    'General': 'general',
    'Photo': 'photo',
    'Product name': 'name',
    'Weight': 'weight',
    'Price': 'price',
    'Manual': 'manual'
  };
  public primeNGInitialized = false;
  public prod: Product = {
    general: '',
    photo: '',
    name: '',
    weight: 0,
    price: 0,
    manual: ''
  };
  public errorDescriptions: ErrorDescriptions = {
    errGeneral: '',
    errPhoto: '',
    errName: '',
    errWeight: '',
    errPrice: '',
    addManual: ''
  };
  public response!: SpellCheckResult;
  public spellingBad: boolean = false;
  public success: boolean = false;

  // eslint-disable-next-line max-len
  constructor(private readonly store: Store, private readonly idSharedService: IdSharedService, private readonly mService: ModeratorService, private readonly router: Router, private readonly dialog: MatDialog, private messageService: MessageService, private readonly mesService: ModeratorMessageService, private locStSer: LocalstorageService, private readonly http: HttpClient) {
    super();
  }

  ngOnInit(): void {
    const savedValue = this.locStSer.getValue('checkboxValue');

    this.isChecked = savedValue ? JSON.parse(savedValue) : false;
    this.pathGetId = this.router.url.split('/');
    this.idProduct = +this.pathGetId[this.pathGetId.length - 2];

    if (!this.idProduct) {
      this.idSharedService.idProduct$.subscribe(
        (idProduct) => (this.idProduct = idProduct)
      );
    }

    this.store.dispatch(new GetOneProductOnModeration(Number(this.idProduct)));
    this.subscribeTo = this.oneProduct$.subscribe(
      (oneProduct: IProductOneOnModeration) => {
        this.oneProduct = { ...oneProduct };
        this.discountPrice();
        this.prod = {
          general: oneProduct.image || '',
          photo: oneProduct.image || '',
          name: oneProduct.name || '',
          weight: oneProduct.weight,
          price: oneProduct && oneProduct.product_discount_percent
            ? +(oneProduct.price / 100 * (100 - oneProduct.product_discount_percent))
            : oneProduct.price,
          manual: ''
        };
      }
    );

    this.form = new FormGroup({
      general: new FormControl(''),
      photo: new FormControl(''),
      name: new FormControl(''),
      weight: new FormControl(''),
      price: new FormControl(''),
      manual: new FormControl('')
    });
    this.subscribeTo = this.store.dispatch(new GetOneProductOnModeration(Number(this.idProduct))).subscribe(res => {
    });
    this.store.dispatch(new GetReviewOnModeration(Number(this.idProduct)));
    this.subscribeTo = this.reviews$.subscribe(
      (review: Review[]) => {
        this.review = clone(review);
        this.form = new FormGroup({
          system: new FormControl(Object.values(this.review)[0]),
          general: new FormControl(Object.values(this.review)[1]),
          photo: new FormControl(Object.values(this.review)[2]),
          name: new FormControl(Object.values(this.review)[3]),
          weight: new FormControl(Object.values(this.review)[4]),
          price: new FormControl(Object.values(this.review)[5]),
          manual: new FormControl(Object.values(this.review)[6])
        });
      }
    );
  }

  public onAdd(): void {
    this.checkDescription();
    const history_record: FormControlMap = {
      'new_status': 'On moderation'
    };
    const field_flags: FormControlMap = {
        'system': 'View the system product auto-validation results below. Please review the product manually.',
        'General': this.errorDescriptions.errGeneral,
        'Photo': this.errorDescriptions.errPhoto,
        'Product name': this.errorDescriptions.errName,
        'Weight': this.errorDescriptions.errWeight,
        'Price': this.errorDescriptions.errPrice,
        'Manual': this.errorDescriptions.addManual
      };

    this.mService.postReviewOnModeration(this.idProduct, history_record, field_flags).subscribe(response => {
      this.store.dispatch(new GetOneProductOnModeration(Number(this.idProduct)));
      this.store.dispatch(new GetReviewOnModeration(Number(this.idProduct)));
    });
    this.updateInfo(history_record, field_flags);
  }

  public checkDescription(): void {
    this.subscribeTo = this.checkSpelling(this.prod.name).subscribe(response => {
      this.response = clone(response);
      if (response.response.errors.length) {
        this.spellingBad = true;
      }
    });
    const general = !this.prod.general;
    const photo = !this.prod.photo;
    let nameError!: number;

    if (this.response) {
      nameError = this.response.response.errors.length;
    }

    const weightError = this.prod.weight < 10 || this.prod.weight > 2000;
    const priceError = this.prod.price < 1 || this.prod.price > 2000;

    if (general) {
      this.errorDescriptions.errGeneral += 'No picture or Product doesn\'t comply with an image.';
    }

    if (photo) {
      this.errorDescriptions.errPhoto += 'No picture.';
    }

    if (nameError) {
      this.errorDescriptions.errName += 'Possibly spelling mistake. Possible variations of the words: ';
      this.response.response.errors.map((item: any) => {
        this.errorDescriptions.errName += item.better.join(', ') + ', ';
      });
    }

    if (weightError) {
      this.errorDescriptions.errWeight += ' Check the product weight, it looks unrealistic.';
    }

    if (priceError) {
      this.errorDescriptions.errPrice += `${this.prod.price} Check the product price, it looks unrealistic.`;
    }

    if (general && photo && nameError && weightError && priceError) {
      this.success = true;
    }
  }

  public checkSpelling(text: string): Observable<any> {
    // https://textgears.com/ru/
    const url = `https://api.textgears.com/spelling?key=0PXfCmzsbJZn8sHN&text=${text}`;

    return this.http.get<any>(url);
  }

  public discountPrice(): void {
    this.ingredients = this.oneProduct.ingredients?.join(', ');
    this.foodGroups = this.oneProduct.food_groups?.join(', ');
    this.dietary_tags = this.oneProduct.dietary_tags?.join(', ');

    if (this.oneProduct.product_discount_percent) {
      this.discount = this.oneProduct.price / 100 * this.oneProduct.product_discount_percent;
      this.price_with_discount = this.oneProduct.price / 100 * (100 - this.oneProduct.product_discount_percent);
    }

    this.price_with_discount = this.oneProduct.price;
  }

  public switchCurrentTab(tabName: string): void {
    this.currentTab = tabName;
  }

  public onImageError($event: ErrorEvent): void {
    if ($event.target instanceof HTMLImageElement) {
      $event.target.src = this.defaultImg;
    }
  }

  public showDialog(): void {
    this.display = true;
  }

  public disable(): void {
    this.isDisabled = !this.isDisabled;
  }

  public submit(): void {
    const history_record: FormControlMap = {
      'new_status': 'On moderation'
    };
    const field_flags: FormControlMap =
      {
        'system': '',
        'General': this.form.value.general,
        'Photo': this.form.value.photo,
        'Product name': this.form.value.name,
        'Weight': this.form.value.weight,
        'Price': this.form.value.price,
        'Manual': this.form.value.manual
      };

    this.updateInfo(history_record, field_flags);
    this.display = false;
  }

  public updateInfo(history_record: FormControlMap, field_flags: FormControlMap): void {
    this.subscribeTo = this.mService.updateReviewOnModeration(Number(this.idProduct), history_record, field_flags).subscribe(response => {
      this.store.dispatch(new GetOneProductOnModeration(Number(this.idProduct)));
      this.store.dispatch(new GetReviewOnModeration(Number(this.idProduct)));
      this.store.dispatch(new GetQuantityAllProd());
    });
    this.store.dispatch(new ClearOnModerateReviewAction());

  }

  ngAfterViewInit(): void {
    this.primeNGInitialized = true;
    this.discountPrice();
    this.deleteItem(this.item);
  }

  public removeAll(): void {
    this.display = false;
    this.displayRemove = true;
  }

  public cancelEdit(): void {
    this.displayRemove = false;
    this.display = true;
  }

  public remove(): void {
    this.display = false;
    this.displayRemove = false;
    this.subscribeTo = this.store.dispatch(new DeleteReviewOnModeration(this.idProduct || 0)).subscribe(
      () => {
        this.store.dispatch(new GetOneProductOnModeration(Number(this.idProduct)));
        this.store.dispatch(new GetReviewOnModeration(Number(this.idProduct)));
      }
    );
  }

  public deleteItem(item: string): void {
    if (item in this.formControlMap) {
      const formControlName = this.formControlMap[item];

      this.form.get(formControlName)?.setValue('');
    }

    this.item = item;
  }

  public editTextarea(item: string): void {
    const formControlName = this.formControlMap[item];

    if (formControlName) {
      this.form.get(formControlName)?.setValue(this.form.value[formControlName]);
    }
  }

  public moderate(): void {
    let isChecked = localStorage.getItem('checkboxValue');

    if (!isChecked) {
      this.displayPublish = true;
      this.displaySend = true;
    } else {
      this.oneProduct.has_reviews ? this.publishFn() : this.sendBackFn();
    }
  }

  public publish(): void {
    let isChecked = localStorage.getItem('checkboxValue');

    if (!isChecked) {
      this.publishFn();
    }

    this.isCheckedFn();
    this.publishFn();
  }

  public publishFn(): void {
    this.store.dispatch(new UpdateStatusOnModeration(Number(this.idProduct), 'Published')).subscribe(() => {

    });
    this.displayPublish = false;
    this.store.dispatch(new GetProductsOnModeration('', '', '', false, 1));
    this.store.dispatch(new GetQuantityAllProd());
    this.mesService.setMessage('The product was published successfully');
    this.store.dispatch(new ClearOnModerateReviewAction());
    this.router.navigate(['/feed/moderation/moderator-account/hotline']);
  }

  public sendBAck(): void {
    let isChecked = localStorage.getItem('checkboxValue');

    if (!isChecked) {
      this.sendBackFn();
    }

    this.isCheckedFn();
    this.sendBackFn();
  }

  public sendBackFn(): void {
    const { general, photo, name, weight, price, manual } = this.form.value;
    const comment = `${general} ${photo} ${name} ${weight} ${price} ${manual}`;

    this.store.dispatch(new UpdateStatusOnModeration(Number(this.idProduct), 'Declined', comment)).subscribe(() => {
      this.store.dispatch(new GetProductsOnModeration('', '', '', false, 1));
    });
    this.store.dispatch(new GetQuantityAllProd());
    this.displaySend = false;
    this.mesService.setMessage('The product was sent back successfully');
    this.store.dispatch(new ClearOnModerateReviewAction());
    this.router.navigate(['/feed/moderation/moderator-account/hotline']);
  }

  public isCheckedFn(): void {
    this.locStSer.setValue('checkboxValue', JSON.stringify(this.isChecked));
  }
}
