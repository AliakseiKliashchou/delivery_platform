import { Component, Inject, OnInit } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { VerifyPhoneComponent } from '../verify-phone-modal/verify-phone.component';
import { VendorAccountService } from '../../shared/services/vendor-account.service';
import { UnsubscribeComponent } from '../../shared/directives/unsubscribe.component';
import { getModalConfig } from '../../shared/utils/getModalConfig';
import {
  CustomerAccountService,
  CustomerCodeVerificationResponse
} from '../../shared/services/customer-account.service';

export interface ConfirmPhoneFG {
  phone: AbstractControl;
}

export interface DialogData {
  isCart: boolean;
}

@Component({
  selector: 'app-confirm-phone',
  templateUrl: './confirm-phone.component.html',
  styleUrls: ['./confirm-phone.component.scss'],
})
export class ConfirmPhoneComponent
  extends UnsubscribeComponent
  implements OnInit {
  public group!: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private readonly dialog: MatDialog,
    private readonly vendorAccountService: VendorAccountService,
    private readonly customerAccountService: CustomerAccountService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.group = new FormGroup<ConfirmPhoneFG>({
      phone: new FormControl('',
        [Validators.required,
          Validators.minLength(10),
          Validators.maxLength(12)]),
    });
  }

  public get phone(): AbstractControl {

    return this.group.get('phone')!;
  }

  public sendCode(): void {
    let phone = this.group.get('phone')!.value;
    this.subscribeTo = this.customerAccountService
      .sendVerificationCode(phone)
      .subscribe((res : CustomerCodeVerificationResponse) => {
        this.dialog.closeAll();
        this.dialog.open(
          VerifyPhoneComponent,
          getModalConfig(500, 285, 'app-delete-dialog', {
            phone: phone,
            ...this.data,
          })
        );
      });
  }
}
