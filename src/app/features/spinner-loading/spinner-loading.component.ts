import { ChangeDetectorRef, Component, OnInit } from "@angular/core";

import { Observable } from "rxjs";
import { Select } from "@ngxs/store";

import { CommonState } from "../../shared/ngxs/common.state";
import { UnsubscribeComponent } from "../../shared/directives/unsubscribe.component";

@Component({
  selector: 'app-spinner-loading',
  templateUrl: './spinner-loading.component.html',
  styleUrls: ['./spinner-loading.component.scss']
})
export class SpinnerLoaderComponent extends UnsubscribeComponent implements OnInit {

  public isSpinnerLoading = false;

  @Select(CommonState.getIsSpinnerLoading) isSpinnerLoading$!: Observable<boolean>;

  constructor(private cdr: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.getIsSpinnerLoading();
  }

  getIsSpinnerLoading() {
    this.subscribeTo = this.isSpinnerLoading$.subscribe(isLoading => {
      this.isSpinnerLoading = isLoading;
      this.cdr.detectChanges();
    });
  }
}
