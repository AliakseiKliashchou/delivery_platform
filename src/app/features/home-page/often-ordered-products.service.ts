import { Injectable } from '@angular/core';

import { Observable, catchError, map } from 'rxjs';

import { IProduct } from 'src/app/core/models/product.model';
import { ApiService } from 'src/app/core/services/api.service';
import { IMostOrderedProductsResp } from 'src/app/shared/interfaces/most-ordered-products-response.interface';

@Injectable()
export class OftenOrderedProductsService {

    constructor(
        private readonly http: ApiService,
    ) {}

    get(): Observable<IProduct[]> {
        return this.http.get('api/accounts/customer/most_ordered_products')
            .pipe(
                map((resp: IMostOrderedProductsResp) => resp.products || []),
                catchError((_err) => {
                    return [];
                })
            );
    }
}
