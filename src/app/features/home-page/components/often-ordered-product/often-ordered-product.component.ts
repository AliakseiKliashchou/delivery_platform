import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { first, Observable } from 'rxjs';

import { IProduct } from 'src/app/core/models/product.model';
import {
  AddProductToCart,
  DecreaseProductQuantity,
  IncreaseProductQuantity,
  RemoveProductFromCart
} from 'src/app/features/cart/ngxs/product.actions';
import { ProductState } from 'src/app/features/cart/ngxs/product.state';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { ChangeVendorComponent } from 'src/app/shared/components/change-vendor/change-vendor.component';
import { ScheduleData } from 'src/app/core/models/vendor.model';

@Component({
  selector: 'app-often-ordered-product',
  templateUrl: './often-ordered-product.component.html',
  styleUrls: ['./often-ordered-product.component.scss']
})
export class OftenOrderedProductComponent extends UnsubscribeComponent {

  @Input() product!: IProduct;
  @Input() vendorsSchedule!: ScheduleData;
  @Input() ignoreSchedule?: boolean;

  @Select(ProductState.getProductCart) productCartData$!: Observable<any>;

  constructor(private dialog: MatDialog, private store: Store) {
    super();
  }

  public openDialog(productOfNewVendor: IProduct, currentVendor: number) {
    this.dialog.closeAll();
    this.dialog.open(ChangeVendorComponent, {
      data: { productOfNewVendor, currentVendor },
      width: "500px",
      height: "216",
      panelClass: "app-delete-dialog",
    });
  }

  public decreaseProductQuantity(product: IProduct): void {
    product.quantity! > 1
      ? this.store.dispatch(new DecreaseProductQuantity(product))
      : this.store.dispatch(new RemoveProductFromCart(product));
  }

  public increaseProductQuantity(cartItem: IProduct): void {
    this.store.dispatch(new IncreaseProductQuantity(cartItem));
  }

  public addProductToCart(product: IProduct): void {
    this.subscribeTo = this.productCartData$.pipe(first()).subscribe(res => {
      if (res === undefined || res.length === 0 || res[0].vendor_id === product.vendor_id) {
        this.store.dispatch(new AddProductToCart(product));

        return;
      } else {
        this.openDialog(product, res[0].vendor_id);
      }
    });
  }

}
