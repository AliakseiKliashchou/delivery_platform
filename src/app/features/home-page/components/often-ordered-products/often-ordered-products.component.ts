import { Component } from '@angular/core';

import { Observable } from 'rxjs';

import { OftenOrderedProductsService } from '../../often-ordered-products.service';
import { IProduct } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-often-ordered-products',
  templateUrl: './often-ordered-products.component.html',
  styleUrls: ['./often-ordered-products.component.scss']
})
export class OftenOrderedProductsComponent {
  public products$: Observable<IProduct[]>;

  constructor(
    oftenOrderedService: OftenOrderedProductsService
  ) {
    this.products$ = oftenOrderedService.get();
  }
}
