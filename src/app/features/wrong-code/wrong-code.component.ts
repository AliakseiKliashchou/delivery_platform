import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmPhoneComponent } from '../confirm-phone-modal/confirm-phone.component';
import { getModalConfig } from '../../shared/utils/getModalConfig';

@Component({
  selector: 'app-wrong-code',
  templateUrl: './wrong-code.component.html',
  styleUrls: ['./wrong-code.component.scss']
})
export class WrongCodeComponent  {

  constructor(private dialog: MatDialog) { }

  public goBack(): void {
    this.dialog.closeAll();
    this.dialog.open(
      ConfirmPhoneComponent,
      getModalConfig(500, 292, 'app-delete-dialog', { isCart: false })
    );
  }
}
