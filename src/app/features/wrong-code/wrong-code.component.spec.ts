import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongCodeComponent } from './wrong-code.component';

describe('WrongCodeComponent', () => {
  let component: WrongCodeComponent;
  let fixture: ComponentFixture<WrongCodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WrongCodeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WrongCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
