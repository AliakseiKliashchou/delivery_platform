import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { AgroexToastService } from 'ngx-agroex-toast';

import { TOAST_CONFIG } from '../../../shared/constants/toast-messages';
import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { IProductById } from '../../../shared/interfaces/product-by-id.interface';
import { IProductData } from '../../../shared/interfaces/product-from-list.interface';
import { VendorAccountService } from '../../../shared/services/vendor-account.service';
import { VendorProductsService } from '../../../shared/services/vendor-products.service';
import { IVendorAccountData } from '../../login-page/login-page.component';

@Component({
  selector: 'app-vendor-edit-product-page',
  templateUrl: './vendor-edit-product-page.component.html',
  styleUrls: ['./vendor-edit-product-page.component.scss']
})
export class VendorEditProductPageComponent extends UnsubscribeComponent implements OnInit {
  public product?: IProductById;
  public wasProductPublished?: boolean;
  public productId?: number;

  constructor(
    private route: ActivatedRoute,
    private vendorProductsService: VendorProductsService,
    private router: Router,
    private toastService: AgroexToastService,
    private vendorAccountService: VendorAccountService
    ) {
    super();
  }

  ngOnInit() {
    this.getProductId();
  }

  public getProductId(): void {
    this.subscribeTo = this.route.params.subscribe((params: Params) => {
      this.productId = params['id'];
      this.getProductById();
    });
  }

  public getProductById(): void {
    this.subscribeTo = this.vendorProductsService.getProduct(this.productId!)
      .subscribe({
        next: (product: IProductById) => {
          this.product = product;
          this.getVendorsData();
        },
        error: (error) => {
          if (error.status === 404) {
            this.navigateToProductsPage();
            this.toastService.addToast(TOAST_CONFIG.forProduct.error.productDoesNotExist);
          }
        }
      });
  }

  public findProductIndex(vendorData: IProductData[]): number {
    return vendorData.findIndex(x => x.id == this.productId);
  }

  public getVendorsData(): void {
    this.subscribeTo = this.vendorAccountService
      .getVendorData()
      .subscribe({
        next: (vendorAccountData: IVendorAccountData) => {
          const vendorData = vendorAccountData.user.products;
          const productIndex = this.findProductIndex(vendorData);

          this.wasProductPublished = !!vendorData[productIndex].publish_date;
        }
      });
  }

  public navigateToProductsPage(): void {
    void this.router.navigate([
      `feed/vendor/vendor-account/${this.product?.vendor.id}/products/`,
    ]);
  }
}
