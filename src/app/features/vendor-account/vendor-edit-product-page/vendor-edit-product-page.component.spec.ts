import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorEditProductPageComponent } from './vendor-edit-product-page.component';

describe('VendorEditProductPageComponent', () => {
  let component: VendorEditProductPageComponent;
  let fixture: ComponentFixture<VendorEditProductPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorEditProductPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorEditProductPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
