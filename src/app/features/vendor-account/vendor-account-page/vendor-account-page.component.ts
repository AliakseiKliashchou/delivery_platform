import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';

import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';
import { removeCookie } from 'src/app/shared/utils/removeCookie';
import {
  IUserData,
  IVendorAccountData,
} from '../../login-page/login-page.component';

@Component({
  selector: 'app-vendor-account-page',
  templateUrl: './vendor-account-page.component.html',
  styleUrls: ['./vendor-account-page.component.scss'],
})
export class VendorAccountPageComponent
  extends UnsubscribeComponent
  implements OnInit
{
  public vendorData!: IUserData;
  public removeCookie(event: any): void {
    removeCookie('hsf-account-vendor-access-token');
    removeCookie('hsf-account-vendor-refresh-token');
    this.router.navigate([`/feed/vendor/login`]);
  }

  constructor(
    private vendorAccountService: VendorAccountService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribeTo = this.vendorAccountService
      .getVendorData()
      .subscribe((data: IVendorAccountData) => {
        if (data.user) {
          this.vendorData = data.user;
        }
      });
  }
}
