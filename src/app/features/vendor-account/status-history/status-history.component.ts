import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngxs/store';
import format from 'date-fns/format';
import * as _ from 'lodash';
import { Observable } from 'rxjs';

import { IStatusHistoryData } from '../../../core/models/history.model';
import { UnsubscribeComponent } from '../../../shared/directives/unsubscribe.component';
import { IdSharedService } from '../../../shared/services/id-shared.service';
import { ProductState } from '../../cart/ngxs/product.state';

import { GetProductHistory } from './../../cart/ngxs/product.actions';

@Component({
  selector: 'app-status-history',
  templateUrl: './status-history.component.html',
  styleUrls: ['./status-history.component.scss'],
})
export class StatusHistoryComponent extends UnsubscribeComponent implements OnInit {
  public statusBack!: IStatusHistoryData[];
  public statusFront!: IStatusHistoryData[];
  public pathGetId!: string [];
  public responseFront!: IStatusHistoryData[];
  public idProduct!: number | undefined;
  private statusBack$: Observable<IStatusHistoryData[]> = this.store.select(
    ProductState.getProductHistory
  );

  constructor(private readonly store: Store, private readonly router: Router, private readonly idSharedService: IdSharedService) {
    super();
  }

  ngOnInit(): void {
    this.pathGetId = this.router.url.split('/');
    this.idProduct = +this.pathGetId[this.pathGetId.length - 2];

    if (!this.idProduct) {
      this.idSharedService.idProduct$.subscribe(
        (idProduct) => (this.idProduct = idProduct
        )
      );
    }
    this.store.dispatch(new GetProductHistory(Number(this.idProduct)));
    this.statusBack$?.subscribe((responseBack: IStatusHistoryData[]) => {
      this.responseFront = _.clone(responseBack);

      if (!!this.responseFront && Array.isArray(this.responseFront)) {
        this.statusBack = this.responseFront.reverse();
      }
      this.statusFront = this.getStatusFront(this.statusBack);
    });
    this.subscribeTo = this.statusBack$.subscribe(() => {
      this.statusFront = this.getStatusFront(this.statusBack);
    });
  }

  public getStatusFront(status: IStatusHistoryData[]) {
    return status.map((group: IStatusHistoryData) => {
      return {
        month: format(new Date(group.created_at), 'MMMM'),
        ...group,
      };
    });
  }
}
