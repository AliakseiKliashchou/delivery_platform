import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';

import { CreateReportModalComponent } from '../create-report-modal/create-report-modal.component';
import { REPORT_ORDERS_FORMATS, VENDOR_ORDER_FORMATS } from 'src/app/shared/constants/date-format';
import { getModalConfig } from 'src/app/shared/utils/getModalConfig';

@Component({
  selector: 'app-report-date-modal',
  templateUrl: './report-date-modal.component.html',
  styleUrls: ['./report-date-modal.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: REPORT_ORDERS_FORMATS },
  ],
})
export class ReportDateModalComponent {

  public date: Date = new Date();
  public formDate: FormControl = new FormControl(this.date);

  constructor (
    private dialog: MatDialog,
    private matDialogRef: MatDialogRef<ReportDateModalComponent>
  ) {}

  public openPreviousModal(): void {
    this.dialog.open(
      CreateReportModalComponent,
      getModalConfig(500, 460, 'app-delete-dialog')
    );
    this.matDialogRef.close();
  }
}
