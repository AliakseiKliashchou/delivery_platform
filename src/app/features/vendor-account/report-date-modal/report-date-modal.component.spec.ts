import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDateModalComponent } from './report-date-modal.component';

describe('ReportDateModalComponent', () => {
  let component: ReportDateModalComponent;
  let fixture: ComponentFixture<ReportDateModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportDateModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportDateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
