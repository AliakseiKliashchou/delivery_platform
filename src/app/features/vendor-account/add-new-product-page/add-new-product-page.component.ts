import { HttpErrorResponse } from '@angular/common/http';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Select, Store } from '@ngxs/store';
import { AgroexToastService, IToastOptions, ToastType } from 'ngx-agroex-toast';
import { map, Observable, startWith } from 'rxjs';

import { ImageEditComponent } from '../../../shared/components/image-edit/image-edit.component';
import { COOKING_INTERVAL_LIST } from '../../../shared/constants/cooking-interval-list';
import { TOAST_CONFIG } from '../../../shared/constants/toast-messages';
import { ProductStatusEnum } from '../../../shared/enums/product-status.enum';
import { IFoodGroupStatus } from '../../../shared/interfaces/food-group-status.interface';
import { ImageEditData } from '../../../shared/interfaces/image-edit-data.interface';
import { INewProduct } from '../../../shared/interfaces/new-product.interface';
import { IProductById } from '../../../shared/interfaces/product-by-id.interface';
import { IIngredients } from '../../../shared/interfaces/product-lists.interface';
import { AssetsService } from '../../../shared/services/assets.service';
import { ToastMessagesService } from '../../../shared/services/toast.service';
import { VendorProductsService } from '../../../shared/services/vendor-products.service';
import { ArchiveProductDialogComponent } from '../../archive-product-dialog/archive-product-dialog.component';
import {
  GetDietaryTags,
  GetFoodGroups,
  SetActiveDietaryTags,
  SetActiveFoodGroups,
  UpdateDietaryTags,
  UpdateFoodGroups
} from '../../cart/ngxs/product.actions';
import { ProductState } from '../../cart/ngxs/product.state';
import { RemoveProductDialogComponent } from '../../remove-product-dialog/remove-product-dialog.component';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';

@Component({
  selector: 'app-add-new-product-page',
  templateUrl: './add-new-product-page.component.html',
  styleUrls: ['./add-new-product-page.component.scss'],
})
export class AddNewProductPageComponent
  extends UnsubscribeComponent
  implements OnInit, OnChanges {
  @ViewChild('productInput') productInput!: ElementRef<HTMLInputElement>;
  @ViewChild('modalHintDescription') modalHint!: TemplateRef<HTMLElement>;

  @Input() public product: IProductById | undefined;
  @Input() public productId: number | undefined;
  @Input() public wasProductPublished?: boolean;

  @Output() public productDataChanged = new EventEmitter<void>();

  public productStatus = ProductStatusEnum;
  public defaultProductData = COOKING_INTERVAL_LIST;
  public currentTab: string = 'information';
  public dishIngredientList!: (string | undefined)[];
  public dishIngredientFilteredList!: Observable<(string | undefined)[]>;
  public dishIngredientSelectedList: string[] = [];
  public dietaryTypeList!: IFoodGroupStatus[];
  public cookingIntervalList: string[] = COOKING_INTERVAL_LIST;
  public foodGroupList!: IFoodGroupStatus[];
  public newProductForm!: FormGroup;
  public discount: number | undefined;
  public finalProductPrice: number | string | undefined;
  public isShownDiscountInput: boolean = false;
  public isFoodGroupEmpty: boolean = false;

  public activeDietaryTags: string[] = [];
  public activeFoodGroups: string[] = [];

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private vendorProductsService: VendorProductsService,
    private router: Router,
    private toastService: AgroexToastService,
    private toastMessagesService: ToastMessagesService,
    private assetsService: AssetsService,
    private translateService: TranslateService,
  ) {
    super();
  }

  @Select(ProductState.getFoodGroups) foodGroupList$!: Observable<IFoodGroupStatus[]>;
  @Select(ProductState.getDietaryTags) dietaryTypeList$!: Observable<IFoodGroupStatus[]>;

  ngOnInit(): void {
    this.initNewProductForm();
    this.getDishIngredientList();
    this.listenFinalProductPriceChanges();
    if (this.route.snapshot.url[1].path === 'add-new-product') {

      this.wasProductPublished = true;
    }

    this.store.dispatch([new GetFoodGroups(), new GetDietaryTags()]);

    this.subscribeTo = this.dietaryTypeList$.subscribe((dietaryTypes) => {
      this.activeDietaryTags = dietaryTypes
        .filter((dietaryType) => dietaryType.status)
        .map((dietaryType) => dietaryType.name);
    });

    this.subscribeTo = this.foodGroupList$.subscribe((foodGroups) => {
      this.activeFoodGroups = foodGroups
        .filter((foodGroup) => foodGroup.status)
        .map((foodGroup) => foodGroup.name);
    });

    this.subscribeTo = this.vendorProductsService
      .getDishIngredientList()
      .subscribe({
        next: (ingredients: IIngredients) => {
          this.dishIngredientList = ingredients.ingredients.map(
            (item: IFoodGroupStatus) => item.name
          );
          this.defineDishIngredientFilteredList();
        }
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['product']?.currentValue !== changes['product']?.previousValue) {
      this.pullAfterProductValueChanged();

      if (changes['product'].currentValue.price && changes['product'].currentValue.product_discount_percent) {
        this.showDiscountInput();
      }
    }

    if (this.product?.dietary_tag) {
      this.store.dispatch(new SetActiveDietaryTags(this.product.dietary_tag));
      this.activeDietaryTags = this.product.dietary_tag;
    }

    if (this.product?.food_group) {
      this.store.dispatch(new SetActiveFoodGroups(this.product?.food_group));
      this.activeFoodGroups = this.product.food_group;
    }
  }

  // WORK WITH CURRENT PRODUCT.
  public pullAfterProductValueChanged(): void {
    this.initNewProductForm();
    this.getFinalProductPrice();
    this.updateFinalProductPriceByChangingDiscount();
    this.updateFinalProductPriceByChangingPrice();
    this.dishIngredientSelectedList = this.product!.ingredient;
  };

  // FORM INITIALIZATION & WORKING WITH FORM
  public initNewProductForm(): void {
    this.newProductForm = new FormGroup({
      productName: new FormControl(this.product?.name || '', [
        Validators.minLength(3),
        Validators.maxLength(50),
        Validators.required,
      ]),
      productCtrl: new FormControl(''),
      productWeight: new FormControl(this.product?.weight || '', [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^[\d|\.]+$/),
      ]),
      productCookingTime: new FormControl(this.product?.cooking_time || '', [
        Validators.required,
        Validators.pattern(/^[\d|\->]+$/),
      ]),
      productPrice: new FormControl(this.product?.price || '', [
        Validators.required,
        Validators.min(1),
        Validators.pattern(/^(\d){1,5}(\.(\d){1,2})?$/),
      ]),
      productDiscount: new FormControl(this.product?.product_discount_percent || '', [
        Validators.pattern(/^[\d|\.]+$/),
        Validators.max(100),
      ]),
    });
  }

  // MANIPULATIONS WITH PRODUCT. DELETING, PUBLISHING, UPDATING, CHANGING STATUSES, ETC.
  public publishNewProduct(newProduct: INewProduct, newStatus?: ProductStatusEnum): void {
    this.subscribeTo = this.vendorProductsService
      .publishNewProductAsDraft(newProduct)
      .subscribe({
        next: (product: IProductById) => {
          if (!newStatus) {
            this.toastService.addToast(
              this.toastMessagesService.chooseSuccessMessageForNewProductStatus(ProductStatusEnum.Draft)
            );
            this.navigateToProductsPage();

            return;
          }

          this.updateProductStatus(product.id, newStatus);
        },
        error: (error: HttpErrorResponse) => {

          const errorObj: IToastOptions = {
            toastType: ToastType.Error,
            title: error.error.errors[0],
          };

          this.toastService.addToast(errorObj);
        }
      });
  }

  public updateProductInfo(newProduct: INewProduct, newStatus?: ProductStatusEnum): void {
    this.subscribeTo = this.vendorProductsService
      .updateProductInfo(this.product!.id, newProduct)
      .subscribe({
        next: (product: IProductById) => {
          if (!newStatus) {
            this.toastService.addToast(TOAST_CONFIG.forProduct.success.updated);
            this.navigateToProductsPage();

            return;
          }

          this.updateProductStatus(product.id, newStatus);
        },
        error: (error: HttpErrorResponse) => {

          const errorObj: IToastOptions = {
            toastType: ToastType.Error,
            title: error.error.errors[0],
          };

          this.toastService.addToast(errorObj);
        }
      });
  }

  public manipulateProduct(newStatus?: ProductStatusEnum): void {
    const newProduct: INewProduct = this.generateNewProduct();

    this.checkIsFoodGroupEmpty();

    if (this.checkIsProductFormValid()) {
      if (!this.product?.id) {
        this.publishNewProduct(newProduct, newStatus);

        return;
      }

      if (this.product?.id && !newStatus) {
        this.updateProductInfo(newProduct);

        return;
      }

      if (this.product?.id && newStatus) {
        this.updateProductInfo(newProduct, newStatus);

        return;
      }

      return;
    }

    this.newProductForm.markAllAsTouched();
    this.toastService.addToast(TOAST_CONFIG.forProduct.error.requiredFields);
  }

  public generateNewProduct(): INewProduct {
    return {
      name: this.newProductForm.get('productName')?.value,
      weight: +this.newProductForm.get('productWeight')?.value,
      price: +this.newProductForm.get('productPrice')?.value,
      cooking_time: this.newProductForm.get('productCookingTime')?.value,
      ingredients: this.dishIngredientSelectedList,
      dietary_tags: this.activeDietaryTags,
      food_groups: this.activeFoodGroups,
      number_purchases: 0,
      count: 30,
      product_discount_percent: +this.newProductForm.get('productDiscount')?.value,
    };
  }

  public checkIsProductFormValid(): boolean {
    return (
      this.newProductForm.valid &&
      !this.isFoodGroupEmpty &&
      this.dishIngredientSelectedList.length !== 0
    );
  }

  public openRemoveProductDialog(): void {
    const removeProductDialogRef = this.dialog.open(RemoveProductDialogComponent, {
      width: '500px',
      data: {
        productId: this.product!.id,
      }
    });

    removeProductDialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.navigateToProductsPage();
      }
    });
  }

  public openArchiveProductDialog(): void {
    const archiveProductDialogRef = this.dialog.open(ArchiveProductDialogComponent, {
      width: '500px',
      data: {
        productId: this.product!.id,
      }
    });

    archiveProductDialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        this.navigateToProductsPage();
      }
    });
  }

  public updateProductStatus(productId: number, newStatus: ProductStatusEnum): void {
    this.subscribeTo = this.vendorProductsService.updateProductStatus(
      productId,
      { status: newStatus }
    ).subscribe({
      next: () => {
        this.toastService.addToast(
          this.toastMessagesService.chooseSuccessMessageForNewProductStatus(newStatus)
        );
        this.navigateToProductsPage();
      }
    });
  }

  // WORKING ON PRICE & DISCOUNT
  public listenFinalProductPriceChanges(): void {
    this.updateFinalProductPriceByChangingDiscount();
    this.updateFinalProductPriceByChangingPrice();
  }

  public updateFinalProductPriceByChangingPrice(): void {
    this.subscribeTo = this.newProductForm
      .get('productPrice')!
      .valueChanges
      .subscribe({
        next: (price: number) => {
          this.discount = +((price * this.newProductForm.get('productDiscount')!.value) / 100)
            .toFixed(2);
          this.finalProductPrice = +(price - this.discount).toFixed(2);
        }
      });
  }

  public updateFinalProductPriceByChangingDiscount(): void {
    this.subscribeTo = this.newProductForm
      .get('productDiscount')!
      .valueChanges
      .subscribe({
        next: (discount: number) => {
          this.discount = +(
            (discount * this.newProductForm.get('productPrice')!.value) / 100)
            .toFixed(2);
          this.finalProductPrice = +(
            this.newProductForm.get('productPrice')!.value - this.discount
          ).toFixed(2);
        }
      });
  }

  public getFinalProductPrice(): void {
    this.finalProductPrice = (+this.newProductForm.get('productPrice')!.value -
      this.getDiscountSize()).toFixed(2);
  }

  public getDiscountSize(): number {
    this.discount = +(+this.newProductForm.get('productPrice')!.value / 100 *
      +this.newProductForm.get('productDiscount')!.value).toFixed(2);

    return this.discount;
  }

  public showDiscountInput(): void {
    this.isShownDiscountInput = true;
  }

  public hideDiscountInput(): void {
    this.isShownDiscountInput = false;
    this.newProductForm.get('productDiscount')!.setValue(null);
  }

  //// WORK WITH GROUPS. FOOD GROUP.
  public checkIsFoodGroupEmpty(): void {
    this.subscribeTo = this.foodGroupList$.pipe(
      map(foodGroups => {
        return foodGroups.every(group => !group.status);
      })
    ).subscribe(isFoodGroupEmpty => {
      this.isFoodGroupEmpty = isFoodGroupEmpty;
    });
  }

  //// WORK WITH GROUPS. INGREDIENT GROUP.
  public addIngredient(event: MatChipInputEvent): void {
    const value: string = (this.transformFirstLetterToUpperCase(event.value) || '').trim();

    if (value && !this.dishIngredientSelectedList.includes(value)) {
      this.dishIngredientSelectedList.push(value);
    } else {
      this.toastService.addToast({
        toastType: ToastType.Info,
        title: `Ingredient '${value}' has already been added`
      });
    }

    event.chipInput!.clear();
    this.newProductForm.get('productCtrl')!.setValue(null);
  }

  public removeIngredient(product: string): void {
    const index = this.dishIngredientSelectedList.indexOf(product);

    if (index >= 0) {
      this.dishIngredientSelectedList.splice(index, 1);
    }
  }

  public selectedIngredient(event: MatAutocompleteSelectedEvent): void {
    const selectedProduct = event.option.viewValue;

    if (!this.dishIngredientSelectedList.includes(selectedProduct)) {
      this.dishIngredientSelectedList.push(selectedProduct);
    } else {
      this.toastService.addToast({
        toastType: ToastType.Info,
        title: `Ingredient '${selectedProduct}' has already been added`
      });
    }

    this.productInput.nativeElement.value = '';
    this.newProductForm.get('productCtrl')!.setValue(null);
  }

  public filterIngredient(value: string): (string | undefined)[] {
    const filterValue = value.toLowerCase();

    return this.dishIngredientList.filter((product: string | undefined) =>
      product?.toLowerCase().includes(filterValue)
    );
  }

  public getDishIngredientList(): void {
    this.subscribeTo = this.vendorProductsService
      .getDishIngredientList()
      .subscribe({
        next: (ingredients: IIngredients) => {
          this.dishIngredientList = ingredients.ingredients.map(
            (item: IFoodGroupStatus) => item.name
          );
          this.defineDishIngredientFilteredList();
        }
      });
  }

  public defineDishIngredientFilteredList(): void {
    this.dishIngredientFilteredList = this.newProductForm
      .get('productCtrl')!
      .valueChanges.pipe(
        startWith(null),
        map((product: string | null) =>
          product
            ? this.filterIngredient(product)
            : this.dishIngredientList!.slice()
        )
      );
  }

  public deletePhoto(): void {
    if(this.productId)
      this.subscribeTo = this.assetsService.deleteProductPhoto(this.productId).subscribe(() => {
        this.productDataChanged.emit();
      });
  }

  // OTHER
  public switchCurrentTab(tabName: string): void {
    this.currentTab = tabName;
  }

  public processStatus(status: string): string {
    if (status == ProductStatusEnum.InModeration) {
      return 'On moderation';
    }

    return status;
  }

  public navigateToProductsPage(): void {
    void this.router.navigate([
      `feed/vendor/vendor-account/${this.product?.vendor.id}/products/`,
    ]);
  }

  public transformFirstLetterToUpperCase(value: string): string {

    return value[0].toUpperCase() + value.slice(1);
  }

  public handleFoodGroupClick(id: number): void {
    this.store.dispatch(new UpdateFoodGroups(id));
  }

  public handleDietaryTypeListClick(id: number): void {
    this.store.dispatch(new UpdateDietaryTags(id));
  }
  getImage(path: string) {
    return this.assetsService.getUrl(path);
  }
  onFileSelected(event: Event) {
    const target = event.target as HTMLInputElement;
    const file: File = (target.files as FileList)[0];

    this.subscribeTo = this.translateService.get('VENDOR_PRODUCT.UPLOAD_PHOTO')
      .subscribe((uploadPhotoText) => {
        const modalConfig: MatDialogConfig<ImageEditData> = {
          data: {
            title: uploadPhotoText,
            subtitle: this.modalHint,
            aspectRatio: 960 / 540,
            image: file,
            onSubmit: this.submitPhoto.bind(this),
          },
          maxHeight: 'calc(100vh - 32px)',
        };

        this.dialog.open(ImageEditComponent, modalConfig);
      });
  }
  submitPhoto(file: File | Blob) {
    // blob to file is necessary for image path changes
    file = new File([file], Date.now().toString() + '.jpg', { type: 'image/jpeg' });
    this.dialog.closeAll();
    if(this.productId)
      this.subscribeTo = this.assetsService.uploadProductPhoto(file, this.productId)
        .subscribe((response) => {
          this.productDataChanged.emit();
        });
  }
}
