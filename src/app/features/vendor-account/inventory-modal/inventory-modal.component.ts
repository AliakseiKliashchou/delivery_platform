import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { INVENTORY_DISPLAYED_COLUMNS } from 'src/app/shared/constants/vendor-table';

@Component({
  selector: 'app-inventory-modal',
  templateUrl: './inventory-modal.component.html',
  styleUrls: ['./inventory-modal.component.scss']
})
export class InventoryModalComponent implements OnInit {

  public dataSource: string[] = [];
  public displayedColumns: string[] = INVENTORY_DISPLAYED_COLUMNS;
  public searchInput: FormGroup = new FormGroup({
    searchInput: new FormControl(''),
  });
  public radioInput = new FormControl('');

  ngOnInit(): void {
    this.radioInput.valueChanges.subscribe(newInput => {
      if(newInput != null && parseInt(newInput!) > 10000) {
        const lastInputValue = newInput.toString().slice(0, newInput.toString().length - 1);

        this.radioInput.patchValue(lastInputValue);
      }
    });
  }

  public onSearch(searchValue: string): void {
    // api call
  }

  public clearSearchInput(): void {
    this.searchInput.get('searchInput')?.patchValue('');
  }

}
