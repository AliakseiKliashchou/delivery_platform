import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

import { IVendorAccountData } from '../../login-page/login-page.component';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';

@Component({
  selector: 'app-vendor-account-page-sidebar',
  templateUrl: './vendor-account-page-sidebar.component.html',
  styleUrls: ['./vendor-account-page-sidebar.component.scss'],
})
export class VendorAccountPageSidebarComponent
  extends UnsubscribeComponent
  implements OnInit
{
  @Output() onLogout = new EventEmitter<boolean>();
  @Input() vendorData!: any;

  public removeCookie() {
    this.onLogout.emit();
  }

  public sections = [
    {
      title: 'Dashboard',
    },
    {
      title: 'Orders',
      quantity: 0,
    },
    {
      title: 'Products',
      quantity: 0,
    },
  ];

  public activeItem: string | undefined;

  public pages = [
    {
      title: 'Account',
    },
    {
      title: 'Billing',
      quantity: 0,
    },
    {
      title: 'Settings',
    },
    {
      title: 'Support',
    },
  ];

  constructor(
    private vendorAccountService: VendorAccountService,
    private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.subscribeTo = this.vendorAccountService
      .getVendorData()
      .subscribe((data: IVendorAccountData) => {
        this.vendorData = data.user;
        this.sections[1].quantity = this.vendorData.orders_amount;
        this.sections[2].quantity = this.vendorData.products.length;
      });

    this.activeItem = this.sections.find(section => this.router.url.includes(section.title.toLowerCase()))?.title;
  }

  public onSelectItem(item: string): void {
    if (item === 'Logout') {
      this.removeCookie();
    }

    this.activeItem = item;
  }
}
