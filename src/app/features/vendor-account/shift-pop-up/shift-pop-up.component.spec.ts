import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftPopUpComponent } from './shift-pop-up.component';

describe('ShiftPopUpComponent', () => {
  let component: ShiftPopUpComponent;
  let fixture: ComponentFixture<ShiftPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShiftPopUpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShiftPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
