import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { ReportDateModalComponent } from '../report-date-modal/report-date-modal.component';
import { ReportDateRangeModalComponent } from '../report-date-range-modal/report-date-range-modal.component';
import { getModalConfig } from 'src/app/shared/utils/getModalConfig';

@Component({
  selector: 'app-create-report-modal',
  templateUrl: './create-report-modal.component.html',
  styleUrls: ['./create-report-modal.component.scss']
})
export class CreateReportModalComponent {

  public reportType!: string;

  constructor(
    private dialog: MatDialog,
    private matDialogRef: MatDialogRef<CreateReportModalComponent>
  ) {}

  public chooseReport(): void {
    switch(this.reportType) {
      case "1":
        this.openNextDialog(ReportDateModalComponent, 500, 276);
        break;
      case "2":
        this.openNextDialog(ReportDateRangeModalComponent, 500, 328);
        break;
      case "3":
      case "4":
      case "5":
        //api call
        this.matDialogRef.close();
        break;
    }
  }

  private openNextDialog(dialogClass: any, width: number, height: number): void {
    this.dialog.open(
      dialogClass,
      getModalConfig(width, height, 'app-delete-dialog')
    );
    this.matDialogRef.close();
  }

}
