import { SelectionModel } from '@angular/cdk/collections';
import { formatDate } from '@angular/common';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

import { AgroexToastService } from 'ngx-agroex-toast';
import { CookieService } from 'ngx-cookie-service';
import { debounceTime, Observable } from 'rxjs';

import { IProduct } from 'src/app/core/models/product.model';
import { ISelecterFilterOptions } from 'src/app/shared/components/dropdown/dropdown.component';
import { DIETARY_TAGS_LIST, DISPLAYED_COLUMNS, FOOD_GROUPS_LIST, STATUSES_LIST, } from 'src/app/shared/constants/vendor-table';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';
import { TOAST_CONFIG } from '../../../shared/constants/toast-messages';
import { ProductStatusEnum } from '../../../shared/enums/product-status.enum';
import { INewProduct } from '../../../shared/interfaces/new-product.interface';
import { IProductData } from '../../../shared/interfaces/product-from-list.interface';
import { IdSharedService } from '../../../shared/services/id-shared.service';
import { VendorProductsService } from '../../../shared/services/vendor-products.service';
import { ArchiveProductDialogComponent } from '../../archive-product-dialog/archive-product-dialog.component';

import { IVendorAccountData } from '../../login-page/login-page.component';
import { RemoveProductDialogComponent } from '../../remove-product-dialog/remove-product-dialog.component';
import { FoodGroupEnum } from 'src/app/shared/enums/food-group.enum';
import { DietaryTagsListEnum } from 'src/app/shared/enums/dietary-tags-list.enum';


@Component({
  selector: 'app-vendor-account-page-products',
  templateUrl: './vendor-account-page-products.component.html',
  styleUrls: ['./vendor-account-page-products.component.scss'],
})
export class VendorAccountPageProductsComponent
  extends UnsubscribeComponent
  implements OnInit {
  @ViewChild(MatPaginator) set matPaginator(paginator: MatPaginator) {
    this.vendorData.paginator = paginator;
  }

  @ViewChild(MatSort) set matSort(sort: MatSort) {
    sort.disableClear = true;
    this.vendorData.sort = sort;
  }

  public today = formatDate(new Date(), 'dd MMM YYYY', 'en-US');
  public vendorData = new MatTableDataSource<IProductData>();
  public selection = new SelectionModel<IProductData>(true, []);
  public numSelected: number = 0;
  public clickedRows = new Set<IProductData>();

  public foodGroupsList: FoodGroupEnum[] = FOOD_GROUPS_LIST;
  public statusesList: ProductStatusEnum[] = STATUSES_LIST;
  public dietaryTagsList: DietaryTagsListEnum[] = DIETARY_TAGS_LIST;
  public displayedColumns: string[] = DISPLAYED_COLUMNS;

  public appliedStatusesFilterOptions: string[] = [];
  public appliedDietaryTagsFilterOptions: string[] = [];
  public appliedfoodGroupsFilterOptions: string[] = [];

  public isNoFilterApplied: boolean = true;
  public searchResult$!: Observable<IVendorAccountData>;
  public searchResultProducts: string[] = [];
  public isSearchResultOpen: boolean = false;
  public searchInputGroup: FormGroup = new FormGroup({
    searchInput: new FormControl(''),
  });
  public isActionMenuOpen: boolean = false;
  public expandedProduct!: IProductData | null;
  public expandedFoodGroups!: IProductData | null;
  public idProduct!: number;

  public resetFilters() {
    this.vendorAccountService.updateAppliedStatusesFilterOptions([]);
    this.vendorAccountService.updateAppliedfoodGroupsFilterOptions([]);
    this.vendorAccountService.updateAppliedDietaryTagsFilterOptions([]);

    this.vendorAccountService
      .getVendorData(
        this.searchInputGroup.get('searchInput')?.value,
        this.appliedStatusesFilterOptions,
        this.appliedfoodGroupsFilterOptions,
        this.appliedDietaryTagsFilterOptions
      )
      .subscribe((data: IVendorAccountData) => {
        this.vendorData = new MatTableDataSource(data.user.products);
      });

    !this.appliedDietaryTagsFilterOptions.length &&
    !this.appliedStatusesFilterOptions.length &&
    !this.appliedfoodGroupsFilterOptions.length
      ? (this.isNoFilterApplied = true)
      : (this.isNoFilterApplied = false);
  }

  // public updateFilter(filter: string, options: string []): void {
  //   const updatedFilter: {[key: string]: void} = {
  //     'Status': this.vendorAccountService.updateAppliedStatusesFilterOptions(options),
  //     'Foodgroup': this.vendorAccountService.updateAppliedfoodGroupsFilterOptions(options),
  //     'Dietarytag':this.vendorAccountService.updateAppliedDietaryTagsFilterOptions(options),
  //   };

  //   return updatedFilter[filter]
  // }

  public onFilterApplied(appliedFilters: ISelecterFilterOptions) {
    // this.updateFilter(appliedFilters.filterName, appliedFilters.selectedOptions)
    switch (appliedFilters.filterName) {
      case 'Status':
        this.vendorAccountService.updateAppliedStatusesFilterOptions(
          appliedFilters.selectedOptions
        );
        break;
      case 'Food group':
        this.vendorAccountService.updateAppliedfoodGroupsFilterOptions(
          appliedFilters.selectedOptions
        );
        break;
      case 'Dietary tag':
        this.vendorAccountService.updateAppliedDietaryTagsFilterOptions(
          appliedFilters.selectedOptions
        );
        break;
      default:
        this.vendorAccountService.updateAppliedStatusesFilterOptions([]);
        this.vendorAccountService.updateAppliedfoodGroupsFilterOptions([]);
        this.vendorAccountService.updateAppliedDietaryTagsFilterOptions([]);
    }

    this.vendorAccountService
      .getVendorData(
        this.searchInputGroup.get('searchInput')?.value,
        this.appliedStatusesFilterOptions,
        this.appliedfoodGroupsFilterOptions,
        this.appliedDietaryTagsFilterOptions
      )
      .subscribe((data: IVendorAccountData) => {
        this.vendorData = new MatTableDataSource(data.user.products);
      });

    !this.appliedDietaryTagsFilterOptions.length &&
    !this.appliedStatusesFilterOptions.length &&
    !this.appliedfoodGroupsFilterOptions.length
      ? (this.isNoFilterApplied = true)
      : (this.isNoFilterApplied = false);
  }

  constructor(
    public _MatPaginatorIntl: MatPaginatorIntl,
    private vendorAccountService: VendorAccountService,
    private vendorProductsService: VendorProductsService,
    private changeDetector: ChangeDetectorRef,
    private dialog: MatDialog,
    private toastService: AgroexToastService,
    private router: Router,
    private cookieService: CookieService,
    private readonly idSharedService: IdSharedService
  ) {
    super();
  }

  ngOnInit(): void {
    this._MatPaginatorIntl.itemsPerPageLabel = 'Rows per page:';
    this.subscribeTo =
      this.vendorAccountService.currentAppliedStatusesFilterOptions.subscribe(
        (options: string[]) => (this.appliedStatusesFilterOptions = options)
      );
    this.subscribeTo =
      this.vendorAccountService.currentAppliedfoodGroupsFilterOptions.subscribe(
        (options: string[]) => (this.appliedfoodGroupsFilterOptions = options)
      );
    this.subscribeTo =
      this.vendorAccountService.currentAppliedDietaryTagsFilterOptions.subscribe(
        (options: string[]) => (this.appliedDietaryTagsFilterOptions = options)
      );
    this.subscribeTo = this.vendorAccountService
      .getVendorData(
        '',
        this.appliedStatusesFilterOptions,
        this.appliedfoodGroupsFilterOptions,
        this.appliedDietaryTagsFilterOptions
      )
      .subscribe((data: IVendorAccountData) => {
        this.vendorData = new MatTableDataSource(data.user.products);
      });
    this.subscribeTo = this.searchInputGroup
      .get('searchInput')!
      .valueChanges.pipe(debounceTime(400))
      .subscribe((value: string) => {
        this.onSearch(value);
      });
  }

  public filterData(data: string): string {
    const firstIndex = data.indexOf(' ') + 1;
    const lastIndex = firstIndex + this.today.length;

    return data.substring(firstIndex, lastIndex);
  }

  public onSearch(searchValue: string): void {
    this._clearResults();
    this.searchResult$ = this.vendorAccountService.getVendorData(
      searchValue,
      this.appliedStatusesFilterOptions,
      this.appliedfoodGroupsFilterOptions,
      this.appliedDietaryTagsFilterOptions
    );
    this.searchResult$.subscribe((data: IVendorAccountData) => {
      data.user.products.forEach((product: IProduct) => {
        this.searchResultProducts.push(product.name);
      });
      this.vendorData = new MatTableDataSource(data.user.products);
    });
  }

  private _clearResults(): void {
    this.searchResultProducts = [];
  }

  public closeSearchResult(): void {
    this._clearResults();
  }

  public clearSearchInput(): void {
    const value = '';
    this.searchInputGroup.get('searchInput')?.patchValue(value);
    this._clearResults();
  }

  public setSearchValue(value: string): void {
    if (this.searchInputGroup.get('searchInput')?.value === value) {
      this._clearResults();

      return;
    }
    this.searchInputGroup.get('searchInput')?.patchValue(value);
  }

  public resetOneOption(filterName: string, option: string): void {
    switch (filterName) {
      case 'Status':
        this.appliedStatusesFilterOptions.splice(
          this.appliedStatusesFilterOptions.indexOf(option),
          1
        );
        this.vendorAccountService.updateAppliedStatusesFilterOptions(
          this.appliedStatusesFilterOptions
        );
        break;
      case 'Food group':
        this.appliedfoodGroupsFilterOptions.splice(
          this.appliedfoodGroupsFilterOptions.indexOf(option),
          1
        );
        this.vendorAccountService.updateAppliedfoodGroupsFilterOptions(
          this.appliedfoodGroupsFilterOptions
        );
        break;
      case 'Dietary tag':
        this.appliedDietaryTagsFilterOptions.splice(
          this.appliedDietaryTagsFilterOptions.indexOf(option),
          1
        );
        this.vendorAccountService.updateAppliedDietaryTagsFilterOptions(
          this.appliedDietaryTagsFilterOptions
        );
        break;
      default:
        this.vendorAccountService.updateAppliedStatusesFilterOptions([]);
        this.vendorAccountService.updateAppliedfoodGroupsFilterOptions([]);
        this.vendorAccountService.updateAppliedDietaryTagsFilterOptions([]);
    }
    this.vendorAccountService
      .getVendorData(
        this.searchInputGroup.get('searchInput')?.value,
        this.appliedStatusesFilterOptions,
        this.appliedfoodGroupsFilterOptions,
        this.appliedDietaryTagsFilterOptions
      )
      .subscribe((data: IVendorAccountData) => {
        this.vendorData = new MatTableDataSource(data.user.products);
      });
  }

  public ngAfterViewChecked(): void {
    this.numSelected = this.selection.selected.length;
    !this.appliedDietaryTagsFilterOptions.length &&
    !this.appliedStatusesFilterOptions.length &&
    !this.appliedfoodGroupsFilterOptions.length
      ? (this.isNoFilterApplied = true)
      : (this.isNoFilterApplied = false);
    this.changeDetector.detectChanges();
  }

  public isAllSelected(): boolean {
    this.numSelected = this.selection.selected.length;
    const numRows = this.vendorData.data.length;

    return this.numSelected === numRows;
  }

  public toggleAllRows(): void {
    if (this.isAllSelected()) {
      this.selection.clear();

      return;
    }
    this.selection.select(...this.vendorData.data);
  }

  public checkboxLabel(row?: IProductData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }

    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.id
    }`;
  }

  public toggleActionMenu(): void {
    this.isActionMenuOpen = !this.isActionMenuOpen;
  }

  public openRemoveProductDialog(): void {
    const removeProductDialogRef = this.dialog.open(
      RemoveProductDialogComponent,
      {
        width: '500px',
        data: {
          productId: this.expandedProduct!.id,
        },
      }
    );

    this.subscribeTo = removeProductDialogRef.afterClosed().subscribe({
      next: (result) => {
        if (result) {
          this.subscribeTo = this.vendorAccountService
            .getVendorData()
            .subscribe({
              next: (data: IVendorAccountData) => {
                this.vendorData = new MatTableDataSource(data.user.products);
              },
            });
        }
      },
    });
  }

  public openArchiveProductDialog(): void {
    const archiveProductDialogRef = this.dialog.open(
      ArchiveProductDialogComponent,
      {
        width: '500px',
        data: {
          productId: this.expandedProduct!.id,
        },
      }
    );

    this.subscribeTo = archiveProductDialogRef
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.subscribeTo = this.vendorAccountService
            .getVendorData()
            .subscribe({
              next: (data: IVendorAccountData) => {
                this.vendorData = new MatTableDataSource(data.user.products);
              },
            });
        }
      });
  }

  public sendToModeration(): void {
    this.subscribeTo = this.vendorProductsService
      .updateProductStatus(this.expandedProduct!.id, {
        status: ProductStatusEnum.InModeration,
      })
      .subscribe({
        next: () => {
          this.toastService.addToast(
            TOAST_CONFIG.forProduct.success.sentOnModeration
          );
          this.subscribeTo = this.vendorAccountService
            .getVendorData()
            .subscribe((data: IVendorAccountData) => {
              this.vendorData = new MatTableDataSource(data.user.products);
            });
        },
        error: () => {
          this.toastService.addToast(TOAST_CONFIG.byDefault.error);
        },
      });
  }

  public capitalizeFirstWordsLetters(productName: string): string {
    return productName.replace(/(^\w{1})|(\s+\w{1})/g, (letter) =>
      letter.toUpperCase()
    );
  }

  public createProductCopy(): INewProduct {
    return {
      name: this.getDuplicatedProductName(),
      weight: this.expandedProduct?.weight,
      price: this.expandedProduct?.price,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      cooking_time: this.expandedProduct?.cooking_time,
      ingredients: this.expandedProduct?.ingredients,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      dietary_tags: this.expandedProduct?.dietary_tags,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      food_groups: this.expandedProduct?.food_groups,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      number_purchases: 0,
      count: 30,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      product_discount_percent: this.expandedProduct?.product_discount_percent,
    };
  }

  public duplicateProduct(): void {
    this.subscribeTo = this.vendorProductsService
      .publishNewProductAsDraft(this.createProductCopy())
      .subscribe({
        next: () => {
          this.toastService.addToast(
            TOAST_CONFIG.forProduct.success.duplicated
          );
          this.subscribeTo = this.vendorAccountService
            .getVendorData()
            .subscribe((data: IVendorAccountData) => {
              this.vendorData = new MatTableDataSource(data.user.products);
            });
        },
        error: () => {
          this.toastService.addToast(TOAST_CONFIG.byDefault.error);
        },
      });
  }

  public getDuplicatedProductName(): string {
    let duplicatedProductName = getDuplicatedName(this.expandedProduct!.name);
    let productIndex = 0;

    function getDuplicatedName(productName: string): string {
      return productName + '*';
    }

    while (productIndex !== this.vendorData.filteredData.length) {
      for (
        productIndex = 0;
        productIndex < this.vendorData.filteredData.length;
        productIndex++
      ) {
        if (
          duplicatedProductName.toLowerCase() ===
          this.vendorData.filteredData[productIndex].name.toLowerCase()
        ) {
          duplicatedProductName = getDuplicatedName(duplicatedProductName);
          break;
        }
      }
    }

    return duplicatedProductName;
  }

  public getDiscountAmount(
    price: number,
    productDiscountPercent: number
  ): string {
    return ((price / 100) * productDiscountPercent).toFixed(2);
  }


  public idProductFn(id: number | undefined): void {
    this.idSharedService.setIdProduct(Number(id));
  }

  public getProperStatusName(status: string): string {
    return status.replace('In moderation', 'On moderation');
  }

}
