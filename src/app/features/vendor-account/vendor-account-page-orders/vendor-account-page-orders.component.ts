import { AfterViewChecked, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginatorIntl, PageEvent } from '@angular/material/paginator';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS,MomentDateAdapter } from '@angular/material-moment-adapter';

import { TranslateService } from '@ngx-translate/core';
import { Select, Store } from '@ngxs/store';
import { debounceTime, distinctUntilChanged, fromEvent, Observable } from 'rxjs';

import { GetVendorOrders } from '../../vendors/ngxs/vendors.actions';
import { VendorsState } from '../../vendors/ngxs/vendors.state';
import { CreateReportModalComponent } from '../create-report-modal/create-report-modal.component';
import { InventoryModalComponent } from '../inventory-modal/inventory-modal.component';
import { ShiftPopUpComponent } from '../shift-pop-up/shift-pop-up.component';
import { IOrdersList } from 'src/app/core/models/vendor.model';
import { ISelecterFilterOptions } from 'src/app/shared/components/dropdown/dropdown.component';
import { VENDOR_ORDER_FORMATS } from 'src/app/shared/constants/date-format';
import { ORDER_STATUSES_LIST,ORDERS_DISPLAYED_COLUMNS } from 'src/app/shared/constants/vendor-table';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { OrderStatusEnum } from 'src/app/shared/enums/order-status.enum';
import { LocalstorageService } from 'src/app/shared/services/localstorage.service';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';
import { getModalConfig } from 'src/app/shared/utils/getModalConfig';

@Component({
  selector: 'app-vendor-account-page-orders',
  templateUrl: './vendor-account-page-orders.component.html',
  styleUrls: ['./vendor-account-page-orders.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: VENDOR_ORDER_FORMATS },
  ],
})
export class VendorAccountPageOrdersComponent
extends UnsubscribeComponent
implements OnInit, AfterViewChecked {

  @ViewChild('searchInput') searchInput!: ElementRef;

  public statusesList: OrderStatusEnum[] = ORDER_STATUSES_LIST;
  public appliedStatusesFilterOptions: string[] = [];
  public searchInputGroup: FormGroup = new FormGroup({
    searchInput: new FormControl(''),
  });
  public dataSource!: IOrdersList;
  public displayedColumns: string[] = ORDERS_DISPLAYED_COLUMNS;
  public startTime: string = "8:15 AM";
  public date: Date = new Date();
  public formDate: FormControl = new FormControl(this.date);
  public isHideCompleted: boolean = false;
  public length: number = 0;
  public pageSize: number = 5;
  public page: number = 1;

  constructor(
    private vendorAccountService: VendorAccountService,
    private dialog: MatDialog,
    private store: Store,
    private localStorage: LocalstorageService,
    private matPaginatorIntl: MatPaginatorIntl,
    private translate: TranslateService
  ) {
    super();
  }

  @Select(VendorsState.getVendorOrders) vendorOrders$!: Observable<IOrdersList>;

  ngOnInit(): void {
    this.subscribeTo =
      this.vendorAccountService.currentAppliedStatusesFilterOptions.subscribe(
        (options: string[]) => (this.appliedStatusesFilterOptions = options)
      );

    this.store.dispatch(new GetVendorOrders({
      required_date: this.date.toISOString().substring(0, 10),
      page: this.page,
      per_page: this.pageSize,
    }, this.localStorage.getValue("vendor_id")));

    this.subscribeTo = this.vendorOrders$.subscribe((data: IOrdersList) => {
      this.dataSource = data;
      this.length = data.meta.total_records;
      this.pageSize = data.meta.per_page;
    });
  }

  ngAfterViewChecked(): void {
    this.subscribeTo = fromEvent(this.searchInput.nativeElement, 'input').pipe(debounceTime(500)).pipe(distinctUntilChanged()).subscribe(() => {
      this.updateOrders();
    });

    this.matPaginatorIntl.itemsPerPageLabel = this.translate.instant("VENDOR_ACCOUNT_ORDERS.PAGINATION_LABEL");
  }

  public onFilterApplied(appliedFilters: ISelecterFilterOptions): void {
    this.vendorAccountService.updateAppliedStatusesFilterOptions(appliedFilters.selectedOptions);
    this.isHideCompleted = false;
    this.updateOrders();
  }

  public resetOption(option: string): void {
    this.appliedStatusesFilterOptions.splice(this.appliedStatusesFilterOptions.indexOf(option), 1);
    this.isHideCompleted = false;
    this.updateOrders();
  }

  public clearSearchInput(): void {
    this.searchInputGroup.get('searchInput')?.patchValue('');
  }

  public onPreviousDate(): void {
      this.date.setDate(this.date.getDate() - 1);
      this.formDate.setValue(this.date);
      this.onDateChange();
  }

  public onNextDate(): void {
    if (this.date < ( d => new Date(d.setDate(d.getDate()-1)) )(new Date)) {
      this.date.setDate(this.date.getDate() + 1);
      this.formDate.setValue(this.date);
      this.onDateChange();
    }
  }

  public onToggleHideCompleted(): void {
    this.isHideCompleted = !this.isHideCompleted;
    this.vendorAccountService.updateAppliedStatusesFilterOptions(this.isHideCompleted ? this.statusesList.slice(0, 5) : []);
    this.updateOrders();
  }

  public openInventory(): void {
    this.dialog.open(
      InventoryModalComponent,
      getModalConfig(850, 750, 'app-delete-dialog')
    );
  }

  public startShift(): void {
    // add condition for pop up
    this.dialog.open(
      ShiftPopUpComponent,
      getModalConfig(500, 244, 'app-delete-dialog')
    );
  }

  public createReport(): void {
    this.dialog.open(
      CreateReportModalComponent,
      getModalConfig(500, 460, 'app-delete-dialog')
    );
  }

  public handlePageEvent(event: PageEvent): void {
    this.updateOrders();
    this.length = event.length;
  }

  public onDateChange(): void {
    this.updateOrders();
  }

  private updateOrders(): void {
    this.store.dispatch(new GetVendorOrders({
      statuses: this.appliedStatusesFilterOptions.map(status => status.toLowerCase().replace(/ /g,"_")).join(","),
      required_date: this.date.toISOString().substring(0, 10),
      page: this.page,
      per_page: this.pageSize,
      order_id: this.searchInputGroup.get('searchInput')?.value
    }, this.localStorage.getValue("vendor_id")));
  }
}
