import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorAccountPageOrdersComponent } from './vendor-account-page-orders.component';

describe('VendorAccountPageOrdersComponent', () => {
  let component: VendorAccountPageOrdersComponent;
  let fixture: ComponentFixture<VendorAccountPageOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorAccountPageOrdersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorAccountPageOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
