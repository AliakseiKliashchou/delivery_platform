import { Component} from '@angular/core';

@Component({
  selector: 'app-login-attempts',
  templateUrl: './login-attempts.component.html',
  styleUrls: ['./login-attempts.component.scss']
})
export class LoginAttemptsComponent {
  //remove code below when it will be necessary to development this page
  public buttonText: string = 'MODERATOR_LOG_IN.RESET_PASSWORD';
  public buttonColor: string = '';
  public colors: string[] = ['red', 'green', 'blue'];
  public currentColorIndex: number = 0;

  changeButton() {
    this.buttonText = 'SORRY_BUT_THIS_PAGE_IS_NOT_AVAILABLE_YET';
    this.buttonColor = this.colors[this.currentColorIndex];
    this.currentColorIndex = (this.currentColorIndex + 1) % this.colors.length;
  }

}
