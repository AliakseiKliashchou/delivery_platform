import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxsReduxDevtoolsPluginModule } from "@ngxs/devtools-plugin";
import { NgxsModule } from "@ngxs/store";
import { ProductState } from "../features/cart/ngxs/product.state";
import { VendorsState } from '../features/vendors/ngxs/vendors.state';


@NgModule({
    declarations: [],
    imports: [
      CommonModule,
      NgxsModule.forRoot([VendorsState, ProductState]),
      NgxsReduxDevtoolsPluginModule.forRoot()
    ],
  })
  export class StoreModule { }
