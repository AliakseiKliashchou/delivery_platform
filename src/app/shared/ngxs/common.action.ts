export class SetSpinnerLoading {
  static readonly type = '[Loading] SetSpinnerLoading';

  constructor(public isLoading: boolean) {}
}
