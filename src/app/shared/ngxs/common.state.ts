import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SetSpinnerLoading } from "./common.action";

export interface CommonStateModel {
  isSpinnerLoading: boolean;
}

@State<CommonStateModel>({
  name: 'commonState',
  defaults: {
    isSpinnerLoading: false,
  }
})

@Injectable()
export class CommonState {

  @Selector()
  static getIsSpinnerLoading({ isSpinnerLoading }: CommonStateModel): boolean {
    return isSpinnerLoading;
  }

  @Action(SetSpinnerLoading)
  setSpinnerLoading(
    { patchState }: StateContext<CommonStateModel>,
    { isLoading }: SetSpinnerLoading
  ): void {
    patchState({
      isSpinnerLoading: isLoading
    });
  }
}
