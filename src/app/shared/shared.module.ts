import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NgOtpInputModule } from 'ng-otp-input';
import { ImageCropperModule } from 'ngx-image-cropper';

import { LoaderInterceptor } from '../core/interceptors/loader.interceptor';
import { SpinnerLoaderComponent } from '../features/spinner-loading/spinner-loading.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { ChangeVendorComponent } from './components/change-vendor/change-vendor.component';
import { CookiePopupComponent } from './components/cookie-popup/cookie-popup.component';
import {
  CookiePreferencesDialogComponent
} from './components/cookie-preferences-dialog/cookie-preferences-dialog.component';
import { CookiePreferencesItemComponent } from './components/cookie-preferences-item/cookie-preferences-item.component';
import { CountDownComponent } from './components/count-down/count-down.component';
import { DeleteOrderComponent } from './components/delete-order/delete-order.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { DropdownMultiselectComponent } from './components/dropdown-multiselect/dropdown-multiselect.component';
import { DropdownRadioComponent } from './components/dropdown-radio/dropdown-radio.component';
import { ImageEditComponent } from './components/image-edit/image-edit.component';
import {
  InlineSelectorExtendedComponent
} from './components/inline-selector-extended/inline-selector-extended.component';
import { LocationPopUpComponent } from './components/location-pop-up/location-pop-up.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import {
  OrderConfirmationWindowComponent
} from './components/order-confirmation-window/order-confirmation-window.component';
import { PopupComponent } from './components/popup/popup.component';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { ResultBarComponent } from './components/result-bar/result-bar.component';
import { SearchAddressComponent } from './components/search-address/search-address.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SvgIconComponent } from './components/svg-icon/svg-icon.component';
import { TitleComponent } from './components/title/title.component';
import { VendorCardComponent } from './components/vendor-card/vendor-card.component';
import { VendorLoginDialogComponent } from './components/vendor-login-dialog/vendor-login-dialog.component';
import { VerificationCodeComponent } from './components/verification-code/verification-code.component';
import { DirectivesModule } from './directives.module';
import { PriceRoundPipe } from './pipes/price-round.pipe';
import { ScheduleCalcPipe } from './pipes/schedule-calc.pipe';
import { SubstringEmphasizePipe } from './pipes/substring-emphasize.pipe';
import { VendorSignUpBannerComponent } from './vendor-sign-up-banner/vendor-sign-up-banner.component';

@NgModule({
  declarations: [
    TitleComponent,
    VendorCardComponent,
    SearchBarComponent,
    SearchAddressComponent,
    SubstringEmphasizePipe,
    ScheduleCalcPipe,
    SvgIconComponent,
    PopupComponent,
    ProductCardComponent,
    NavigationComponent,
    ResultBarComponent,
    DropdownMultiselectComponent,
    DropdownRadioComponent,
    CartItemComponent,
    DeleteOrderComponent,
    LocationPopUpComponent,
    SearchAddressComponent,
    InlineSelectorExtendedComponent,
    ChangeVendorComponent,
    OrderConfirmationWindowComponent,
    VendorSignUpBannerComponent,
    VerificationCodeComponent,
    CookiePopupComponent,
    CookiePreferencesDialogComponent,
    CookiePreferencesItemComponent,
    VendorLoginDialogComponent,
    CountDownComponent,
    DropdownComponent,
    SpinnerLoaderComponent,
    PriceRoundPipe,
    ImageEditComponent,
  ],
  imports: [
    DirectivesModule,
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    TranslateModule,
    MatFormFieldModule,
    MatSelectModule,
    MatRadioModule,
    AngularSvgIconModule,
    TranslateModule,
    RouterModule,
    MatMenuModule,
    NgOtpInputModule,
    MatSlideToggleModule,
    FormsModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    ImageCropperModule,
  ],
  exports: [
    DirectivesModule,
    VendorCardComponent,
    TitleComponent,
    SearchBarComponent,
    SearchAddressComponent,
    PopupComponent,
    ScheduleCalcPipe,
    SubstringEmphasizePipe,
    SvgIconComponent,
    ProductCardComponent,
    NavigationComponent,
    ResultBarComponent,
    DropdownMultiselectComponent,
    DropdownRadioComponent,
    CartItemComponent,
    LocationPopUpComponent,
    InlineSelectorExtendedComponent,
    VendorSignUpBannerComponent,
    VerificationCodeComponent,
    CookiePopupComponent,
    CountDownComponent,
    TranslateModule,
    DropdownComponent,
    SpinnerLoaderComponent,
  ],
  providers: [
    ScheduleCalcPipe,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
  ]
})
export class SharedModule {
}
