import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ClickOutsideDirective } from './directives/click-outside.directive';
import { HideAfterDirective } from './directives/hide-after.directive';
import { StickedClassDirective } from './directives/sticked-class-directive.directive';
import { ObserveVisibilityDirective } from './directives/visibility.directive';

@NgModule({
  declarations: [
    ClickOutsideDirective,
    HideAfterDirective,
    StickedClassDirective,
    ObserveVisibilityDirective

  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ClickOutsideDirective,
    HideAfterDirective,
    StickedClassDirective,
    ObserveVisibilityDirective
  ]
})
export class DirectivesModule {
}
