import { animate, group, query, stagger,style, transition, trigger } from '@angular/animations';

export const listAnimation = trigger(
    'inOutAnimation', 
    [
      transition(
        ':enter', 
        [
          style({ height: '0px', overflow: 'hidden' }),
          group([animate('300ms ease-in', style({ opacity:1, height: '!' }))]),
        ]
      ),
      transition(
        ':leave', 
        [
          style({ height: '!', overflow: 'hidden' }),
          group([animate('250ms ease-out', style({ opacity:0, height: '0px' }))]),
        ]
      )
    ]
  );