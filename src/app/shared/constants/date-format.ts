import { DateFormat } from "../interfaces/date-format-interface";

export const VENDOR_ORDER_FORMATS: DateFormat = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'MMM D',
    monthYearLabel: 'L',
    dateA11yLabel: 'L',
    monthYearA11yLabel: 'L',
  },
};

export const REPORT_ORDERS_FORMATS: DateFormat = {
  parse: {
    dateInput: 'L',
  },
  display: {
    dateInput: 'MMM D, YYYY',
    monthYearLabel: 'L',
    dateA11yLabel: 'L',
    monthYearA11yLabel: 'L',
  },
};
