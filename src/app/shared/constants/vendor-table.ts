import { DietaryTagsListEnum } from "../enums/dietary-tags-list.enum";
import { FoodGroupEnum } from "../enums/food-group.enum";
import { OrderStatusEnum } from "../enums/order-status.enum";
import { ProductStatusEnum } from "../enums/product-status.enum";

export const FOOD_GROUPS_LIST: FoodGroupEnum[] = [
  FoodGroupEnum.Meat,
  FoodGroupEnum.Fruit,
  FoodGroupEnum.Fast_food,
  FoodGroupEnum.Fish,
  FoodGroupEnum.Vegetables,
  FoodGroupEnum.Seafood,
  FoodGroupEnum.Smoothies,
  FoodGroupEnum.Confection];
export const STATUSES_LIST: ProductStatusEnum[] = [
  ProductStatusEnum.Published,
  ProductStatusEnum.InModeration,
  ProductStatusEnum.Declined,
  ProductStatusEnum.Archived,
  ProductStatusEnum.Draft];
export const DIETARY_TAGS_LIST: DietaryTagsListEnum[] = [
  DietaryTagsListEnum.Vegan,
  DietaryTagsListEnum.Gluten_free,
  DietaryTagsListEnum.Lactose_free,
  ];
export const ORDER_STATUSES_LIST: OrderStatusEnum[] = [
  OrderStatusEnum.Queue,
  OrderStatusEnum.InProgress,
  OrderStatusEnum.Ready,
  OrderStatusEnum.InDelivery,
  OrderStatusEnum.Cancelled,
  OrderStatusEnum.Completed
  ];
export const DISPLAYED_COLUMNS: string[] = ['select', 'name', 'approval_status', 'price','quantity', 'food_groups', 'updated_at', 'actions'];
export const ORDERS_DISPLAYED_COLUMNS: string[] = ['id', 'type', 'status', 'price', 'items', 'payment', 'customer', 'created_at'];
export const INVENTORY_DISPLAYED_COLUMNS: string[] = ['productName', 'sold', 'left', 'quantity', 'availability'];
