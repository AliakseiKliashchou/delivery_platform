export const ENDPOINTS = {
  accountLocation: 'api/accounts/location/',
  createCustomerLocation: (id: number) => `api/accounts/customer/${id}/location`,
  customerLocation: `api/accounts/location/`,
  vendors: 'api/accounts/vendors',
  vendorsInactive: 'api/accounts/vendors/inactive',
  getVendorOrders: (id: number) => `api/accounts/vendor/${id}/orders/`,
  products: 'api/products/',
  productsSearch: 'api/search',
  productsOnModeration: 'api/moderation/products/on_moderation',
  review: 'api/moderation/review/',
  updateStatus: 'api/products/update_status/',
  login: 'api/accounts/login',
  vendorProductPhoto: 'api/products/photo',
  accounts: 'api/accounts/',
  resetPasswordLink: 'api/accounts/send_reset_pwd_link',
};

export const ERRORS = {
  WRONG_VERIFICATION_CODE: 'Wrong verification code',
  VERIFICATION_CODE_EXPIRED: 'Verification email code expired or not requested'
};

export const cookiePreferences = [
  {
    title: 'Targeting and Advertising',
    subtitle: 'Used to create profiles or personalize content to enhance your shopping experience.',
    disabled: false,
    isBorderDisplay: true,
    isChecked: true,
  },
  {
    title: 'Analytics',
    subtitle: 'Provide statistical information on site usage, e.g., web analytics, so we can improve this website over time.',
    disabled: false,
    isBorderDisplay: true,
    isChecked: true,
  },
  {
    title: 'Functional',
    subtitle: 'Enables enhanced functionality, such as videos and live chat. If you do not allow these, then some or all of these functions may not work properly.',
    disabled: false,
    isBorderDisplay: true,
    isChecked: true,
  },
  {
    title: 'Essential (Required)',
    subtitle: 'We use browser cookies that are necessary for the site to work as intended.',
    disabled: true,
    isBorderDisplay: false,
    isChecked: true,
  },
];

export const ACCEPT_COOKIE = 'HSF_ACCEPT_COOKIE';
