export const LEAVE_REVIEW = '#';
export const VENDOR_MAP = '#';
export const CONTACT_US = 'https://www.fao.org/contact-us/en/';
export const GET_HELP = '#';
export const LOGIN_AS_VENDOR = '/feed/vendor/login';
export const LOGIN_AS_COURIER = '#';
