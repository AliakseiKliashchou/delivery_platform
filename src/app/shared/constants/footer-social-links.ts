export const INSTAGRAM = 'https://www.instagram.com/fao/';
export const FACEBOOK = 'https://www.facebook.com/UNFAO/?ref=mf';
export const LINKEDIN =
  // eslint-disable-next-line max-len
  'https://www.linkedin.com/authwall?trk=bf&trkInfo=AQGy6r1gco5_6wAAAYCDZTo4QawXemtQOHihse6G7YdRpvHnB7FX_H1PXyAlFRm3cS_uSo-1XyjU7Zu6aBbDyRG_KqVOzI8nlfpsd6Vb_vLekeGKBRv1JQvEHe26AjKy7My5n_Y=&originalReferer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Ffao';
export const TWITTER = 'https://twitter.com/FAO';
export const YOUTUBE = 'https://www.youtube.com/user/FAOoftheUN';
