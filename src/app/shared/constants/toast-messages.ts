import { ToastType } from "ngx-agroex-toast";

export const TOAST_CONFIG = {
  byDefault: {
    error: {
      toastType: ToastType.Error,
      title: 'Something went wrong',
    },
    success: {
      toastType: ToastType.Success,
      title: 'Your actions have been processed successfully',
    }
  },

  forProduct: {
    error: {
      requiredFields: {
        toastType: ToastType.Error,
        title: 'Required fields',
        message: 'You must fill in all required fields'
      },
      productDoesNotExist: {
        toastType: ToastType.Error,
        title: `Product with this Id does not exist`
      }
    },
    success: {
      updated: {
        toastType: ToastType.Success,
        title: 'Updated',
      },
      archived: {
        toastType: ToastType.Success,
        title: 'Archived',
      },
      savedAsDraft: {
        toastType: ToastType.Success,
        title: 'Saved as draft'
      },
      sentOnModeration: {
        toastType: ToastType.Success,
        title: 'Sent for moderation',
      },
      removed: {
        toastType: ToastType.Success,
        title: 'Removed',
      },
      duplicated: {
        toastType: ToastType.Success,
        title: 'Duplicated',
      }
    }
  }
};
