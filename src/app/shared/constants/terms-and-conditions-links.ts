export const FAO_DOCUMENT_REPOSITORY =
  'https://www.fao.org/documents#querystring=JmVuZHN0cmluZz0x';
export const OPEN_ACCESS_POLICY = 'https://www.fao.org/3/I9461EN/I9461en.pdf';
export const OPEN_DATA_LICENSING_POLICY =
  'https://www.fao.org/3/ca7570en/ca7570en.pdf';
export const STATISTICAL_DATABASES_TERMS_OF_USE =
  'https://www.fao.org/contact-us/terms/db-terms-of-use/en/';
export const PRIVACY_POLICY =
  'https://www.fao.org/contact-us/privacy-policy/en/';
