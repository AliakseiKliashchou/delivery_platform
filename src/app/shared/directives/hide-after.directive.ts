import { Directive, HostListener, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

class HideAfterContext {
  public appHideAfter = 0;
  public counter = 0;
}

@Directive({
  selector: '[appHideAfter]',
})
export class HideAfterDirective implements OnInit {
  @Input('appHideAfterThen')
  placeholder: TemplateRef<any> | null = null;

  @Input('appHideAfter')
  set delay(value: number | null) {
    this._delay = value ?? 0;
    this.context.appHideAfter = this.context.counter = this._delay / 1000;    
  }

  private _delay: number = 0;  

  private context = new HideAfterContext();

  constructor(
    private viewContainerRef: ViewContainerRef,
    private template: TemplateRef<any>
  ) { }

  ngOnInit(): void {
    this.viewContainerRef.createEmbeddedView(this.template, this.context);

    const intervalId = setInterval(() => {
      this.context.counter--;
    }, 1000)

    setTimeout(() => {
      this.viewContainerRef.clear();
      
      if (this.placeholder) {
        this.viewContainerRef.createEmbeddedView(this.placeholder)
      }
      clearInterval(intervalId);
    }, this._delay);
  }
}
