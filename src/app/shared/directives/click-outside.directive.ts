import { AfterViewInit, Directive, ElementRef, EventEmitter, Inject, OnDestroy, Output } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { fromEvent, filter } from "rxjs";

import { UnsubscribeComponent } from "./unsubscribe.component";

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[clickOutside]'
})
export class ClickOutsideDirective extends UnsubscribeComponent implements AfterViewInit, OnDestroy {
  @Output() clickOutside = new EventEmitter<void>();

  constructor (
    private element: ElementRef,
    @Inject(DOCUMENT) private document: Document) {
    super();
  }

  public ngAfterViewInit(): void {
    this.subscribeTo = fromEvent(this.document, 'click').pipe(
      filter((event: Event) => !this.isInside(event.target as HTMLElement))
    ).subscribe(() => {
      this.clickOutside.emit();
    });
  }

  isInside(elementToCheck: HTMLElement): boolean {
    return elementToCheck === this.element.nativeElement || this.element.nativeElement.contains(elementToCheck);
  }
}
