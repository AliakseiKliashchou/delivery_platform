/* eslint-disable @angular-eslint/no-input-rename */
import { Directive, ElementRef, HostListener, Input, OnInit } from "@angular/core";

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[stickedClass]'
})

export class StickedClassDirective {
  @Input('stickedClass') stickedClass: string = '';
  @Input('transitionTop') transitionTop: number = 0;

  constructor(private element: ElementRef) {
    element.nativeElement.style.position = 'sticky';
  }

  @HostListener("window:scroll", ["$event"])
  onScroll($event: MouseEvent): void {
    if(!this.stickedClass)
      return;

    const bounding = this.element.nativeElement.getBoundingClientRect();
    const { classList } = this.element.nativeElement;

    bounding.top <= this.transitionTop ? classList.add(this.stickedClass) : classList.remove(this.stickedClass);
  }
}
