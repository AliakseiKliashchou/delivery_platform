import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { delay, filter, skip, Subject } from 'rxjs';

import { UnsubscribeComponent } from './unsubscribe.component';

@Directive({
  selector: '[appVisibility]',
})
export class ObserveVisibilityDirective extends UnsubscribeComponent
  implements OnDestroy, OnInit, AfterViewInit
{
  @Input() debounceTime = 0;
  @Input() threshold = 1;

  @Output() visible = new EventEmitter<HTMLElement>();
  @Output() notVisible = new EventEmitter<HTMLElement>();

  private observer: IntersectionObserver | undefined;
  private subject$ = new Subject<{
    entry: IntersectionObserverEntry;
    observer: IntersectionObserver;
  }>();

  private isVisible(element: HTMLElement): Promise<unknown> {
    return new Promise(resolve => {
      const observer = new IntersectionObserver(([entry]) => {
        resolve(entry.intersectionRatio === 1);
        observer.disconnect();
      });
  
      observer.observe(element);
    });
  }

  constructor(private element: ElementRef) {
    super();
  }

  ngOnInit(): void {
    this.createObserver();
  }

  ngAfterViewInit(): void {
    this.startObservingElements();
  }

  private createObserver(): void {
    const options = {
      rootMargin: '0px',
      threshold: this.threshold,
    };

    const isIntersecting = (entry: IntersectionObserverEntry) =>
      entry.isIntersecting || entry.intersectionRatio > 0;

    this.observer = new IntersectionObserver((entries, observer) => {
      entries.forEach((entry) => {
        if (isIntersecting(entry)) {
          this.subject$.next({ entry, observer });
        }
      });
    }, options);
  }

  private startObservingElements(): void  {
    if (!this.observer) {
      return;
    }

    this.observer.observe(this.element.nativeElement);

    this.subscribeTo = this.subject$
      .pipe( delay(this.debounceTime), skip(1), filter(Boolean) )
      .subscribe(async ({ entry }) => {
        const target = entry.target as HTMLElement;
        const isStillVisible = await this.isVisible(target);
        isStillVisible ? this.visible.emit(target) : this.notVisible.emit(target);
      });
  }
}
