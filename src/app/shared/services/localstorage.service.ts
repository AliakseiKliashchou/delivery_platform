import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {
  public getValue(key: string): any {
    return localStorage.getItem(key);
  }

  public setValue(key: string, value: any): void {
    localStorage.setItem(key, value);
  }
}
