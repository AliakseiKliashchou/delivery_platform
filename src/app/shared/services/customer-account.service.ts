import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Observable, take } from 'rxjs';

import { ILoggedInCustomer } from '../../core/models/product.model';
import { ApiService } from '../../core/services';
import { SetOrderDetails, UpdateCustomerProfileData } from '../../features/cart/ngxs/product.actions';
import {
  CustomerProfileData
} from '../../features/customer/customer-account-profile-page/customer-account-profile-page.component';
import { UserLocationService } from './user-location.service';
import { CookieService } from 'ngx-cookie-service';

export interface CustomerDataVerificationResponse {
  success: boolean;
  refresh: string;
  access: string;
}

export interface CustomerCodeVerificationResponse {
  success: boolean;
  phone: string;
  errors?: string[];
}

@Injectable({
  providedIn: 'root'
})
export class CustomerAccountService {
  constructor(
    private readonly _apiService: ApiService,
    private readonly cookieService: CookieService,
    private readonly store: Store,
    private readonly _router: Router,
    private readonly _userLocationService: UserLocationService,
  ) {
  }

  public customerSignUp(): Observable<ILoggedInCustomer> {

    return this._apiService.get(`api/accounts/customer/me`);
  }

  public sendVerificationCode(
    phone: string
  ): Observable<CustomerCodeVerificationResponse> {

    return this._apiService.post(`api/accounts/customer/send_code`, {
      phone: phone,
    });
  }

  public verifyCustomerPhone(
    verificationCode: number,
    phone: string
  ): Observable<CustomerDataVerificationResponse> {

    return this._apiService.post(`api/accounts/customer/verify_code`, {
      verification_code: verificationCode,
      phone: phone,
    });
  }

  public setAccessToken(data: string) {
    this.cookieService.set(
      'hsf-account-customer-access-token', //todo refactor to constants
      data,
      undefined,
      '/',
    );
  }

  public setRefreshToken(data: string) {
    this.cookieService.set(
      'hsf-account-customer-refresh-token',
      data,
      undefined,
      '/'
    );
  }

  public getIsAccountAddress() {

    return !!(this.cookieService.get('hsf-account-address'));
  }

  public setCustomerData(): void {
    this.customerSignUp()
      .pipe(take(1))
      .subscribe((customerData: ILoggedInCustomer) => {
        if (customerData) {
          const { id, phone, email, location, customer_name } = customerData.customer_data!;

          this.store.dispatch(
            new SetOrderDetails({
              customerId: id,
              customerPhone: phone,
              customerComments: '',
              customerEmail: email,
              customerName: customer_name,
              customerLocation: location,
            })
          );
          this._userLocationService.confirmDraftData(id);
        }
      });
  }

  public navigateToFeed(): Promise<boolean> {
     return this._router.navigate(['/feed']);
  }

  public navigateToOrder(): Promise<boolean> {
    return this._router.navigate(['/feed/order']);
  }

  public updateUserData(
    id: number,
    data: CustomerProfileData
  ): Observable<void> {
    return this._apiService.put(
      `api/accounts/customer/${id}`,
      {
        ...data
      }
    ).pipe(
      tap(() => {
        this.store.dispatch(new UpdateCustomerProfileData(data));
      })
    );
  }
}
