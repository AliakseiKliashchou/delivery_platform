import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { catchError, Observable, Subject, tap } from 'rxjs';

import { ENDPOINTS } from '../constants/endpoints';
import { removeCookie } from '../utils/removeCookie';
import { ApiService } from 'src/app/core/services/api.service';
import { IVendorAccountData } from 'src/app/features/login-page/login-page.component';

export interface IEmailVerificationResponse {
  success: boolean;
  errors?: string[];
  vendor_email: string;
}

export interface ISCNVerificationResponse {
  identity_token?: string;
  vendor_data?: IVendorDataForSignUp;
  success?: boolean;
  errors?: string[];
}

export interface IVendorDataForSignUp {
  city: string;
  country: string;
  email: string;
  id: number;
  license_number: string;
  phone: string;
  scratch_card_number: string;
  vendor_name: string;
  verification_code: number;
}
export interface signUpResponse {
  access?: string;
  refresh?: string;
}

export interface IgeneralVendorData {
  license_number: string;
  scratch_card_number: string;
  country: string;
  city: string;
  email: string;
  phone: string;
  vendor_name: string;
  password: string;
  identity_token: string;
}
export interface loginData {
  email: string;
  password: string;
  user_type: string;
}

@Injectable({
  providedIn: 'root',
})
export class VendorAccountService {
  private appliedStatusesFilterOptions = new Subject<string[]>();
  currentAppliedStatusesFilterOptions =
    this.appliedStatusesFilterOptions.asObservable();

  private appliedfoodGroupsFilterOptions = new Subject<string[]>();
  currentAppliedfoodGroupsFilterOptions =
    this.appliedfoodGroupsFilterOptions.asObservable();

  private appliedDietaryTagsFilterOptions = new Subject<string[]>();
  currentAppliedDietaryTagsFilterOptions =
    this.appliedDietaryTagsFilterOptions.asObservable();

  public updateAppliedStatusesFilterOptions(options: string[]) {
    this.appliedStatusesFilterOptions.next(options);
  }
  public updateAppliedfoodGroupsFilterOptions(options: string[]) {
    this.appliedfoodGroupsFilterOptions.next(options);
  }
  public updateAppliedDietaryTagsFilterOptions(options: string[]) {
    this.appliedDietaryTagsFilterOptions.next(options);
  }

  constructor(
    private readonly cookieService: CookieService,
    private readonly router: Router,
    private readonly _apiService: ApiService,
  ) {}

  public sendVerificationEmail(
    scratchCardNumber: string | null
  ): Observable<IEmailVerificationResponse> {
    return this._apiService.post(
      `api/accounts/vendor/send_verification_email`,
      {
        scratch_card_number: scratchCardNumber,
      }
    );
  }

  public verifySCN(
    verificationCode: number,
    email: string
  ): Observable<ISCNVerificationResponse> {
    return this._apiService.post(`api/accounts/vendor/verify_scn`, {
      verification_code: verificationCode,
      email: email,
    });
  }

  public signUpVendor(
    generalVendorData: IgeneralVendorData
  ): Observable<signUpResponse> {
    return this._apiService.post(`api/accounts/vendor/signup`, {
      ...generalVendorData,
    });
  }

  public loginVendor(loginData: loginData): Observable<signUpResponse> {
    return this._apiService.post(`api/accounts/login`, { ...loginData });
  }

  public getVendorData(
    value?: string,
    status?: string[],
    food_group?: string[],
    dietary_tag?: string[]
  ): Observable<IVendorAccountData> {
    let nameForSearch;
    let statusOptions;
    let foodGroupOptions;
    let dietaryTagOptions;

    value ? (nameForSearch = value) : (nameForSearch = '');
    status &&
      status!.indexOf('Onmoderation') !== -1 &&
      (status![status!.indexOf('Onmoderation')] = 'On moderation');
    status ? (statusOptions = status!.join()) : (statusOptions = '');
    food_group &&
      food_group!.indexOf('FastFood') !== -1 &&
      (food_group![food_group!.indexOf('FastFood')] = 'Fast Food');
    food_group
      ? (foodGroupOptions = food_group!.join())
      : (foodGroupOptions = '');
    dietary_tag &&
      dietary_tag!.indexOf('GlutenFree') !== -1 &&
      (dietary_tag![dietary_tag!.indexOf('GlutenFree')] = 'Gluten Free');
    dietary_tag &&
      dietary_tag!.indexOf('LactoseFree') !== -1 &&
      (dietary_tag![dietary_tag!.indexOf('LactoseFree')] = 'Lactose Free');
    dietary_tag
      ? (dietaryTagOptions = dietary_tag!.join())
      : (dietaryTagOptions = '');
    statusOptions = statusOptions.replace('On moderation', 'On moderation');

    return this._apiService.get(
      `api/accounts/vendors/me?name=${nameForSearch}&status=${statusOptions}&food_group=${foodGroupOptions}&dietary_tag=${dietaryTagOptions}`
    );
  }

  public sentResetLink(email: string): Observable<{ [key: string]: boolean }> {
    return this._apiService.post(ENDPOINTS.resetPasswordLink, {
      email: email,
    });
  }

  public resetPassword(
    identity_token: string,
    password: string
  ): Observable<signUpResponse> {
    return this._apiService.post(`api/accounts/reset_password`, {
      identity_token,
      password,
    });
  }

  public refreshToken(refreshToken: string, role: string) {
    return this._apiService
      .post(`api/accounts/refresh?refresh`, {
        refresh: refreshToken,
      })
      .pipe(
        catchError((error) => {
          this.returnToLogin(role);
          throw error;
        }),
        tap((tokens: signUpResponse) => {
          this.updateTokens(tokens, role);
        })
      );
  }

  public returnToLogin(role: string): void {
    removeCookie(`hsf-account-${role}-access-token`);
    removeCookie(`hsf-account-${role}-refresh-token`);
    role === 'customer'
      ? this.router.navigate([`/feed`])
      : this.router.navigate([`/feed/${role}/login`]);
  }

  public updateTokens(tokens: signUpResponse, role: string): void {
    removeCookie(`hsf-account-${role}-access-token`);
    removeCookie(`hsf-account-${role}-refresh-token`);
    this.cookieService.set(
      `hsf-account-${role}-refresh-token`,
      tokens.refresh!,
      undefined,
      '/'
    );
    this.cookieService.set(
      `hsf-account-${role}-access-token`,
      tokens.access!,
      undefined,
      '/'
    );
  }
}
