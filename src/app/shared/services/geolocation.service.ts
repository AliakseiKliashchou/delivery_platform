import { Injectable } from '@angular/core';
import { MapGeocoder, MapGeocoderResponse } from '@angular/google-maps';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GeolocationService {

  constructor(private mapGeocoder: MapGeocoder) {}


  public geocodePosition(location: google.maps.LatLngLiteral | google.maps.LatLng | null | undefined):  Observable<MapGeocoderResponse> {
    return this.mapGeocoder.geocode({ location });
  }

  public geocodeAddress(address: string): Observable<MapGeocoderResponse> {
    return this.mapGeocoder.geocode({ address });
  }

  public geocodePlace(placeId: string): Observable<MapGeocoderResponse> {
    return this.mapGeocoder.geocode({ placeId });
  }

  public searchPlaces(place: string): Observable<google.maps.places.AutocompletePrediction[]> {
    return Observable.create((observer: Observer<google.maps.places.AutocompletePrediction[] | null>) => {
      const displaySuggestions = (
        predictions: google.maps.places.AutocompletePrediction[] | null,
        status:  google.maps.places.PlacesServiceStatus): void | undefined => {
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
          observer.error(status);
        } else {
          observer.next(predictions);
          observer.complete();
        }
      };
      const autocompleteService = new google.maps.places.AutocompleteService();
      autocompleteService.getPlacePredictions({ input: place }, displaySuggestions);
    });
  }
}
