import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

import { ApiService } from '../../core/services/api.service';
import { ENDPOINTS } from '../constants/endpoints';


@Injectable({
  providedIn: 'root',
})
export class SearchService {
  constructor(private apiService: ApiService) {
  }

  public getProducts(filterData?: any): Observable<any> {
    let params: HttpParams = new HttpParams();
    if (filterData) {
      Object.keys(filterData).forEach(
        (key: string) => filterData[key] && (params = params.append(key, filterData[key]))
      );
    }

    return this.apiService.get(ENDPOINTS.productsSearch, params);
  }
}
