import { Injectable } from "@angular/core";
import { IToastOptions } from "ngx-agroex-toast";

import { ProductStatusEnum } from "../enums/product-status.enum";
import { TOAST_CONFIG } from "../constants/toast-messages";

@Injectable({
  providedIn: 'root',
})
export class ToastMessagesService {
  public chooseSuccessMessageForNewProductStatus(
    newStatus: ProductStatusEnum
  ): IToastOptions {
    switch (newStatus) {
      case ProductStatusEnum.Archived:
        return TOAST_CONFIG.forProduct.success.archived;
      case ProductStatusEnum.InModeration:
        return TOAST_CONFIG.forProduct.success.sentOnModeration;
      case ProductStatusEnum.Draft:
        return TOAST_CONFIG.forProduct.success.savedAsDraft;
      default:
        return TOAST_CONFIG.byDefault.success;
    }
  }
}
