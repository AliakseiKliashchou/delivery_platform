import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModeratorMessageService {

  private measageSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public message$ = this.measageSubject.asObservable();

  public setMessage(message: string) {
    this.measageSubject.next(message);
  }
}
