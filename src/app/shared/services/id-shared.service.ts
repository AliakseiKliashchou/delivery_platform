import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IdSharedService {
  private idProductSubject: BehaviorSubject<number | undefined> = new BehaviorSubject<number | undefined>(undefined);
  public idProduct$ = this.idProductSubject.asObservable();

  public setIdProduct(id: number) {
    this.idProductSubject.next(id);
  }
}
