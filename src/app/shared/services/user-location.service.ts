import { Injectable } from '@angular/core';

import { Store } from '@ngxs/store';
import { Observable, of, map } from 'rxjs';

import { ApiService } from 'src/app/core/services/api.service';
import { ILocationResponse } from '../interfaces/location-response.interface';
import { SetCustomerLocation } from 'src/app/features/cart/ngxs/product.actions';
import { ICustomerLocation } from 'src/app/core/models/product.model';
import { ENDPOINTS } from '../constants/endpoints';

@Injectable({
  providedIn: 'root',
})
export class UserLocationService {
  location: ICustomerLocation | null = null;
  draftLocation: ICustomerLocation | null = null;
  locationId: number = 0;
  customerLocationId: number = 0;

  constructor(
    private readonly _apiService: ApiService,
    private readonly store: Store,
  ) {}

  setLocation(customerId: number | undefined, data: ICustomerLocation): Observable<ILocationResponse | undefined> {
    if(customerId === undefined) {
      this.setDraftData(data);
      
      return of(undefined);
    } else {
      this.setDraftData(data);

      return this._apiService.post(ENDPOINTS.createCustomerLocation(customerId), this.draftLocation)
        .pipe(
          map((response: ILocationResponse) => {
              this.location = this.draftLocation;
              this.locationId = response.location_id;
              this.customerLocationId = response.customer_location_id;

              return response;
            }),
          );
    }
  }

  private setDraftData(data: ICustomerLocation): void {
    this.draftLocation = data;
  }

  public confirmDraftData(customerId: number): void {
    if(this.draftLocation !== null) {
      this.store.dispatch(new SetCustomerLocation(this.draftLocation));
    }
  }
}
