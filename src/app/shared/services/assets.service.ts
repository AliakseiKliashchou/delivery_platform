import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs";

import { ENDPOINTS } from "../constants/endpoints";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root',
})
export class AssetsService {

  constructor(
    private httpClient: HttpClient,
  ) {}

    public getUrl(path: string): string {
        return `${environment.ASSETS_DOMAIN}/${path}`;
    }
    public uploadProductPhoto(file: File | Blob, id: number): Observable<unknown> {
        const fd = new FormData();

        fd.append('image', file);

        return this.httpClient.post(`${environment.API_DOMAIN}/${ENDPOINTS.vendorProductPhoto}/${id}`, fd);
    }

    public deleteProductPhoto(id: number): Observable<unknown> {
      return this.httpClient.delete(`${environment.API_DOMAIN}/${ENDPOINTS.vendorProductPhoto}/${id}`);
    }
}
