import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class VisibilityService {
  private subject = new Subject<string | void>();
  private visibility: string = '';

  public updateVisibility(status:string): void {
    this.visibility = status;
    this.subject.next(this.visibility);
  }

  public getVisibiitySubscription(): Observable<any> {
    return this.subject.asObservable();
  }
}
