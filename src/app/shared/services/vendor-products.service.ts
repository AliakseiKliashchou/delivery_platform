import { Injectable } from "@angular/core";
import { forkJoin, Observable } from "rxjs";

import { ApiService } from "src/app/core/services/api.service";
import { ProductStatusEnum } from "../enums/product-status.enum";
import { IFoodGroupTag } from "../interfaces/food-group-tag.interface";
import { INewProduct } from "../interfaces/new-product.interface";
import { IProductById } from "../interfaces/product-by-id.interface";
import {
  IDietaryTag,
  IFoodGroups,
  IIngredients,
} from "../interfaces/product-lists.interface";
import { ENDPOINTS } from "../constants/endpoints";

@Injectable({
  providedIn: "root",
})
export class VendorProductsService {
  constructor(private _apiService: ApiService) {
  }

  public publishNewProductAsDraft(newProductData: INewProduct) {
    return this._apiService.post(ENDPOINTS.products, { ...newProductData });
  }

  public getProduct(productId: number): Observable<IProductById> {
    return this._apiService.get(`${ENDPOINTS.products}${productId}`);
  }

  public updateProductInfo(
    productId: number,
    newProductData: INewProduct
  ): Observable<IProductById> {
    return this._apiService.put(`${ENDPOINTS.products}${productId}`, newProductData);
  }

  public removeProduct(productId: number): Observable<void> {
    return this._apiService.delete(`${ENDPOINTS.products}${productId}`);
  }

  public updateProductStatus(
    productId: number,
    newStatus: { status: ProductStatusEnum }
  ): Observable<Object> {
    return this._apiService.put(
      `${ENDPOINTS.products}update_status/${productId}`,
      newStatus
    );
  }

  public getDishIngredientList(): Observable<IIngredients> {
    return this._apiService.get(`${ENDPOINTS.products}ingredients`);
  }

  public getFoodGroupList(): Observable<IFoodGroups> {
    return this._apiService.get(`${ENDPOINTS.products}food_groups`);
  }

  public getDietaryTypeList(): Observable<IDietaryTag> {
    return this._apiService.get(`${ENDPOINTS.products}dietary_tags`);
  }

  public getAllowedFoodGroupsForTag(tagId: number): Observable<IFoodGroupTag> {
    return this._apiService.get(`${ENDPOINTS.products}allowed_food_groups_for_tag/${tagId}`);
  }

  public getAllowedFoodGroupsForTags(tagsIds: number[]): Observable<IFoodGroupTag[]> {
    return forkJoin(tagsIds.map((tagId) => this.getAllowedFoodGroupsForTag(tagId)));
  }
}
