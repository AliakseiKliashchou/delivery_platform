import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { forkJoin, map, Observable } from 'rxjs';

import { IOrdersFilter, IOrdersList, IVendorsFilter, IVendorsList } from '../../core/models/vendor.model';
import { ApiService } from '../../core/services/api.service';
import { ENDPOINTS } from '../constants/endpoints';

@Injectable({
  providedIn: 'root'
})
export class VendorsService {
  constructor(
    private apiService: ApiService,
    private cookieService: CookieService) {
  }

  public getVendors(filterData?: IVendorsFilter | any): Observable<IVendorsList> {

    let params: HttpParams = new HttpParams();

    Object.keys(filterData).forEach(
      (key: string) => filterData[key] && (params = params.append(key, filterData[key]))
    );
    const location = this.cookieService.get('hsf-account-location');
    if (location) {
      params = params.append('latitude', location.split(',')[0]);
      params = params.append('longitude', location.split(',')[1]);
      params = params.append('radius', 10000);
    }

    const vendors = this.apiService.get(ENDPOINTS.vendors, params);
    const vendorsInactive = this.apiService.get(ENDPOINTS.vendorsInactive, params);

    return forkJoin([vendors, vendorsInactive]).pipe(
      map(([activeVendors, inactiveVendors]) => {
        return {
          meta: activeVendors.meta,
          vendors: [...activeVendors.vendors, ...inactiveVendors.vendors]
        };
      })
    );
  }

  public getVendorOrders(filterData: IOrdersFilter | any, vendorId: number): Observable<IOrdersList> {
    let params: HttpParams = new HttpParams();

    Object.keys(filterData).forEach(
      (key: string) => filterData[key] && (params = params.append(key, filterData[key]))
    );

    return this.apiService.get(ENDPOINTS.getVendorOrders(vendorId), params);
  }
}
