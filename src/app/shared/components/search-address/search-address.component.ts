import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs';

import { GeolocationService } from "../../services/geolocation.service";
import { UnsubscribeComponent } from '../../directives/unsubscribe.component';

@Component({
  selector: 'app-search-address',
  templateUrl: './search-address.component.html',
  styleUrls: ['./search-address.component.scss']
})
export class SearchAddressComponent extends UnsubscribeComponent implements OnInit {
  @Input() marginTop: number = 10;
  @Input() title: string = 'Search by vendor, products';

  @Input() searchField: FormControl = new FormControl();
  @Output() newSelection = new EventEmitter<string>();

  public addresses: google.maps.places.AutocompletePrediction[] = [];
  public selectedAddress?: string;

  constructor (private geolocationService: GeolocationService) {
    super();
  }

  public ngOnInit(): void {
    this.subscribeTo = this.searchField.valueChanges.pipe(debounceTime(500))
      .subscribe(value => {
        if (value.length > 2) {
          if (this.searchField.getRawValue() !== this.selectedAddress) {
            this.subscribeTo = this.geolocationService.searchPlaces(value).subscribe((res) => {
              if (res.length > 0) {
                this.addresses = res;
              }
            });
          }
        } else {
          this.addresses = [];
        }
      });
  }

  public onSelect(address: google.maps.places.AutocompletePrediction): void {
    this.selectedAddress = address.description;
    this.newSelection.emit(address.place_id);
    this.searchField.setValue(address.description);
    this.addresses = [];
  }

  public onEnter(event: any) {
    event.preventDefault();
  }
}
