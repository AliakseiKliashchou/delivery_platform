import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngxs/store';
import { concat, first, take } from 'rxjs';
import { IProduct } from 'src/app/core/models/product.model';
import { AddProductToCart, RemoveAllProductsFromCart } from 'src/app/features/cart/ngxs/product.actions';
import { ProductsService } from 'src/app/features/cart/ngxs/products.service';
import { UnsubscribeComponent } from '../../directives/unsubscribe.component';

@Component({
  selector: 'app-change-vendor',
  templateUrl: './change-vendor.component.html',
  styleUrls: ['./change-vendor.component.scss']
})
export class ChangeVendorComponent extends UnsubscribeComponent implements OnInit {
  public currentVendorName: string = '';
  public newVendorName: string = '';

  public changeVendor() {
    this.store.dispatch(new RemoveAllProductsFromCart())
      .pipe(take(1))
        .subscribe(() => this.store.dispatch(new AddProductToCart(this.data.productOfNewVendor)))
  }
  

  constructor(@Inject(MAT_DIALOG_DATA) public data: { productOfNewVendor:IProduct, currentVendor:number}, 
  private store: Store, private productService:ProductsService) {
    super()
   }

  ngOnInit(): void {
    this.subscribeTo = this.productService.getProductsByVendorsId(this.data.currentVendor.toString(), "").subscribe(
      res => this.currentVendorName = res.name
      )

    this.subscribeTo = this.productService.getProductsByVendorsId(this.data.productOfNewVendor.vendor_id?.toString(), "").subscribe(
      res => this.newVendorName = res.name
      )    
  }
  
}
