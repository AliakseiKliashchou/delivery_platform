import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeVendorComponent } from './change-vendor.component';

describe('ChangeVendorComponent', () => {
  let component: ChangeVendorComponent;
  let fixture: ComponentFixture<ChangeVendorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeVendorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeVendorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
