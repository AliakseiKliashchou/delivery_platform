import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GoogleMapsModule } from "@angular/google-maps";

import { MapComponent } from "./map.component";
import { TranslateModule } from "@ngx-translate/core";


@NgModule({
  declarations: [
    MapComponent,
  ],
  imports: [
    GoogleMapsModule,
    CommonModule,
    TranslateModule
  ],
  exports: [
    MapComponent
  ]
})
export class MapModule {
}
