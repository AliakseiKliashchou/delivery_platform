import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';

import { IMarkerData } from '../../../features/location/location-modal/location-modal.component';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent {

  @Input() zoom: number = 14;
  @Input() height: string = '500px';
  @Input() width: string = '100%';
  @Input() options: google.maps.MapOptions = {
    mapTypeId: 'roadmap',
    zoomControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: true,
    maxZoom: 19,
    minZoom: 8,
    fullscreenControl: false,
    streetViewControl: false,
    mapTypeControl: false
  };
  @Input() markers: IMarkerData[] = [];

  // @ts-ignore
  @Input() center: google.maps.LatLngLiteral = google.maps.LatLngLiteral;
  @Output() newPosition = new EventEmitter<google.maps.LatLngLiteral>();

  @ViewChild(GoogleMap, { static: false }) map!: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false }) infoWindow!: MapInfoWindow;
  public markerContent: string = '';


  public zoomIn(): void {
    if (this.options.maxZoom && this.zoom < this.options.maxZoom) this.zoom++;
  }

  public zoomOut(): void {
    if (this.options.minZoom && this.zoom > this.options.minZoom) this.zoom--;
  }

  public mapClick(event: google.maps.MapMouseEvent): void {
  }

  public logCenter(): void {
  }

  public addMarker(): void {
    this.markers.push({
      position: {
        lat: this.center.lat + ((Math.random() - 0.5) * 2) / 30,
        lng: this.center.lng + ((Math.random() - 0.5) * 2) / 30,
      },
      title: 'Marker title ' + (this.markers.length + 1),
      options: { animation: google.maps.Animation.DROP },
    });
  }

  public openInfo(marker: MapMarker, content: string): void {
    this.markerContent = content;
    this.infoWindow.open(marker);
  }

  public getNewPosition(event: any): void {
      this.map.panTo({ lat: event.latLng?.lat(), lng: event.latLng?.lng() });
      this.zoom = 16;
      this.newPosition.emit({ lat: event.latLng.lat(), lng: event.latLng.lng() });
  }
}
