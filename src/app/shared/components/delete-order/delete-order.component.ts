import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { RemoveAllProductsFromCart } from 'src/app/features/cart/ngxs/product.actions';

@Component({
  selector: 'app-delete-order',
  templateUrl: './delete-order.component.html',
  styleUrls: ['./delete-order.component.scss']
}
)
export class DeleteOrderComponent {

  constructor(private store: Store) { }

  removeAllProductsFromCart() {
    this.store.dispatch(new RemoveAllProductsFromCart()); 
  }
}
