import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiePreferencesItemComponent } from './cookie-preferences-item.component';

describe('CookiePreferencesItemComponent', () => {
  let component: CookiePreferencesItemComponent;
  let fixture: ComponentFixture<CookiePreferencesItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CookiePreferencesItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CookiePreferencesItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
