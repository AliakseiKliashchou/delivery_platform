import { Component, Input, OnInit } from '@angular/core';

export interface ICookiePreferencesItem {
  title:string;
  subtitle:string;
  disabled: boolean;
  isBorderDisplay: boolean;
  isChecked: boolean;
}

@Component({
  selector: 'app-cookie-preferences-item',
  templateUrl: './cookie-preferences-item.component.html',
  styleUrls: ['./cookie-preferences-item.component.scss']
})
export class CookiePreferencesItemComponent {
  @Input() cookieItem!: ICookiePreferencesItem;
}
