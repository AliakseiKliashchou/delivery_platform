import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss'],
})
export class TitleComponent implements OnInit {
  public isTitleOrange: boolean = false;

  constructor(
    private router: Router,
    private translateService: TranslateService
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isTitleOrange = val.url.includes('moderation');
      }
    });
  }
  ngOnInit(): void {
    this.isTitleOrange = this.router.url.includes('moderation');
  }
}
