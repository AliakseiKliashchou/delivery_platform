import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorLoginDialogComponent } from './vendor-login-dialog.component';

describe('VendorLoginDialogComponent', () => {
  let component: VendorLoginDialogComponent;
  let fixture: ComponentFixture<VendorLoginDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorLoginDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorLoginDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
