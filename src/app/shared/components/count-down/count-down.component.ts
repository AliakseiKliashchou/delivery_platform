import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { UnsubscribeComponent } from '../../directives/unsubscribe.component';
import { CustomerAccountService, CustomerCodeVerificationResponse } from '../../services/customer-account.service';
import { catchError } from 'rxjs';

@Component({
  selector: 'app-count-down',
  templateUrl: './count-down.component.html',
  styleUrls: ['./count-down.component.scss'],
})
export class CountDownComponent extends UnsubscribeComponent implements OnInit {
  @Input() init!: number;
  @Input() controlValue!: string;
  @Output() onEmpty = new EventEmitter<number>();
  public count: number = 0;

  constructor(
    private readonly customerAccountService: CustomerAccountService,
  ) {
    super();
  }

  public ngOnInit(): void {
    if (this.init && this.init > 0) {
      this.count = this.init;
      this.doCountdown();
    }
  }

  public doCountdown(): void {
    setTimeout(() => {
      this.count = this.count - 1;
      this.processCount();
    }, 1000);
  }

  public processCount(): void {
    if (this.count === 0) {
      this.onEmpty.emit(this.count);
    } else {
      this.doCountdown();
    }
  }

  public resendCode(): void {
    this.subscribeTo = this.customerAccountService
      .sendVerificationCode(this.controlValue)
      .pipe(
        catchError((error) => {
          throw error;
        })
      )
      .subscribe((resp: CustomerCodeVerificationResponse) => {
        this.count = this.init;
        this.doCountdown();
      });
  }
}
