import { Component, Input } from '@angular/core';
import { FormGroup, FormGroupDirective } from "@angular/forms";
import { MAT_SELECT_CONFIG } from "@angular/material/select";
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-dropdown-radio',
  templateUrl: './dropdown-radio.component.html',
  styleUrls: ['./dropdown-radio.component.scss'],
  providers: [
    {
      provide: MAT_SELECT_CONFIG,
      useValue: {
        overlayPanelClass: 'radio-layer',
        disableOptionCentering: true
      },
    }
  ]
})
export class DropdownRadioComponent {

  @Input() label: string = 'Label';
  @Input() options!: {title: string, value: string}[]; // temporary until have contract with backend
  @Input() controlName: string = '';
  public form!: FormGroup;

  constructor(private rootFormGroup: FormGroupDirective, private cookieService: CookieService) {
  }

  public ngOnInit(): void {
    this.form = this.rootFormGroup.control;
  }

  public isCoords(option: string): boolean {
    if (!this.cookieService.get('hsf-account-location') && option === 'nearest') {
      return true;
    }

    return false;
  }
}
