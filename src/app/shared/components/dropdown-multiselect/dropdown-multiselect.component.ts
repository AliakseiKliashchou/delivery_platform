import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormGroupDirective } from '@angular/forms';

import { MatOption } from '@angular/material/core';
import { MAT_SELECT_CONFIG } from '@angular/material/select';

export interface IMultiselectOption {
  title: string;
  img: string;
}

@Component({
  selector: 'app-dropdown-multiselect',
  templateUrl: './dropdown-multiselect.component.html',
  styleUrls: ['./dropdown-multiselect.component.scss'],
  providers: [
    {
      provide: MAT_SELECT_CONFIG,
      useValue: {
        overlayPanelClass: 'multiselect-layer',
        disableOptionCentering: true
      },
    }
  ]
})
export class DropdownMultiselectComponent implements OnInit {

  @Input() label: string = 'Label';
  @Input() controlName: string = '';
  @Input() options!: IMultiselectOption[];
  public form!: FormGroup;

  constructor(private rootFormGroup: FormGroupDirective) {
  }

  public ngOnInit(): void {
    this.form = this.rootFormGroup.control;
  }

  public toggleAllSelection(id: MatOption, control: AbstractControl, arr: IMultiselectOption[]): void {
    id.selected
      ? control.patchValue(['All', ...arr.map((item: IMultiselectOption) => item.title)])
      : control.patchValue([]);
  }

  public togglePerOne(id: MatOption, control: AbstractControl, arr: IMultiselectOption[]): void {
    id.selected && id.deselect();
    control.value.length === arr.length && id.select();
  }

  public checkNotEmpty(): boolean {
    return this.form.controls[this.controlName].value?.length > 1
      && this.form.controls[this.controlName].value?.length <= this.options.length;
}
}
