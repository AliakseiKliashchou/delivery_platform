import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { MAT_MENU_DEFAULT_OPTIONS } from '@angular/material/menu';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inline-selector-extended',
  templateUrl: './inline-selector-extended.component.html',
  styleUrls: ['./inline-selector-extended.component.scss'],
  providers: [
    {
      provide: MAT_MENU_DEFAULT_OPTIONS,
      useValue: {
        overlayPanelClass: 'vendors-filter-menu',
        disableOptionCentering: true
      },
    }
  ]
})
export class InlineSelectorExtendedComponent implements OnInit {

  @Input() options: string[] = [];
  @Input() controlName: string = '';
  @Input() splitCount: number = 4;

  public form!: FormGroup;
  public inlineOptions!: string[];
  public hiddenOptions!: string[];

  constructor (
    private rootFormGroup: FormGroupDirective,
    private _translateService: TranslateService
  ) {}

  public ngOnInit(): void {
    this.inlineOptions = this.options.slice(0, this.splitCount);
    this.hiddenOptions = this.options.slice(this.splitCount);
    this.form = this.rootFormGroup.control;
  }

  public onSelect(food?: string): void  {
      this.form.controls[this.controlName].patchValue(food);
  }

  public hiddenOptionsTitle(): string {
    return this.hiddenOptions.includes(this.form.controls[this.controlName].value)
      ? this.form.controls[this.controlName].value
      : this._translateService.instant('VENDORS.MORE');
  }
}
