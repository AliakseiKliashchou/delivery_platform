import { Component, Input, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  @Input() address: string = 'Search';
  @Input() home?: string = 'Home';
  @Input() navigation?: string = '/feed';
  public isLoginPage: boolean = false;

  constructor(private router: Router) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isLoginPage = val.url.includes('login');
      }
    });
  }
}
