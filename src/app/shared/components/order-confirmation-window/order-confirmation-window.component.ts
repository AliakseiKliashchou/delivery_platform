import { Component, Inject, Input, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IVendor } from 'src/app/core/models/vendor.model';

@Component({
  selector: 'app-order-confirmation-window',
  templateUrl: './order-confirmation-window.component.html',
  styleUrls: ['./order-confirmation-window.component.scss']
})
export class OrderConfirmationWindowComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: { currentVendorName: string}) {}

}
