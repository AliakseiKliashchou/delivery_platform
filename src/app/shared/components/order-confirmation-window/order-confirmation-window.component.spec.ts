import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderConfirmationWindowComponent } from './order-confirmation-window.component';

describe('OrderConfirmationWindowComponent', () => {
  let component: OrderConfirmationWindowComponent;
  let fixture: ComponentFixture<OrderConfirmationWindowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderConfirmationWindowComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrderConfirmationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
