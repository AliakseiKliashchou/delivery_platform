import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiePreferencesDialogComponent } from './cookie-preferences-dialog.component';

describe('CookiePreferencesDialogComponent', () => {
  let component: CookiePreferencesDialogComponent;
  let fixture: ComponentFixture<CookiePreferencesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CookiePreferencesDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CookiePreferencesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
