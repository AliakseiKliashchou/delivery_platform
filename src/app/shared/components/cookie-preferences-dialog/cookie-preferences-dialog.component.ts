import { Component, OnInit } from '@angular/core';
import { ACCEPT_COOKIE, cookiePreferences } from '../../constants/endpoints';

@Component({
  selector: 'app-cookie-preferences-dialog',
  templateUrl: './cookie-preferences-dialog.component.html',
  styleUrls: ['./cookie-preferences-dialog.component.scss']
})
export class CookiePreferencesDialogComponent {
  public isLinkOpened: boolean = false;
  public cookiePreferences = cookiePreferences;

  public acceptCookie(): void {
    window.sessionStorage.setItem(ACCEPT_COOKIE, 'true');
  }
}