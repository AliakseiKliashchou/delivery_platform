import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { catchError, Observable, throwError } from 'rxjs';

import { WrongCodeComponent } from '../../../features/wrong-code/wrong-code.component';
import { ERRORS } from '../../constants/endpoints';
import { UnsubscribeComponent } from '../../directives/unsubscribe.component';
import { CustomerAccountService, CustomerDataVerificationResponse } from '../../services/customer-account.service';
import {
  ISCNVerificationResponse,
  VendorAccountService,
} from '../../services/vendor-account.service';
import { NgOtpInputConfig } from 'ng-otp-input';

@Component({
  selector: 'app-verification-code',
  templateUrl: './verification-code.component.html',
  styleUrls: ['./verification-code.component.scss'],
})
export class VerificationCodeComponent extends UnsubscribeComponent {
  @Input() controlValue!: { [key: string]: string };
  @Output() setVendorData = new EventEmitter<ISCNVerificationResponse>();
  @Output() setCustomerData = new EventEmitter<CustomerDataVerificationResponse>();

  public isVerificationCodeValid: boolean = true;
  public isVerificationCodeExpired: boolean = false;

  public counter: number = 0;
  public otp: number = 0;
  public showOtpComponent = true;
  @ViewChild('ngOtpInput', { static: false }) ngOtpInput: any;
  config: NgOtpInputConfig = {
    allowNumbersOnly: true,
    length: 4,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      textAlign: 'center',
      backgroundColor: '#EEEEEE',
      width: '48px',
      height: '48px',
      marginRight: '12px',
      borderRadius: '2px 2px 0px 0px',
      fontSize: '16px',
      fontWeight: '400',
      lineHeight: '24px',
      fontFamily: "'Inter', Helvetica, sans-serif",
      outline: 'none',
    },
    inputClass: "input-otp-component"
  };

  constructor(
    private readonly vendorAccountService: VendorAccountService,
    private readonly customerAccountService: CustomerAccountService,
    private readonly dialog: MatDialog
  ) {
    super();
  }

  public focusOnInput(event: any): void {
    event.target.classList.add('focus');
  }

  public focusOutInput(event: any): void {
    event.target.classList.remove('focus');
  }

  public onOtpChange(otp: any): void {
    this.otp = otp;
    if (otp.length >= 4) {
      this.validateOtp();
    }
  }

  public validateOtp(): void {
    this.openWrongModal(this.counter);

    if (this.controlValue['vendor']) {
      this.verifySCN();
    } else {
      this.verifyCustomerPhone();
    }
  }

  private verifySCN(): void {
    const verificationObservable: Observable<ISCNVerificationResponse> = this.vendorAccountService
      .verifySCN(+this.otp, this.controlValue['vendor']);

    this.subscribeTo = verificationObservable
      .pipe(
        catchError((error) => this.handleVerificationError(error))
      )
      .subscribe((result) => {
        this.setVendorData.emit(result);
      });
  }

  private verifyCustomerPhone(): void {
    const verificationObservable: Observable<CustomerDataVerificationResponse> = this.customerAccountService
      .verifyCustomerPhone(+this.otp, this.controlValue['customer']);

    this.subscribeTo = verificationObservable
      .pipe(
        catchError((error) => this.handleVerificationError(error))
      )
      .subscribe((result: CustomerDataVerificationResponse) => {
        this.setCustomerData.emit(result);
        this.dialog.closeAll();
      });
  }

  private handleVerificationError(error: any): Observable<never> {
    this.counter++;
    const errorCode = error.error.errors[0];

    switch (errorCode) {
      case ERRORS.WRONG_VERIFICATION_CODE:
        this.isVerificationCodeValid = false;
        break;
      case ERRORS.VERIFICATION_CODE_EXPIRED:
        this.isVerificationCodeExpired = true;
        break;
      default:
        break;
    }

    return throwError(error);
  }

  private openWrongModal(counter: number): void {
    if (counter === 4) {
      this.dialog.closeAll();
      this.dialog.open(WrongCodeComponent, {
        width: '500px',
        height: '192',
        panelClass: 'app-delete-dialog',
      });
    }
  }

}
