import { HttpParams } from '@angular/common/http';
import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { Router } from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { debounceTime, Observable } from 'rxjs';

import { ApiService } from 'src/app/core/services/api.service';
import { UnsubscribeComponent } from '../../directives/unsubscribe.component';

export interface IResult {
  vendors: ISearchResultVendor[];
  products: ISearchResultProduct[];
}

export interface ISearchResultProduct {
  id: number;
  product_name: string;
}
export interface ISearchResultVendor {
  id: number;
  vendor_name: string;
  distance: number;
}

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent
  extends UnsubscribeComponent
  implements OnInit, OnDestroy
{
  @Input() marginTop: number = 10;
  @Input() label: string = 'Search by vendor, products';
  @Input() controlName!: string;
  @Input() searchBy: string = 'both';
  public vendorsResult: ISearchResultVendor[] = [];
  public productsResult: ISearchResultProduct[] = [];

  private searchResult$!: Observable<IResult>;
  public form!: FormGroup;
  public distance: string = '...';

  @ViewChild('searchInput') searchInputRef!: ElementRef;

  constructor(
    private rootFormGroup: FormGroupDirective,
    private _apiService: ApiService,
    private _router: Router,
    private cookieService: CookieService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.form = this.rootFormGroup.control;

    this.subscribeTo = this.form.controls[this.controlName].valueChanges
      .pipe(debounceTime(400))
      .subscribe((value: string) => {
        if (value.length > 1) {
          this.getResult(value);
        }
      });
  }

  public onSearch(value: string): void {
    this.getResult(value);
  }

  public getResult(value: string): void {
    this._clearResults();
    this.searchResult$ = this.getProducts(value);
    this.searchResult$.subscribe((res: IResult) => {
      if (this.searchBy === 'vendors' || this.searchBy === 'both') {
        this.vendorsResult = res.vendors;
        this.vendorsResult = res.vendors.filter((vendor) => {
          return (
            vendor.vendor_name !== this.form.controls[this.controlName].value
          );
        });
      }
      if (this.searchBy === 'products' || this.searchBy === 'both') {
        this.productsResult = res.products.filter((product) => {
          return (
            product.product_name !== this.form.controls[this.controlName].value
          );
        });
      }
    });
  }

  public getProducts(value: string): Observable<IResult> {
    const location = this.cookieService.get('hsf-account-location');
    let params: HttpParams = new HttpParams();
    params = params.append('search_name', value);
    if (location) {
      params = params.append('latitude', location.split(',')[0]);
      params = params.append('longitude', location.split(',')[1]);
      params = params.append('radius', 10000);
    }

    return this._apiService.get(`api/products/search/popular`, params);
  }

  public goToVendorPage(item: number): void {
    this._clearResults();
    this._router.navigate(['feed', 'place', item]);
  }

  public goToSearchResultPage(product: string): void {
    this.form.controls[this.controlName].patchValue(product);
    this._clearResults();
    this._router.navigate(['feed', 'search', product]);
  }

  public onResultsClose(): void {
    this._clearResults();
  }

  private _clearResults(): void {
    this.productsResult = [];
    this.vendorsResult = [];
  }

  public showDistance(vendor: ISearchResultVendor): string {
    if (vendor.distance) {
      return vendor.distance.toFixed(1).toString();
    }

    return this.distance;
  }
  public get searchFieldHasText(): boolean {
    return this.form.get(this.controlName)?.value?.length;
  }
  public cleanSearchBar() {
    this.form.get(this.controlName)!.setValue('');
    this.getProducts('');
    this.searchInputRef.nativeElement.focus();
    if(this._router.url !== '/feed') {
      this._router.navigate(['feed', 'search']);
    }

  }

  public onEnterPress() {
    this._router.navigate(['feed', 'search', this.form.controls[this.controlName].value]);
  }
}
