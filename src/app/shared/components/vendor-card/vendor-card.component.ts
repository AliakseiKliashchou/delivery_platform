import { Component, Input, OnInit } from "@angular/core";
import { IVendor, IVendorItem, ScheduleData, ScheduleList } from "../../../core/models/vendor.model";
import { ScheduleCalcPipe } from "../../pipes/schedule-calc.pipe";

@Component({
  selector: "app-vendor-card",
  templateUrl: "./vendor-card.component.html",
  styleUrls: ["./vendor-card.component.scss"],

})
export class VendorCardComponent implements OnInit {

  public workingSchedule: ScheduleData = {
    openedHours: "",
    whenCloses: "",
    whenOpens: "",
    isStoreOpen: true,
    isClosesIn30Minutes: false,
  };

  @Input() vendor!: IVendorItem | IVendor;

  constructor(private scheduleCalcPipe: ScheduleCalcPipe) {
  };

  ngOnInit(): void {
    this.workingSchedule = this.scheduleCalcPipe.transform(this.vendor.schedule);
  }

  public distance: string = "...";

  public showDistance(): string {
    if (this.vendor.locations[0]?.distance) {
      return this.vendor.locations[0].distance;
    }

    return this.distance;
  }
}
