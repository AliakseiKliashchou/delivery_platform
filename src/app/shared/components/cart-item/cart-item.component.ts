import { Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';
import { IProduct } from 'src/app/core/models/product.model';
import { DecreaseProductQuantity, IncreaseProductQuantity, RemoveProductFromCart } from 'src/app/features/cart/ngxs/product.actions';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss']
})
export class CartItemComponent {
  @Input() cartItem!: IProduct;
  public quantity: number = 1;


  public decreaseProductQuantity(cartItem: IProduct):void {
    cartItem.quantity! > 1
      ? this.store.dispatch(new DecreaseProductQuantity(cartItem))
      : this.store.dispatch(new RemoveProductFromCart(cartItem));
  }

  public increaseProductQuantity(cartItem: IProduct): void {
    this.store.dispatch(new IncreaseProductQuantity(cartItem));
  }

  constructor(private store: Store) { }

  public removeProductFromCart(product:IProduct): void {
    this.store.dispatch(new RemoveProductFromCart(product));
  }
}
