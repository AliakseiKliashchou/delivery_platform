import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationPopUpComponent } from './location-pop-up.component';

describe('LocationPopUpComponent', () => {
  let component: LocationPopUpComponent;
  let fixture: ComponentFixture<LocationPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationPopUpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LocationPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
