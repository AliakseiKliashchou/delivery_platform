import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { LocationModalComponent } from 'src/app/features/location/location-modal/location-modal.component';

@Component({
  selector: 'app-location-pop-up',
  templateUrl: './location-pop-up.component.html',
  styleUrls: ['./location-pop-up.component.scss']
})
export class LocationPopUpComponent {

  constructor(private dialog: MatDialog) { }

  public setLocation() {
    this.dialog.open(LocationModalComponent, {
      width: "800px",
      height: "600px",
      panelClass: "app-no-padding-dialog",
    });
  }
}
