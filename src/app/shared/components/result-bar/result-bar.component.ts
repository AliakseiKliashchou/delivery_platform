import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-result-bar',
  templateUrl: './result-bar.component.html',
  styleUrls: ['./result-bar.component.scss']
})

export class ResultBarComponent {

  @Input() number: number | undefined = 0;
  @Input() value: string = '';

  public getValue(): string {
    if (this.value) {
      return `${ this.number } results found for “${ this.value }”`;
    }
    else return 'All products';
  }
}
