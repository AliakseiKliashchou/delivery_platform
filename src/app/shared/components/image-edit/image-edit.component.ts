import { Component, Inject, TemplateRef } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ImageCroppedEvent, OutputFormat } from 'ngx-image-cropper';

import { ImageEditData } from '../../interfaces/image-edit-data.interface';

@Component({
  selector: 'app-image-edit',
  templateUrl: './image-edit.component.html',
  styleUrls: ['./image-edit.component.scss']
})
export class ImageEditComponent {

  public title: string;
  public subtitle: string | TemplateRef<any>;
  public image: File | string | null;
  public aspectRatio: number | null;
  public format: OutputFormat;
  private readonly onCrop?: (url: Blob) => void;
  private readonly onCancel?: () => void;

  private croppedImage: Blob | null | undefined;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ImageEditData,
  ) {
    this.title = data.title || '';
    this.subtitle = data.subtitle || '';
    this.image = data.image;
    this.aspectRatio = data.aspectRatio || 1;
    this.format = data.format || 'png';
    this.onCrop = data.onSubmit;
    this.onCancel = data.onCancel;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.blob;
  }

  get imageUrl(): string | undefined {
    if (this.image && typeof this.image === 'string') {
      return this.image;
    } else {
      return;
    }
  }
  get imageBlob(): File | undefined {
    if (this.image && typeof this.image !== 'string') {
        return this.image;
    } else {
      return undefined;
    }
  }

  cancel() {
    if(this.onCancel)
      this.onCancel();
  }
  submit() {
    if(this.onCrop && this.croppedImage) {
      this.onCrop(this.croppedImage);
    }
  }

}
