import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ACCEPT_COOKIE } from '../../constants/endpoints';
import { CookiePreferencesDialogComponent } from '../cookie-preferences-dialog/cookie-preferences-dialog.component';

@Component({
  selector: 'app-cookie-popup',
  templateUrl: './cookie-popup.component.html',
  styleUrls: ['./cookie-popup.component.scss']
})
export class CookiePopupComponent {
  public isCookiePopupOpen: boolean = false;
  public isCookieChecked: boolean = false;

  constructor(private dialog: MatDialog) {
    this.isCookieChecked = !!window.sessionStorage.getItem(
      ACCEPT_COOKIE
    );
    this.isCookiePopupOpen = !this.isCookieChecked;
  }

  public openCookiePreferences(): void {
    this.isCookiePopupOpen = false;
    this.dialog.open(CookiePreferencesDialogComponent, {
      width: "500px",
      panelClass: 'cookie-preferences-dialog'
    });
  };

  public closeCookiePopup(): void {
    this.isCookiePopupOpen = false;
    window.sessionStorage.setItem(ACCEPT_COOKIE, 'true');
  }  
}
