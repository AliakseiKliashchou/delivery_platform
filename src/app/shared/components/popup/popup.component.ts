import { Component, Input, TemplateRef } from "@angular/core";

@Component({
  selector: "app-popup",
  templateUrl: "./popup.component.html",
  styleUrls: ["./popup.component.scss"],
})

export class PopupComponent {
  @Input() public title: string = "";
  @Input() public subtitle: string | TemplateRef<any> = "";
  @Input() public icon: string = "";
  @Input() public destination: string = "";

  public isSubtitleString(subtitle: string | TemplateRef<any>): subtitle is string {
    return typeof subtitle === "string";
  }
}
