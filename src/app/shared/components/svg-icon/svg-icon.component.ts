import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnInit {
  @Input() type: string = '';
  @Input() fill: string = '#EEEEEE';
  @Input() fillSwappingColor: string = '#1A1A1A';
  @Input() backgroundColor: string = '#1A1A1A';
  @Input() defaultBackgroundColor: string = '#EEEEEE';

  public link: string = '';
  public styleExpression: string = '';

  public changeColor() {
    [this.fill,this.fillSwappingColor] = [this.fillSwappingColor, this.fill];
    [this.defaultBackgroundColor, this.backgroundColor] = [this.backgroundColor, this.defaultBackgroundColor];
    this.styleExpression = `color: ${this.fillSwappingColor}`;
  }

  constructor() {
  }


  ngOnInit(): void {
    this.link = `../../../../assets/images/social-media-icons/${this.type}.svg#${this.type}`;
    this.styleExpression = `color: ${this.fillSwappingColor}`;
  }
}
