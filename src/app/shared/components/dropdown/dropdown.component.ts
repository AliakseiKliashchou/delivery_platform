import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { VendorAccountService } from '../../services/vendor-account.service';
import { UnsubscribeComponent } from '../../directives/unsubscribe.component';


export interface ISelecterFilterOptions  {
  selectedOptions: string[];
  filterName: string;
}
@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent extends UnsubscribeComponent implements OnInit {
  @Input() public options!: string[];
  @Input() public filterName!: string;
  @Output() public onChanged = new EventEmitter<any>();
  public selectFilterOptions(selectedOptions: string[], filterName: string) {
    let changes = { filterName, selectedOptions };
    this.onChanged.emit(changes);
  }

  public isDropdownMenuOpened: boolean = false;
  public selectedOptions: string [] = [];
  public dropdownOptions: FormGroup = new FormGroup({});

  constructor(private vendorAccountService: VendorAccountService) {
    super();
  }

  public ngOnInit(): void {
    this.initFilter();
  }

  public initFilter(): void {
    for(let option in this.options) {
      const control = this.options[option];
      this.dropdownOptions.addControl(control, new FormControl());
    }

    // this.applyOptionsByLength(this.options.length)

    switch (this.options.length) {
      case 5:
      case 6:
        this.vendorAccountService.currentAppliedStatusesFilterOptions.pipe().subscribe((selectedOptions: string[]) => {
          selectedOptions.length === 0 && this.clearFilter();
          this.selectedOptions = selectedOptions;
        });
        break;
      case 8:
        this.vendorAccountService.currentAppliedfoodGroupsFilterOptions.pipe().subscribe((selectedOptions: string[])  => {
          selectedOptions.length === 0 && this.clearFilter();
          this.selectedOptions = selectedOptions;
        });
        break;
      case 3:
        this.vendorAccountService.currentAppliedDietaryTagsFilterOptions.pipe().subscribe((selectedOptions: string[])  => {
          selectedOptions.length === 0 && this.clearFilter();
          this.selectedOptions = selectedOptions;
        });
        break;
      default:
        this.clearFilter();
     }
  }

  // public applyOptionsByLength(length: number): Subscription | void {
  //   const appliedOptionsByLength: {[key: string] : Subscription} = {
  //     5 : this.vendorAccountService.currentAppliedStatusesFilterOptions.pipe().subscribe(selectedOptions => {
  //           selectedOptions.length === 0 && this.clearFilter();
  //           this.selectedOptions = selectedOptions;
  //         }),
  //     8 : this.vendorAccountService.currentAppliedfoodGroupsFilterOptions.pipe().subscribe(selectedOptions => {
  //           selectedOptions.length === 0 && this.clearFilter();
  //           this.selectedOptions = selectedOptions;
  //         }),
  //     3: this.vendorAccountService.currentAppliedDietaryTagsFilterOptions.pipe().subscribe(selectedOptions => {
  //         selectedOptions.length === 0 && this.clearFilter();
  //         this.selectedOptions = selectedOptions;
  //       })
  //   };

  //   return appliedOptionsByLength[length] || this.clearFilter();
  // }
  public toggleDropdownMenu(): void {
    this.isDropdownMenuOpened = !this.isDropdownMenuOpened;
    if(this.isDropdownMenuOpened) {
      for (let key in this.dropdownOptions.controls) {
        this.setValue(key);
      }
    } else if(!this.isDropdownMenuOpened) {
        this.setOptionsToArray();
    }
  }
  public clickedOutside(): void {
    if(this.isDropdownMenuOpened) {
      this.setOptionsToArray();
      this.isDropdownMenuOpened = false;
    }
  }
  public getValue(value: string): boolean {
    return this.dropdownOptions.get(value)?.value;
  }
  public setValue(value: string): void {
    if(!this.isInSelectedOptions(value)) {
      this.dropdownOptions.get(value)?.setValue(true);
    } else {
      this.dropdownOptions.get(value)?.setValue(null);
    }
  }
  public isInSelectedOptions(value: string): boolean {
    return this.selectedOptions.indexOf(value) === -1;
  }
  public builderSelectedOptions(value: string): void {
    if(this.getValue(value) && this.isInSelectedOptions(value)) {
      this.selectedOptions.push(value);
    }
    else if(!this.getValue(value) && !this.isInSelectedOptions(value)) {
      this.selectedOptions.splice(this.selectedOptions.indexOf(value), 1);
    }
  }

  public setOptionsToArray(): void {
    for (let key in this.dropdownOptions.controls) {
      this.builderSelectedOptions(key);
    }
    this.selectFilterOptions(this.selectedOptions, this.filterName);
  }


  public clearFilter(): void {
    for (let key in this.dropdownOptions.controls) {
      this.dropdownOptions.get(key)?.setValue(null);
    }
    this.selectedOptions.length = 0;
  }
}
