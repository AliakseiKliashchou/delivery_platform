import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'removeAllSymbols' })
export class RemoveAllSymbolsPipe implements PipeTransform {
    transform(value: string): string {
        return value.replace(/[^A-Z0-9a-z,.!?@:;\s]/g,'');
    }
}