import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'priceRound' })
export class PriceRoundPipe implements PipeTransform {
    transform(value: number, moveDotFor: number = 2): number {
        const mul = Math.pow(10, moveDotFor);
        
        return Math.round(value * mul) / mul;
    }
}