import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment/moment";
import { ScheduleData, ScheduleList } from "../../core/models/vendor.model";

@Pipe({
  name: "scheduleCalc",
})
export class ScheduleCalcPipe implements PipeTransform {

  private readonly currentSchedule: ScheduleData = {
    openedHours: "Opening hours not specified",
    whenCloses: "",
    whenOpens: "",
    isStoreOpen: true,
    isClosesIn30Minutes: false,
  };

  private createDefaultSchedule(): ScheduleData {
    return {
      openedHours: "Opening hours not specified",
      whenCloses: "",
      whenOpens: "",
      isStoreOpen: true,
      isClosesIn30Minutes: false,
    };
  }

  transform(schedule: ScheduleList): ScheduleData {
    const currentSchedule = this.createDefaultSchedule();

    if (!schedule || Object.keys(schedule).length === 0) {
      return currentSchedule;
    }

    const currentTime = moment().format("HH:mm");
    //making an array from objects and now weekDaysList[0] is Monday
    const weekDaysList = [...Object.keys(schedule), ...Object.keys(schedule)];
    // 0 being Monday and 6 being Sunday
    const currentWeekDay = moment().isoWeekday() - 1;
    const isDayOffToday = schedule[weekDaysList[currentWeekDay]].day_off;
    const nextWorkingDay: number = this.scheduleCalcForDayOf(schedule, currentWeekDay, weekDaysList);
    const nextWorkingDaySchedule = schedule[weekDaysList[nextWorkingDay]];
    const nextWorkingDayOpeningTime = moment(nextWorkingDaySchedule.opening, "HH:mm").format("hh:mm A");
    const workingWeekendSchedule = `Opens ${nextWorkingDay - currentWeekDay > 1
      ? "on " + [weekDaysList[nextWorkingDay]]
      : "tomorrow"} at ${nextWorkingDayOpeningTime}`;
    const workingSchedule = `Closed till ${nextWorkingDay - currentWeekDay > 1
      ? [weekDaysList[nextWorkingDay]]
      : ""} ${nextWorkingDayOpeningTime} `;

    if (isDayOffToday) {
      currentSchedule.isStoreOpen = false;
      currentSchedule.openedHours = workingSchedule;
      currentSchedule.whenOpens = currentSchedule.openedHours;

    } else {
      const todaySchedule = schedule[weekDaysList[currentWeekDay]];
      const opensIn = moment(todaySchedule.opening, "HH:mm");
      const openingTime = opensIn.format("HH:mm");
      const openingTimeDifference = this.deductTime(opensIn, currentTime);
      const isOpensIn30Minutes = !openingTimeDifference[0] && openingTimeDifference[1] <= 30;
      const closesAt = moment(todaySchedule.closing, "HH:mm");
      const closingTime = closesAt.format("HH:mm");
      const closingTimeDifference = this.deductTime(closesAt, currentTime);
      const isClosesIn30Minutes = !closingTimeDifference[0] && closingTimeDifference[1] <= 30;

      if (isClosesIn30Minutes) {
        currentSchedule.isClosesIn30Minutes = true;
        currentSchedule.isStoreOpen = true;
        currentSchedule.openedHours = `Closes in ${closingTimeDifference[1]} min`;
        currentSchedule.whenCloses = currentSchedule.openedHours;
        currentSchedule.whenOpens = workingWeekendSchedule;

      } else if (isOpensIn30Minutes) {
        currentSchedule.isStoreOpen = false;
        currentSchedule.openedHours = `Opens in ${openingTimeDifference[1]} min`;
        currentSchedule.whenOpens = currentSchedule.openedHours;

      } else if (currentTime > closingTime && currentTime < openingTime) {
        currentSchedule.isStoreOpen = false;
        currentSchedule.openedHours = `Closed till ${opensIn.format("hh:mm A")}`;
        currentSchedule.whenOpens = `Opens today at ${opensIn.format("hh:mm A")}`;

      } else if (currentTime > closingTime) {
        currentSchedule.isStoreOpen = false;
        currentSchedule.openedHours = workingSchedule;
        currentSchedule.whenOpens = workingWeekendSchedule;

      } else if (currentTime < closingTime && currentTime > openingTime) {
        currentSchedule.isStoreOpen = true;
        currentSchedule.openedHours = `Opened from ${opensIn.format("hh:mm A")} to ${closesAt.format("hh:mm A")}`;

      } else if (currentTime < openingTime) {
        currentSchedule.isStoreOpen = false;
        currentSchedule.openedHours = workingSchedule;
        currentSchedule.whenOpens = workingWeekendSchedule;
      } else currentSchedule.openedHours = "Opening hours not specified";
    }

    return currentSchedule;
  }

  private scheduleCalcForDayOf(schedule: ScheduleList, currentWeekDay: number, weekDaysList: string[]): number {
    let willOpenIn = currentWeekDay + 1;
    for (let day = currentWeekDay + 1; day <= weekDaysList.length; day++) {

      if (!schedule[weekDaysList[day]]?.day_off) break;
      willOpenIn++;
    }

    return willOpenIn;
  }

  private deductTime = (opensIn: moment.Moment, currentTime: string) => {
    return opensIn
      .clone()
      .subtract(currentTime)
      .format("HH:mm")
      .split(":")
      .map(Number);
  };
}
