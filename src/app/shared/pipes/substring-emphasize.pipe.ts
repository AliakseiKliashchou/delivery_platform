import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "substringEmphasize" })
export class SubstringEmphasizePipe implements PipeTransform {
  transform(transformValue: string, substringValue: string): string {
      return transformValue.replace( new RegExp(substringValue, "gi"), (match: string): string => match && `<b>${match}</b>`);
  }
}
