import { TemplateRef } from "@angular/core";

import { OutputFormat } from "ngx-image-cropper";

export interface ImageEditData {
    title ?: string;
    subtitle ?: string | TemplateRef<any>;
    image: File | string | null;
    aspectRatio ?: number | null;
    format ?: OutputFormat;
    onSubmit ?: (url: Blob) => void;
    onCancel ?: () => void;
}
