export interface IProductData {
  select: any;
  actions: any;
  approval_status: string[];
  avg_good_rating: number;
  avg_page_rank: number;
  avg_rating: number;
  comments: any[];
  cooking_time: string;
  count: number;
  description: string;
  dietary_tags: string[];
  discounts: any[];
  food_groups: (number | string | undefined)[];
  id: number;
  images: any[];
  ingredients: string[];
  name: string;
  number_purchases: number;
  price: number;
  publish_date: any;
  quantity: number;
  updated_at: Date;
  vendor_id: number;
  weight: number;
  product_discount_percent: number;
}
