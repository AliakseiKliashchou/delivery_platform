export interface ILocationResponse {
    customer_location_id: number;
    location_id: number;
    message ?: string;
}