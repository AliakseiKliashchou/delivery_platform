import { IProduct } from "src/app/core/models/product.model";

export interface IMostOrderedProductsResp {
    success: boolean;
    products: IProduct[];
}