import { IFoodGroupStatus } from "./food-group-status.interface";

export interface IDietaryTag {
  dietary_tags: IFoodGroupStatus[];
}

export interface IFoodGroups {
  food_groups: IFoodGroupStatus[];
}

export interface IIngredients {
  ingredients: IFoodGroupStatus[];
}
