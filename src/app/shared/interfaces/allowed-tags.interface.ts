export interface AllowedTags {
  [key: number]: string[];
}
