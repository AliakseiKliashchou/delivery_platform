export interface IFoodGroupStatus {
  id: number;
  name: string;
  status: boolean;
  group?: string;
  isDisabled: boolean;
}
