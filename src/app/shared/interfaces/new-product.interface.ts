export interface INewProduct {
  name?: string;
  weight?: number;
  price?: number;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  cooking_time?: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  number_purchases?: number;
  count?: number;
  ingredients?: string[];
  // eslint-disable-next-line @typescript-eslint/naming-convention
  dietary_tags?: (number | string | undefined)[];
  // eslint-disable-next-line @typescript-eslint/naming-convention
  food_groups?: (number | string | undefined)[];
  // eslint-disable-next-line @typescript-eslint/naming-convention
  product_discount_percent?: number;
}
