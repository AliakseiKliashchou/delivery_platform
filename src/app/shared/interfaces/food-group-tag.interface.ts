export interface IFoodGroupTag {
  success: boolean;
  allowed_food_groups: string[];
}
