export interface DateFormat {
  parse: {
    dateInput: string;
  };
  display: {
    dateInput: string;
    monthYearLabel: string;
    dateA11yLabel: string;
    monthYearA11yLabel: string;
  },
}
