export interface IProductById {
  id: number,
  image: string | null,
  name: string,
  price: number,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  product_discount_percent: number,
  weight: number,
  count: number,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  cooking_time: string,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  number_purchases: number,
  vendor: {
    id: number,
    name: string,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    license_number: string,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    location_info: number[],
    // eslint-disable-next-line @typescript-eslint/naming-convention
    flag_status: any,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    star_status: number
  },
  // eslint-disable-next-line @typescript-eslint/naming-convention
  customer_comment: string[],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  vendor_comment: string[],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  food_group: string[],
  discount: number[],
  ingredient: string[],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  dietary_tag: string[],
  // eslint-disable-next-line @typescript-eslint/naming-convention
  avg_page_rank: number,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  avg_rating: number,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  avg_good_rating: number,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  approval_status: string[]
}
