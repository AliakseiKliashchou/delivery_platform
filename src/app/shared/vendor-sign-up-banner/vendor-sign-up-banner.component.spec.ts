import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorSignUpBannerComponent } from './vendor-sign-up-banner.component';

describe('VendorSignUpBannerComponent', () => {
  let component: VendorSignUpBannerComponent;
  let fixture: ComponentFixture<VendorSignUpBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorSignUpBannerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VendorSignUpBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
