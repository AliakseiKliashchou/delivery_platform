import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-vendor-sign-up-banner',
  templateUrl: './vendor-sign-up-banner.component.html',
  styleUrls: ['./vendor-sign-up-banner.component.scss']
})
export class VendorSignUpBannerComponent {

  @Output() closeBanner = new EventEmitter<void>();

  onCloseButtonClick(): void {
    this.closeBanner.emit();
  }
}
