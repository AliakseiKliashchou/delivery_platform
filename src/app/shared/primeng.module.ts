import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ButtonModule,
    DialogModule,
    DropdownModule,
    InputTextareaModule,
    MessageModule,
    ToastModule
  ],
  providers: [
    MessageService
  ],
  exports: [
    ButtonModule,
    DialogModule,
    DropdownModule,
    InputTextareaModule,
    MessageModule,
    ToastModule
  ]
})
export class PrimengModule {
}
