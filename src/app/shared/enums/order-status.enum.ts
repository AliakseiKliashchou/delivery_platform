export enum OrderStatusEnum {
  Queue = 'Queue',
  InProgress = 'In progress',
  Ready = 'Ready',
  InDelivery = 'In delivery',
  Cancelled = 'Cancelled',
  Completed = 'Completed'
}
