export enum FoodGroupEnum{
  Meat = 'Meat',
  Fruit = 'Fruit',
  Fast_food = 'Fast Food',
  Fish = 'Fish',
  Vegetables = 'Vegetables',
  Seafood = 'Seafood',
  Smoothies = 'Smoothies',
  Confection = 'Confection'
}
