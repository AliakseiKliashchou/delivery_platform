export enum ProductStatusEnum {
  Draft = 'Draft',
  Archived = 'Archived',
  Published = 'Published',
  InModeration = 'On moderation',
  Declined = 'Declined',
}
