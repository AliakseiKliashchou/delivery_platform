export enum DietaryTagsListEnum{
  Vegan = 'Vegan',
  Gluten_free = 'Gluten Free',
  Lactose_free = 'Lactose Free'
}
