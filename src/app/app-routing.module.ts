import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorComponent } from './core/components/error/error.component';
import { PrivacyPolicyComponent } from './core/components/privacy-policy/privacy-policy.component';
import { ReportMisconductPageComponent } from './core/components/report-misconduct-page/report-misconduct-page.component';
import { TermsAndCondactionsPageComponent } from './core/components/terms-and-condactions-page/terms-and-condactions-page.component';
import { AuthCustomerGuard } from './core/guards/auth-customer.guard';
import { AuthModeratorGuard } from './core/guards/auth-moderator.guard';
import { AuthVendorGuard } from './core/guards/auth-vendor.guard';
import { LoginVendorGuard } from './core/guards/login-vendor.guard';
import { CreateNewPasswordComponent } from './features/create-new-password/create-new-password.component';
import { LoginPageComponent } from './features/login-page/login-page.component';
import { ModeratorAccountAccountComponent } from './features/moderator/moderator-account-account/moderator-account-account.component';
import { ModeratorAccountFeedbackComponent } from './features/moderator/moderator-account-feedback/moderator-account-feedback.component';
import { ModeratorAccountHotlineComponent } from './features/moderator/moderator-account-hotline/moderator-account-hotline.component';
import { ModeratorAccountSettingsComponent } from './features/moderator/moderator-account-settings/moderator-account-settings.component';
import { ModeratorAccountSupportComponent } from './features/moderator/moderator-account-support/moderator-account-support.component';
import { ModeratorAccountUserComponent } from './features/moderator/moderator-account-user/moderator-account-user.component';
import { ModeratorAccountVendorsComponent } from './features/moderator/moderator-account-vendors/moderator-account-vendors.component';

import { ModeratorAccountComponent } from './features/moderator/moderator-account/moderator-account.component';
import { ModeratorSignInComponent } from './features/moderator/moderator-sign-in/moderator-sign-in.component';
import { ProductOnModerationComponent } from './features/moderator/product-on-moderation/product-on-moderation.component';
import { OrderDetailsPageComponent } from './features/order-details-page/order-details-page.component';
import { ResetPasswordComponent } from './features/reset-password/reset-password.component';
import { SearchResultComponent } from './features/search-result/search-result.component';
import { SignUpPageComponent } from './features/sign-up-page/sign-up-page.component';
import { AddNewProductPageComponent } from './features/vendor-account/add-new-product-page/add-new-product-page.component';
import { VendorAccountPageOrdersComponent } from './features/vendor-account/vendor-account-page-orders/vendor-account-page-orders.component';
import { VendorAccountPageProductsComponent } from './features/vendor-account/vendor-account-page-products/vendor-account-page-products.component';
import { VendorAccountPageComponent } from './features/vendor-account/vendor-account-page/vendor-account-page.component';
import { VendorEditProductPageComponent } from './features/vendor-account/vendor-edit-product-page/vendor-edit-product-page.component';
import { VendorPageComponent } from './features/vendor-page/vendor-page.component';
import { CustomerAccountProfilePageComponent } from './features/customer/customer-account-profile-page/customer-account-profile-page.component';
import { CustomerAccountHistoryPageComponent } from './features/customer/customer-account-history-page/customer-account-history-page.component';
import { CustomerAccountComponent } from './features/customer/customer-account/customer-account.component';

const customerAccountPageRoutes: Routes = [
  { path: 'profile', component: CustomerAccountProfilePageComponent },
  { path: 'history', component: CustomerAccountHistoryPageComponent },
];

const vendorAccountPageRoutes: Routes = [
  { path: 'products', component: VendorAccountPageProductsComponent, },
  { path: 'orders', component: VendorAccountPageOrdersComponent },
  { path: 'products/add-new-product', component: AddNewProductPageComponent, },
  { path: 'products/:id/edit', component: VendorEditProductPageComponent }
];
const moderatorAccountPageRoutes: Routes = [
  { path: 'hotline', component: ModeratorAccountHotlineComponent },
  { path: 'feedback', component: ModeratorAccountFeedbackComponent },
  { path: 'vendors', component: ModeratorAccountVendorsComponent },
  { path: 'user', component: ModeratorAccountUserComponent },
  { path: 'account', component: ModeratorAccountAccountComponent },
  { path: 'settings', component: ModeratorAccountSettingsComponent },
  { path: 'support', component: ModeratorAccountSupportComponent },
  { path: 'hotline/:id/on-moderation', component: ProductOnModerationComponent }
];

const routes: Routes = [
  {
    path: '',
    redirectTo: 'feed',
    pathMatch: 'full',
  },
  {
    path: 'feed',
    loadChildren: () =>
      import('./features/features.module').then(
        (module) => module.FeaturesModule
      ),
  },
  {
    path: 'feed/customer-account',
    canActivate: [AuthCustomerGuard],
    component: CustomerAccountComponent,
    children: customerAccountPageRoutes,
  },
  {
    path: 'feed/order',
    component: OrderDetailsPageComponent,
    canActivate: [AuthCustomerGuard],
  },
  {
    path: 'feed/vendor/sign-up',
    canActivate: [LoginVendorGuard],
    component: SignUpPageComponent,
  },
  {
    path: 'feed/vendor/login',
    canActivate: [LoginVendorGuard],
    component: LoginPageComponent,
  },
  {
    path: 'feed/vendor/reset',
    canActivate: [LoginVendorGuard],
    component: ResetPasswordComponent,
  },
  {
    path: 'feed/vendor/reset/password',
    // canActivate:[LoginVendorGuard],
    component: CreateNewPasswordComponent,
  },
  {
    path: 'feed/vendor/vendor-account/:id',
    canActivate: [AuthVendorGuard],
    component: VendorAccountPageComponent,
    children: vendorAccountPageRoutes,
  },
  {
    path: 'feed/moderation/sign-in',
    component: ModeratorSignInComponent,
    children: moderatorAccountPageRoutes
  },
  {
    path: 'feed/moderation/moderator-account',
    component: ModeratorAccountComponent,
    children: moderatorAccountPageRoutes,
    canActivate: [AuthModeratorGuard],
  },
  {
    path: 'feed/termsAndCondactions',
    component: TermsAndCondactionsPageComponent,
  },
  {
    path: 'feed/reportMisconduct',
    component: ReportMisconductPageComponent,
  },
  {
    path: 'feed/privacyPolicy',
    component: PrivacyPolicyComponent,
  },
  {
    path: 'feed/place/:id',
    component: VendorPageComponent,
  },
  {
    path: 'feed/search/:product',
    component: SearchResultComponent,
  },
  {
    path: '**',
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
