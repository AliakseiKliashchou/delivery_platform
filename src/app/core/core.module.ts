import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';

import { FeaturesModule } from '../features/features.module';
import { VendorsFilterComponent } from '../features/vendors/vendors-filter/vendors-filter.component';
import { SharedModule } from '../shared/shared.module';
import { ErrorComponent } from './components/error/error.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { NoResultPageComponent } from './components/no-result-page/no-result-page.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ScrollToTopComponent } from './components/scroll-to-top/scroll-to-top.component';
import { TermsAndCondactionsPageComponent } from './components/terms-and-condactions-page/terms-and-condactions-page.component';
import { ReportMisconductPageComponent } from './components/report-misconduct-page/report-misconduct-page.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';

@NgModule({
  declarations: [
    ErrorComponent,
    NotFoundComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    ErrorComponent,
    NoResultPageComponent,
    ScrollToTopComponent,
    TermsAndCondactionsPageComponent,
    ReportMisconductPageComponent,
    PrivacyPolicyComponent,
  ],
  imports: [
    MatMenuModule,
    MatButtonModule,
    MatInputModule,
    SharedModule,
    CommonModule,
    TranslateModule,
    FeaturesModule,
    MatSelectModule,
    RouterModule,
    AngularSvgIconModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    ErrorComponent,
    VendorsFilterComponent,
    NoResultPageComponent,
    ScrollToTopComponent,
    TermsAndCondactionsPageComponent,
    TranslateModule,
  ],
})
export class CoreModule {}
