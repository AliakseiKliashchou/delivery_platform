import { IProduct } from './product.model';

export interface IVendor {
  id: number;
  name?: string;
  vendor_name?: string;
  average_rate?: number;
  star_status?: 0;
  license_number?: string;
  products?: [];
  locations?: [];
  flag_status?: null;
  orders_count?: number;
  orders_amount?: number;
  dietary_tags?: string[];
  schedule: ScheduleList;
}

export interface IVendorsList {
  meta: IPaginationMeta;
  vendors: IVendorItem[];
}

export interface IVendorItem {
  id: number;
  name: string;
  vendor_name?: string;
  average_rate: number;
  activity_status: boolean;
  orders_amount?: number;
  locations?: ILocation[] | any;
  flag_status?: null;
  dietary_tags: string[];
  food_groups?: IFoodGroup[];
  products?: IProduct[];
  orders_count?: number;
  star_status?: number;
  schedule: ScheduleList;
}

export interface ILocation {
  id: number;
  longitude: number;
  latitude: number;
  distance: number;
  address_1: string;
  address_2: string;
  country_id: number;
  city_id: number;
}

export interface IDietaryTag {
  id: number;
  name: string;
}

export interface IFoodGroup {
  id: number;
  name: string;
}

export interface IPaginationMeta {
  page: number;
  total_pages: number;
  total_records: number;
  per_page: number;
}

export interface IVendorsFilter {
  food_group?: string;
  dietary_tags?: string[];
  order?: string;
  product_name?: string;
  // latitude?: number;
  // longitude?: number;
  // radius?: number;
  // page?: number;
  // per_page?: number;
  // max_per_page?: number;
}

export interface ScheduleList {
  [key: string]: Schedule;
}

export interface Schedule {
  id: number;
  day_off: boolean;
  opening: string | null;
  closing: string | null;
}

export interface ScheduleData {
  openedHours: string;
  whenCloses: string;
  whenOpens: string;
  isStoreOpen: boolean;
  isClosesIn30Minutes: boolean;
}

export interface IOrdersList {
  meta: IPaginationMeta;
  orders: IOrder[];
}

 export interface IOrder {
  id: number;
  type: string;
  status: string;
  price: number;
  items: string[];
  payment_status: string;
  payment_method: string;
  customer: string;
  created_at: Date;
 }

 export interface IOrdersFilter {
  required_date: string;
  statuses?: string;
  order_id?: number;
  page?: number;
  per_page?: number;
 }
