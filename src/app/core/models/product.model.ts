import { ILocation, ScheduleList } from './vendor.model';

export interface IProduct {
    id: number;
    name: string;
    weight: number;
    dietary_tags?: string[];
    ingredient?: string[];
    price: number;
    old_price?:number;
    discount?: number[];
    out_of_stock?: boolean;
    description?: string;
    quantity?: number;
    count?: number;
    vendor_id?: number;
    vendor_name?: string;
}

export interface IProducts {
    Recommended?: IProduct[];
    Meat?: IProduct[];
    Fruit?: IProduct[];
    "Fast food"?: IProduct[];
    Fish?: IProduct[];
    Vegetables?: IProduct[];
    Seafood?: IProduct[];
    Smoothies?: IProduct[];
    Confection?: IProduct[];
}

export interface IVendorData {
    average_rate: number;
    flag_status: null
    id: number;
    license_number: string;
    locations: ILocation[];
    name: string;
    orders_count: number;
    products: IProducts;
    star_status: number;
    schedule: ScheduleList;
}

export interface ICartItem {
    approval_id: number;
    avg_good_rating: number;
    avg_page_rank: number;
    avg_rating: number;
    comments: number[];
    cooking_time: string;
    count: number;
    description: string;
    dietary_tags: string[];
    discounts: number[];
    food_groups: number[];
    id: number;
    images: string[];
    ingredients: number[];
    name: string;
    number_purchases: number;
    price: number;
    quantity: number;
    vendor_id: number;
    weight: string;
}

export interface IOrderDetails {
    customerId?: number;
    customerName?: string;
    customerPhone?: string;
    customerEmail?: string;
    customerComments?: string;
    customerLocation?: string[];
    customerLocationId?: number;
}

export interface ILoggedInCustomer {
    success: boolean;
    customer_data?: ICustomerData;
}

export interface ICustomerData {
    email?: string;
    phone: string;
    id: number;
    customer_name?: string;
    location: string [];
}

export interface ICustomerLocation {
    latitude: number;
    longitude: number;
    address_1: string;
    address_2: string;
    country_name: string;
    city_name: string;
}

export interface IOrderData {
    order_status: "pending"; // TODO add other literal types
    payment_method: "by_cash"; // TODO add other literal types
    payment_status: "unpaid"; // TODO add other literal types
    order_comment: 'string';
}

export interface IOrderBody extends IOrderData {
    customer_id: number;
    order_location_id: number;
    products_quantity: {
        [key: number]: {
            quantity: number
        }
    };
    total_price: number;
    vendor_id: number;
}
