export interface IStatusHistoryData {
  old_status: string;
  new_status: string;
  created_at: Date;
  changed_by: string;
  review?: Review;
  month?: string;
}

export interface Review {
  msg: string;
  id: number;
}
