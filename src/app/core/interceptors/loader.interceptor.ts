import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Store } from "@ngxs/store";

import { SetSpinnerLoading } from "../../shared/ngxs/common.action";

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  // add a link to the api call in regexp to show the spinner when the request is receiving
  private urlsToTrack = [
    /\/api\/accounts\/vendors\/?/,
    /\/api\/accounts\/vendors\/\d+\/get_products\//,
    /\/api\/accounts\/vendors\?product_name=?.*$/,
  ];

  constructor(
    private store: Store
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url = request.url;
    const shouldTrack = this.urlsToTrack.some(regex => regex.test(url));

    if (shouldTrack) {
      this.store.dispatch(new SetSpinnerLoading(true));
    }

    return next.handle(request).pipe(
      finalize(() => {
        this.store.dispatch(new SetSpinnerLoading(false));
      })
    );
  }
}
