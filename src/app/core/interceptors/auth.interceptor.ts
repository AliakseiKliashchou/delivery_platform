import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, filter, switchMap, take } from 'rxjs/operators';

import { signUpResponse, VendorAccountService } from 'src/app/shared/services/vendor-account.service';


const tokenNeededRoutesList: string[] = [
  'vendors/me',
  '/products',
  'customer',
  'moderation',
  '/search/popular',
  '/accounts/vendor/\\d+/orders'
];

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private role: string = '';
  private isRefreshing: boolean = false;
  private refreshTokenSubject: BehaviorSubject<string> =
    new BehaviorSubject<string>('');

  constructor(
    private cookieService: CookieService,
    private vendorAccountService: VendorAccountService,
  ) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    // if (
    //       request.url.includes('vendors/me')
    //       ||
    //       request.url.includes('api/products')
    //     ) {
    //     ??? it should be without api/products for vendor
    if (
      request.url.includes('vendors/me') || (new RegExp('/accounts/vendor/\\d+/orders')).test(request.url)
    ) {
      this.role = 'vendor';
    } else if (request.url.includes('customer')) {
      this.role = 'customer';
    } else if (request.url.includes('moderation') || request.url.includes('history')) {
      this.role = 'vendor_moderator';
    }

    // if request url is in list
    if (new RegExp(tokenNeededRoutesList.join('|')).test(request.url)) {
      request = this.addToken(
        request,
        this.cookieService.get(`hsf-account-${this.role}-access-token`)
      );
    }

    return next.handle(request).pipe(
      catchError((error) => {
        if (error.status === 401 || error.status === 422) {
          return this.handleError(request, next);
        } else {
          return throwError(error);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return token ?
      request.clone({
        setHeaders: {
          // eslint-disable-next-line @typescript-eslint/naming-convention
          Authorization: `Bearer ${token}`,
        },
      }) : request;
  }

  private getRefreshToken(role: string): string {
    return this.cookieService.get(`hsf-account-${role}-refresh-token`) || '';
  }

  private handleError(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next('');

      let refreshTokenData = this.getRefreshToken(this.role);

      return this.vendorAccountService
        .refreshToken(refreshTokenData, this.role)
        .pipe(
          catchError((error) => {
            throw error;
          }),
          switchMap((tokens: signUpResponse) => {
            this.isRefreshing = false;
            this.refreshTokenSubject.next(tokens.access!);

            return next.handle(this.addToken(request, tokens.access!));
          })
        );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((token) => token != null),
        take(1),
        switchMap((jwt) => {
          return next.handle(this.addToken(request, jwt));
        })
      );
    }
  }
}
