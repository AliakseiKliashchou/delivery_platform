import {getActionTypeFromInstance} from '@ngxs/store';
import { MonoTypeOperatorFunction } from 'rxjs';
import {tap} from 'rxjs/operators';

/*
* This plugin will
* 1. Store the state in localstorage, after every action
* 2. After page is refresed, read from localstorage data and write that into state
* */
export function persistPlugin(state: any, action: any, next: (arg0: any, arg1: any) => { (): any; new(): any; pipe: { (arg0: MonoTypeOperatorFunction<unknown>): any; new(): any; }; }) {
  // After every refresh first action fired will be @@INIT
  if (getActionTypeFromInstance(action) === '@@INIT') {

    // reading from local storage and writing into state, when app is refreshed
    let storedStateStr = localStorage.getItem('LOCALSTORAGE_APP_STATE');
    let storedState = JSON.parse(storedStateStr!);
    state = {...state, ...storedState};
    return next(state, action);
  }
  
  return next(state, action).pipe(tap(result => {
  //following code will trigger after reducer
    localStorage.setItem('LOCALSTORAGE_APP_STATE', JSON.stringify(result));;
  }));
}