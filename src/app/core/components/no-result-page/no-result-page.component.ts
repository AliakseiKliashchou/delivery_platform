import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-no-result-page',
  templateUrl: './no-result-page.component.html',
  styleUrls: ['./no-result-page.component.scss']
})
export class NoResultPageComponent implements OnInit {
  @Input() value: string = 'fgfbkvhfb';

  public searchBarMargin: number = 0;
  public dietaryOptions: { title: string, img: string }[] = [ //temporary until have contract with backend
  { title: 'Vegan',
    img: 'Vegan'
  },
  { title: 'Lactose Free',
    img: 'Lactose Free'
  },
  { title: 'Gluten Free',
    img: 'Gluten Free'
  }];


  public sortOptions: { title: string, value: string }[] = [ //temporary until have contract with backend

    {
      title: 'The nearest',
      value: ''
    },
    {
      title: 'The most popular',
      value: 'true'
    }
    ];

  public filterForm!: FormGroup;

  constructor(private _translateService: TranslateService) {
  }

  public ngOnInit(): void {
    this.initFilters();
  }

  public initFilters(): void {
    this.filterForm = new FormGroup({
        'dietaryTags': new FormControl(''),
        'sortBy': new FormControl(this.sortOptions[0])
      }
    );
  }
}
