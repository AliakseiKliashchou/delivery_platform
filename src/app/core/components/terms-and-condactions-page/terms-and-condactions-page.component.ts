import { Component, OnInit } from '@angular/core';
import {
  FAO_DOCUMENT_REPOSITORY,
  OPEN_ACCESS_POLICY,
  OPEN_DATA_LICENSING_POLICY,
  PRIVACY_POLICY,
  STATISTICAL_DATABASES_TERMS_OF_USE,
} from 'src/app/shared/constants/terms-and-conditions-links';

@Component({
  selector: 'app-terms-and-condactions-page',
  templateUrl: './terms-and-condactions-page.component.html',
  styleUrls: ['./terms-and-condactions-page.component.scss'],
})
export class TermsAndCondactionsPageComponent {
  public faoDoc = FAO_DOCUMENT_REPOSITORY;
  public openAccess = OPEN_ACCESS_POLICY;
  public openDataLicense = OPEN_DATA_LICENSING_POLICY;
  public statistical = STATISTICAL_DATABASES_TERMS_OF_USE;
  public privacyPolicy = PRIVACY_POLICY;
}
