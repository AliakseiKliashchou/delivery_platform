import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { FormControl, FormGroup } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { MatDialog } from '@angular/material/dialog';

import { LocationModalComponent } from 'src/app/features/location/location-modal/location-modal.component';
import { listAnimation } from 'src/app/shared/animations/app.animation';
import { VisibilityService } from 'src/app/shared/services/visibility-service';
import { ProductState } from 'src/app/features/cart/ngxs/product.state';
import { ConfirmPhoneComponent } from '../../../features/confirm-phone-modal/confirm-phone.component';
import { getModalConfig } from '../../../shared/utils/getModalConfig';
import { IOrderDetails } from '../../models/product.model';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { CartComponent } from 'src/app/features/cart/cart.component';
import { CustomerLogout, RemoveAllProductsFromCart } from 'src/app/features/cart/ngxs/product.actions';

export interface ICustomerLogoutResponse {
  id: number;
  success: boolean;
}

@Component({
  selector: 'app-header',
  animations: [listAnimation],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends UnsubscribeComponent implements OnInit {
  public searchBarStatus: string = 'visible';
  public visibility$!: Observable<any>;
  public locationAddress?: string;
  public searchForm!: FormGroup;
  public isInfoInHeader: boolean = true;
  public isAuth: boolean = false;
  public isCustomerButtonClicked: boolean = false;
  public customerName: string | undefined = '';
  public customerEmail: string | undefined = '';
  public customerPhone: string | undefined = '';
  public customerNameAbbreviation: string | undefined = '';
  public customerId: number | undefined;
  public customerLocation!: string[];
  public location!: string[];

  @Select(ProductState.getOrderDetails)
  orderDetails$!: Observable<IOrderDetails>;
  @Select(ProductState.getTotalPrice) totalPrice$!: Observable<number>;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private dialog: MatDialog,
    private cookieService: CookieService,
    private visibilityService: VisibilityService,
    private router: Router,
    private store: Store,
  ) {
    super();
    this.visibility$ = this.visibilityService.getVisibiitySubscription();
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isInfoInHeader =
          val.url.includes('order') ||
          val.url.includes('sign-up') ||
          val.url.includes('login') ||
          val.url.includes('sign-in') ||
          val.url.includes('reset');
      }
    });
  }

  public ngOnInit(): void {
    this.initFilters();
    this.isInfoInHeader =
      this.router.url.includes('order') ||
      this.router.url.includes('sign-up') ||
      this.router.url.includes('sign-in') ||
      this.router.url.includes('login') ||
      this.router.url.includes('reset');

    this.subscribeTo = this.orderDetails$.subscribe(
      (orderDetails: IOrderDetails) => {
        if (orderDetails) {
          this.customerEmail = orderDetails.customerEmail;
          this.customerPhone = orderDetails.customerPhone;
          this.customerName = orderDetails.customerName!;
          this.customerId = orderDetails.customerId;
          this.customerLocation = orderDetails.customerLocation!;
          this.customerNameAbbreviation = this.customerName
            ?.split(' ')
            .map((word: string) => word[0])
            .join('')
            .substring(0, 2)
            .toUpperCase();
        }
      }
    );
  }

  public openDialog(): void {
    this.dialog.open(CartComponent, {
      width: '512px',
      height: '100%',
      panelClass: 'app-cart-dialog',
    });
  }

  public ngAfterViewChecked(): void {
    this.locationAddress = this.cookieService.get('hsf-account-address');
    this.location = this.cookieService.get('hsf-account-location').split(',');
    this.isAuth = !!this.cookieService.get('hsf-account-customer-access-token');
    this.customerNameAbbreviation = this.customerName
      ?.split(' ')
      .map((word: string) => word[0])
      .join('')
      .substring(0, 2);
    this.changeDetector.detectChanges();
  }

  public openMapDialog(): void {
    this.dialog.open(
      LocationModalComponent,
      getModalConfig(800, 600, 'app-no-padding-dialog')
    );
  }

  public initFilters(): void {
    this.searchForm = new FormGroup({
      search: new FormControl(''),
    });
  }

  public logout(): void {
    this.isCustomerButtonClicked = false;
    this.store.dispatch([new CustomerLogout(), new RemoveAllProductsFromCart()]);
  }

  public sigInAsCustomer(): void {
    this.dialog.open(
      ConfirmPhoneComponent,
      getModalConfig(500, 292, 'app-delete-dialog', { isCart: false })
    );
  }
}
