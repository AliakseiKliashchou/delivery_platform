import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  public searchBarMargin: number = 0;

  public dietaryOptions: { title: string, img: string }[] = [ //temporary until have contract with backend
  { title: 'Vegan',
    img: 'Vegan'
  },
  { title: 'Lactose Free',
    img: 'Lactose Free'
  },
  { title: 'Gluten Free',
    img: 'Gluten Free'
  }];

public sortOptions: { title: string }[] = [ //temporary until have contract with backend
  {
    title: 'The nearest',
  },
  {
    title: 'The most popular',
  },
  {
    title: 'The most reviewed',
  }];

public dietaryControl = new FormControl('');
public sortControl = new FormControl(this.sortOptions[0]);

  ngOnInit(): void {
    this.searchBarMargin = 0;
  }
}