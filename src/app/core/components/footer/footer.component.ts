import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import {
  CONTACT_US,
  LOGIN_AS_COURIER,
} from 'src/app/shared/constants/footer-links';
import {
  FACEBOOK,
  LINKEDIN,
  TWITTER,
  YOUTUBE,
} from 'src/app/shared/constants/footer-social-links';
import { INSTAGRAM } from './../../../shared/constants/footer-social-links';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  public facebook = FACEBOOK;
  public instagram = INSTAGRAM;
  public linkedin = LINKEDIN;
  public twitter = TWITTER;
  public youtube = YOUTUBE;
  public contactUs = CONTACT_US;

  public openLink(val: string): void {
    window.open(val);
  }
}
