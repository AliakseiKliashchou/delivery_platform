import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CanDeactivate, UrlTree } from '@angular/router';

import { Observable } from 'rxjs';

import { RefuseRegistrationDialogComponent } from 'src/app/features/refuse-registration-dialog/refuse-registration-dialog.component';
import { SignUpPageComponent } from 'src/app/features/sign-up-page/sign-up-page.component';
import { getModalConfig } from 'src/app/shared/utils/getModalConfig';

@Injectable({
  providedIn: 'root'
})
export class VendorSignUpGuard implements CanDeactivate<SignUpPageComponent> {

  constructor(private dialog: MatDialog){}
  
  canDeactivate(
    component: SignUpPageComponent
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      const dialogRef: MatDialogRef<RefuseRegistrationDialogComponent> = this.dialog.open(RefuseRegistrationDialogComponent, getModalConfig(500, 224, 'app-delete-dialog'));

      return dialogRef.afterClosed();
  }  
}