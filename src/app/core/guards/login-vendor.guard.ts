import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

import { IVendorAccountData } from 'src/app/features/login-page/login-page.component';
import { UnsubscribeComponent } from 'src/app/shared/directives/unsubscribe.component';
import { VendorAccountService } from 'src/app/shared/services/vendor-account.service';

@Injectable({
  providedIn: 'root',
})
export class LoginVendorGuard
  extends UnsubscribeComponent
  implements CanActivate
{
  constructor(
    private router: Router,
    private cookieService: CookieService,
    private vendorAccountService: VendorAccountService
  ) {
    super();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.cookieService.get('hsf-account-vendor-access-token')) {
      return true;
    } else {
      this.subscribeTo = this.vendorAccountService
        .getVendorData()
        .subscribe((res: IVendorAccountData) => {
          this.router.navigate([
            `/feed/vendor/vendor-account/${res.user.id}/products`,
          ]);
        });

      return false;
    }
  }
}
