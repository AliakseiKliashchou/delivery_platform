import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { Store } from '@ngxs/store';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

import { ModeratorState } from '../../features/moderator/ngxs/moderator.state';

@Injectable({
  providedIn: 'root'
})
export class AuthModeratorGuard implements CanActivate {
  constructor(
    private router: Router,
    private cookieService: CookieService,
    private readonly store: Store
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const isAuthenticated = this.store.selectSnapshot(ModeratorState.isAuthenticated);

    if (isAuthenticated) return true;

    this.router.navigate(['/feed']);

    return false;
  }
}
