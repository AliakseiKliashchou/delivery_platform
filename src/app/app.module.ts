import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { BrowserModule, TransferState } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { MissingTranslationHandler, TranslateLoader, TranslateModule, } from '@ngx-translate/core';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NGXS_PLUGINS } from '@ngxs/store';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { NgOtpInputModule } from 'ng-otp-input';
import { CookieService } from 'ngx-cookie-service';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { persistPlugin } from './core/services/persist.plugin';
import { FeaturesModule } from './features/features.module';
import { MapModule } from './shared/components/map/map.module';
import { translateBrowserLoaderFactory } from './shared/loaders/translate-browser.loader';
import { MissingTranslationService } from './shared/services/missing-translation.service';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from './store/store.module';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    MapModule,
    CoreModule,
    FeaturesModule,
    SharedModule,
    BrowserAnimationsModule,
    StoreModule,
    HttpClientModule,
    TransferHttpCacheModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateBrowserLoaderFactory,
        deps: [HttpClient, TransferState],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler,
        useClass: MissingTranslationService,
      },
      defaultLanguage: 'en',
    }),
    AngularSvgIconModule.forRoot(),
    NgxMaskModule.forRoot(),
    NgOtpInputModule,
    NgxsStoragePluginModule.forRoot(
      {
        key: ['authModerator']
      }
    )
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    FormGroupDirective,
    {
      provide: NGXS_PLUGINS,
      useValue: persistPlugin,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
