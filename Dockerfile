FROM node:14-slim as builder
WORKDIR /tmp
ADD package.json ./package.json
ADD package-lock.json ./package-lock.json

RUN npm ci

ADD . .
RUN npm run dev

##  STAGE 2

FROM nginx:stable-alpine

EXPOSE 80

COPY --from=builder ./tmp/dist/delivery-platform/browser ./usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx/conf /etc/nginx/conf.d

#FROM node:14-slim
#WORKDIR /tmp
### Copy source code
## COPY ./dist ./dist
#COPY --from=builder ./tmp/dist ./dist
#
### Start the application
#CMD ["node", "/tmp/dist/delivery-platform/server/main.js"]
